In the root directory, execute 
git subrepo clone <remote-url> <subdir> 
where <remote-url> is the url of the remote and 
<subdir> is the subdir that you want to clone into. 
Note that the tool will not create another subdirectory with the name of the subrepo that you are adding.

Use git subrepo pull <subdir> and git subrepo push <subdir> to pull and push.
https://github.com/ingydotnet/git-subrepo/wiki/Basics
https://github.com/ingydotnet/git-subrepo/blob/master/Intro.pod
