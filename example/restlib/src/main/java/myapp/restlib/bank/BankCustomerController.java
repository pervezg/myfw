package myapp.restlib.bank;

import my.fw.rest.BaseSvcController;
import my.fw.service.ISimpleSvc;
import myapp.model.BankCustomer;
import myapp.service.BankCustomerService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/bankcustomer")
public class BankCustomerController extends BaseSvcController<BankCustomer,String> {
    public BankCustomerController() {
        super(new BankCustomerService());
    }
}
