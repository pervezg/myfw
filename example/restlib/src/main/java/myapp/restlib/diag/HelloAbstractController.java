package myapp.restlib.diag;

import my.fw.rest.BaseRestController;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.ValidationException;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

// simple hello controller that can be used to test api liveness
// validated annotation is essential to enforce parameter validation in method
@Validated

public abstract class HelloAbstractController extends BaseRestController {

    // simple hello controller for testing any project
    // name should be alphanumeric and max 20
    @GetMapping({"/hello/{name}", "/hello"})
    public String helloString(@PathVariable(required = false) @Size(max = 20) @Pattern(regexp = "^[A-Za-z0-9]*$", message = "non alpha numeric chars found") String name) {
        String s = "hello";
        if (StringUtils.isNotBlank(name)) s = s + " " + name;
        return s;
    }


    // exception handler for validation errors
    @ExceptionHandler({ConstraintViolationException.class})
    public ResponseEntity<Object> handleConstraintViolation(
            ConstraintViolationException ex, WebRequest request) {
        List<String> errors = new ArrayList<String>();
        for (ConstraintViolation<?> violation : ex.getConstraintViolations()) {
            errors.add(violation.getRootBeanClass().getName() + " " +
                    violation.getPropertyPath() + ": " + violation.getMessage());
        }

        return new ResponseEntity<Object>(
                ex.getMessage(), new HttpHeaders(), HttpStatus.CONFLICT);
    }
}
