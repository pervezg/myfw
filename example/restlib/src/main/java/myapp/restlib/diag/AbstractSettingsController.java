package myapp.restlib.diag;

import my.fw.base.app.App;
import my.fw.base.system.MyProperties;
import my.fw.rest.BaseRestController;
import my.fw.sqlrepo.db.DBConnHelper;
import my.fw.sqlrepo.db.DSFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.context.request.WebRequest;

import javax.validation.ConstraintViolationException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class AbstractSettingsController extends BaseRestController {
    protected App app = null;

    public AbstractSettingsController() {
    }

    public AbstractSettingsController(App app) {
        this.app = app;
    }

    // returns a map of db info
    //@GetMapping(value = "/settings/db",produces = { "application/json" })
    @GetMapping(value = "/settings/db")
    public Map<String, String> getDbInfo() {
        Map<String, String> dbinfo = DBConnHelper.getDataSourceInfo(DSFactory.getDefaultDS());
        return dbinfo;

    }

    //@GetMapping(value = "/settings/props",produces = { "application/json"})
    @GetMapping(value = "/settings/props")
    public Map<String, String>getAppProps() {
        if (app != null) {
            Properties p = app.getProps();
            return MyProperties.toMap(p);
        }
        return new HashMap<>();
    }

    @ExceptionHandler({ Exception.class })
    public void handleException(  ConstraintViolationException ex, WebRequest request) {
        log.error("exception",ex);
    }
}
