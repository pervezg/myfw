DELETE FROM ROLES;
COMMIT;

INSERT INTO `ROLES`
(`name`,`title`,`description`)
VALUES
('admin','Administrator','system administrator'),
('support','Support Staff',''),
('helpdesk','Helpdesk Agent',''),
('developer','Programmer',''),
('tester','QA Tester',''),
('hr','HR',''),
('accounts','Accounts ',''),
('it','IT','')
;

DELETE FROM PERSON;
COMMIT;

INSERT INTO `PERSON`
( `userid`,`email`,`fname`,`lastname`,`fullname`,`reportsto`,`status`,`updatets`,`createts`)
VALUES
('admin','admin@example.com','Super','Admin','Super Admin',0,'ACTIVE',now(),now()),
('support','support@example.com','Support','Agent','Support Agent',0,'ACTIVE',now(),now())
;


DELETE from PERSON_ROLE;
COMMIT;
INSERT INTO `PERSON_ROLE`
(`personid`,`rolename`,`comment`,`startts`,`endts`,`updatets`)
select PERSON.id,'admin','initial data',now(),'9999-12-31 23:59:59'  ,now() from PERSON where PERSON.userid='admin';
INSERT INTO `PERSON_ROLE`
(`personid`,`rolename`,`comment`,`startts`,`endts`,`updatets`)
select PERSON.id,'support','initial data',now(),'9999-12-31 23:59:59'  ,now() from PERSON where PERSON.userid='admin';
INSERT INTO `PERSON_ROLE`
(`personid`,`rolename`,`comment`,`startts`,`endts`,`updatets`)
select PERSON.id,'support','initial data',now(),'9999-12-31 23:59:59'  ,now() from PERSON where PERSON.userid='support';


DELETE from PERSON_PROPS;
COMMIT;

INSERT INTO PERSON_PROPS (personid,name,value,updatets)
select PERSON.id,'gender','M',now() from PERSON where PERSON.userid='admin';
INSERT INTO PERSON_PROPS (personid,name,value,updatets)
select PERSON.id,'location','Mumbai',now() from PERSON where PERSON.userid='admin';