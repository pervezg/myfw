package myapp.repo;

import my.fw.sqlrepo.EntityInfo;
import my.fw.sqlrepo.EntityInfoFactory;
import my.fw.sqlrepo.repo.GenericSQLRepo;
import myapp.model.Person;

public class PersonRepo extends GenericSQLRepo<Person, Integer> {
    static EntityInfo einfo = EntityInfoFactory.getEntityInfo(Person.class);

    public PersonRepo() {
        super(Person.class, einfo);
    }
}
