package myapp.repo;

import myapp.model.User;
import my.fw.sqlrepo.db.DatabaseConnectionManager;
import my.fw.sqlrepo.repo.GenericSQLRepo;
import my.fw.sqlrepo.repo.RowUnmapper;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

public class UserRepo extends GenericSQLRepo<User,String> {
    public static final RowMapper<User> ROW_MAPPER = new RowMapper<User>() {

        public User mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new User(
                    rs.getString("user_name"),
                    rs.getDate("date_of_birth"),
                    rs.getInt("reputation"),
                    rs.getBoolean("enabled")
            ).withPersisted(true);
        }
    };

    public static final RowUnmapper<User> ROW_UNMAPPER = new RowUnmapper<User>() {

        public Map<String, Object> mapColumns(User o) {
            LinkedHashMap<String, Object> row = new LinkedHashMap<>();
            row.put("user_name", o.getUserName());
            row.put("date_of_birth", new Date(o.getDateOfBirth().getTime()));
            row.put("reputation", o.getReputation());
            row.put("enabled", o.isEnabled());
            return row;
        }
    };
    public UserRepo() {
        super("USER","user_name",ROW_MAPPER,ROW_UNMAPPER, DatabaseConnectionManager.getDS());
    }
}
