package myapp.repo;

import myapp.model.Role;
import my.fw.sqlrepo.EntityInfo;
import my.fw.sqlrepo.EntityInfoFactory;
import my.fw.sqlrepo.repo.GenericSQLRepo;

public class RoleRepo extends GenericSQLRepo<Role, String> {

    public static EntityInfo einfo = EntityInfoFactory.getEntityInfo(Role.class);

    public RoleRepo() {
        super(Role.class, einfo);
    }
}
