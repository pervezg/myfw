package myapp.repo;

import myapp.model.PersonRole;
import my.fw.sqlrepo.EntityInfo;
import my.fw.sqlrepo.EntityInfoFactory;
import my.fw.sqlrepo.repo.GenericSQLRepo;

public class PersonRoleRepo extends GenericSQLRepo<PersonRole, Integer> {
    static EntityInfo einfo = EntityInfoFactory.getEntityInfo(PersonRole.class);

    public PersonRoleRepo() {
        super(PersonRole.class, einfo);
    }

}
