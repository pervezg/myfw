package myapp.model;

import org.springframework.data.domain.Persistable;

import java.util.Date;

public class User implements Persistable<String> {
//    public class User  {


    boolean persisted;
    String userName;
    Date dateOfBirth;

    int reputation;

    boolean enabled;

    public User(String user_name, Date date_of_birth, int rep, boolean e) {
        this.userName=user_name;
        this.dateOfBirth=date_of_birth;
        this.reputation=rep;
        this.enabled=e;

    }

    public User(String _name) {
        this.userName=_name;
        this.dateOfBirth=new Date();
        this.reputation=0;
        this.enabled=true;    }

    public boolean isPersisted() {
        return persisted;
    }

    public void setPersisted(boolean persisted) {
        this.persisted = persisted;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public int getReputation() {
        return reputation;
    }

    public void setReputation(int reputation) {
        this.reputation = reputation;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }




    //@Override
    public String getId() {
        return this.userName;
    }

    //@Override
    public boolean isNew() {
        return !persisted;
    }

    public User withPersisted(boolean p) {
        this.persisted = p;
        return this;
    }
}
