package myapp.model;

import org.springframework.data.domain.Persistable;

import java.util.Date;

public class Comment implements Persistable<Integer> {

    public Comment(int id, String user_name, String contents, Date created_time) {
        this.id=id;
        this.userName=user_name;
        this.contents=contents;
        this.createdTime=created_time;
    }
    public Comment(String per, String comm) {
        this(0,per,comm,new Date());
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    private Integer id;
    private String userName;
    private String contents;
    private Date createdTime;



    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public boolean isNew() {
        return id == null || id==0;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }
}
