package myapp.model;

import my.fw.model.AbsEntityProperty;
import my.fw.model.IPropertyOfEntity;

import javax.persistence.Column;
import javax.persistence.Table;

@Table(name = "PERSON_PROPS")
public class PersonProps extends AbsEntityProperty<Integer> implements IPropertyOfEntity<Integer, Integer> {
    public Integer getPersonid() {
        return personid;
    }

    public void setPersonid(Integer personid) {
        this.personid = personid;
    }

    @Column
    Integer personid;

    @Override
    public Integer getParentId() {
        return personid;
    }

    @Override
    public void setParentId(Integer i) {
        this.personid = i;
    }
}
