package myapp.model;

import my.fw.model.BaseObjWithName;
import my.fw.model.IObjWithCreateTS;
import my.fw.model.IObjWithId;
import my.fw.model.IObjWithUpdateTS;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Table(name = "ROLES")
public class Role implements IObjWithUpdateTS, IObjWithCreateTS, IObjWithId<String> {

    @Id
    @Column
    String name;
    @Column
    String title;

    @Column
    String description;

    @Column
    Date createdOn;

    @Column
    Date modifiedOn;
    public Role() {
    }

    public Role(String s) {
        setName(s);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public Date getCreatedOn() {
        return createdOn;
    }

    @Override
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    @Override
    public Date getModifiedOn() {
        return modifiedOn;
    }

    @Override
    public void setModifiedOn(Date modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    @Override
    public String getId() {
        return name;
    }

    @Override
    public void setId(String _id) {
setName(_id);
    }
}
