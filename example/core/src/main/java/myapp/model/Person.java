package myapp.model;

import my.fw.model.*;
import org.springframework.data.domain.Persistable;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Table(name = "PERSON")
public class Person implements Persistable<Integer>,
        IEntityWithProperties<Integer, Integer>,
        IEntityWithAutogenId,
        IObjWithCreateTS, IObjWithCreatedBy, IObjWithUpdateTS {

    @Column
    @Id
    @GeneratedValue
    Integer id;

    @Column
    String userid;

    @Column
    String email;

    // example of different columnname
    @Column(name = "fname")
    String firstname;

    @Column
    String lastname;

    @Column
    Integer reportsto;

    @Column
    String fullname;

    @Column
    String status;

    @Column
    Date startDate;

    @Column
    Date createdOn;

    @Column
    String createdBy;

    @Column
    Date modifiedOn;

    Map<String, PersonProps> properties;


    public Person() {
        properties = new HashMap<>();
    }

    @Override
    public PersonProps getProp(String name) {
        return properties.get(name);
    }

    @Override
    public void putProp(IPropertyOfEntity<Integer, Integer> prop) {
        properties.put(prop.getName(), (PersonProps) prop);
    }

    @Override
    public void removeProp(String key) {
        properties.remove(key);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public boolean isNew() {
        return id == null || id == 0;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public Integer getReportsto() {
        return reportsto;
    }

    public void setReportsto(Integer reportsto) {
        this.reportsto = reportsto;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    @Override
    public Date getCreatedOn() {
        return createdOn;
    }

    @Override
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    @Override
    public Date getModifiedOn() {
        return modifiedOn;
    }

    @Override
    public void setModifiedOn(Date modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    @Override
    public String getCreatedBy() {
        return this.createdBy;
    }

    @Override
    public void setCreatedBy(String created_by) {
        this.createdBy = created_by;
    }
}
