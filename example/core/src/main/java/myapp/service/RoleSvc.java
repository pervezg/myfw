package myapp.service;

import myapp.model.PersonRole;
import myapp.model.Role;
import my.fw.base.date.MYDate;
import my.fw.sqlrepo.EntityInfo;
import my.fw.sqlrepo.EntityInfoFactory;
import my.fw.sqlrepo.service.GenericService;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class RoleSvc extends GenericService<Role, String> {
    static EntityInfo einfo = EntityInfoFactory.getEntityInfo(Role.class);
    static EntityInfo preinfo = EntityInfoFactory.getEntityInfo(PersonRole.class);
    private final GenericService<PersonRole, Integer> prsvc;


    public RoleSvc() {
        super(Role.class, einfo);
        prsvc = new GenericService<PersonRole, Integer>(PersonRole.class, preinfo);
    }

    public void findMembers() {

    }

    // saves a role assignment record
    public PersonRole AssignRole(String rolename, Integer personid, Date startDate, Date endDate) {
        var t = newAssignment(rolename, personid, startDate, endDate);
        t = prsvc.save(t);
        return t;
    }

    public static PersonRole newAssignment(String rolename, Integer personid, Date startDate, Date endDate) {
        var t = new PersonRole();
        t.setPersonid(personid);
        t.setRolename(rolename);
        if (startDate == null) startDate = MYDate.now();
        if (endDate == null) endDate = MYDate.MAXDate();
        t.setStartTS(startDate);
        t.setEndTS(endDate);
        return t;
    }
}
