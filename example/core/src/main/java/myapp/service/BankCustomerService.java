package myapp.service;

import my.fw.sqlrepo.EntityInfoFactory;
import my.fw.sqlrepo.service.GenericService;
import myapp.model.BankCustomer;

public class BankCustomerService extends GenericService<BankCustomer, String> {
    public BankCustomerService() {
        super(BankCustomer.class, EntityInfoFactory.getEntityInfo(BankCustomer.class));
    }
}
