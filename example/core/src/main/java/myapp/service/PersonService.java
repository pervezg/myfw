package myapp.service;

import myapp.MyAppSettings;
import myapp.model.Person;
import myapp.model.PersonRole;
import myapp.repo.PersonRoleRepo;
import my.fw.base.constants.MYConstants;
import my.fw.sqlrepo.EntityInfo;
import my.fw.sqlrepo.EntityInfoFactory;
import my.fw.sqlrepo.service.GenericService;

import java.util.List;


public class PersonService extends GenericService<Person, Integer> {
    static EntityInfo einfo = EntityInfoFactory.getEntityInfo(Person.class);
    //static EntityInfo personRoleEInfo = EntityInfoFactory.getEntityInfo(Role.class);

    PersonRoleRepo prrepo = new PersonRoleRepo();

    public PersonService() {
        super(Person.class, einfo);
    }

    public List<Person> findHavingRole(String rolename) {
        return _repo.findWhereChild("rolename", rolename, einfo.pk, "PERSON_ROLE", "personid");
    }

    public Person findByUserid(String userid) {
        return _repo.findOne("userid", userid);
    }

    // get role assignements for a person id
    public List<PersonRole> getRoles(Integer id) {
        return prrepo.findAll("personid", id);
    }

    public Person newPerson(String userid) {
        return newPerson(userid, null, null);
    }

    public Person newPerson(String userid, String firstName, String lastName) {
        Person p = new Person();
        p.setId(0);
        p.setUserid(userid);
        p.setStatus(MYConstants.Status.Active);
        p.setFirstname(firstName);
        p.setLastname(lastName);
        p.setEmail(userid + "@email.com");
        p = setDefaults(p);
        return format(p);
    }


    @Override
    public Person format(Person p) {
        if (MyAppSettings.User.idIsCaseSensitive && p.getUserid() != null) {
            p.setUserid(p.getUserid().toLowerCase());
        }
        if (p.getEmail() != null) p.setEmail(p.getEmail().trim().toLowerCase());
        return p;
    }

    @Override
    public Person setDefaults(Person p) {
        if (p.getId() == null) p.setId(0);
        if (p.getReportsto() == null) p.setReportsto(0);
        if (p.getFullname() == null && (p.getFirstname() != null && p.getLastname() != null)) {
            p.setFullname(p.getFirstname() + " " + p.getLastname());
        }
        if (p.getStatus() == null) p.setStatus(MyAppSettings.User.defaultStatus);

        return p;
    }
}
