package myapp.factory;


import my.fw.base.app.App;
import my.fw.base.logging.MYLogFactory;
import my.fw.sqlrepo.db.DBConnHelper;
import my.fw.sqlrepo.db.DatabaseConnectionManager;
import org.postgresql.ds.PGSimpleDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;
import java.sql.SQLException;

@Configuration

public class AppInit {


    private static DataSource ds;

    public static class DB {
        public static String dsNAme = "spring_data_jdbc_repository_test";
        public static String Schema = "example";
        public static String Host = "localhost";
        //public static String Host = "192.168.0.135";
        // public static String Host = "192.168.29.191";
//        public static String Host = "192.168.56.103";


        public static int Port = 5432;
        public static String DbUser = "postgres";
        public static String DbPwd = "love2test";
        public static String URL = "jdbc:postgresql://" + Host + ":" + Port + "/" + Schema;
        public static String DbDriverName = "com.mysql.cj.jdbc.Driver";

    }

    public static class DBMYSQL {
        public static String dsNAme = "spring_data_jdbc_repository_test";
        public static String Schema = "MYFW";
        public static String Host = "localhost";
        //public static String Host = "192.168.0.135";
        // public static String Host = "192.168.29.191";
//        public static String Host = "192.168.56.103";


        public static int Port = 3306;
        public static String DbUser = "root";
        public static String DbPwd = "password";
        public static String URL = "jdbc:mysql://" + Host + ":" + Port + "/" + Schema;
        public static String DbDriverName = "org.postgresql.jdbc3.Jdbc3PoolingDataSource";

    }

    public static class H2DB {
        public static String Schema = "MYFW";
        public static String Host = "localhost";
        public static int Port = 3306;
        public static String DbUser = "sa";
        public static String DbPwd = null;
        public static String URL = "jdbc:mysql://" + Host + ":" + Port + "/" + Schema;
        public static String DbDriverName = "com.mysql.cj.jdbc.Driver";
    }

    public static DataSource init() {

        if (ds != null) return ds;
        MYLogFactory.init(null, "myapp", "DEBUG");

        ds = getPGDataSource();
        try {
            ds.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        DatabaseConnectionManager.logDataSource(ds);
        DatabaseConnectionManager.setDefaultDS(ds);

        return null;
    }

    public static DataSource init(App _app) {

        if (ds != null) return ds;

        //MYLogFactory.init(null,_app.getAppcode(),"INFO");
        ds = DBConnHelper.getDSFromProps("db", _app.getProps());
        DatabaseConnectionManager.logDataSource(ds);
        DatabaseConnectionManager.setDefaultDS(ds);
        return null;
    }

    @Bean
    public static DataSource getDataSource() {
        //return DBConnHelper.createH2memDS( H2DB.Schema, H2DB.DbUser, H2DB.DbPwd);
        // return DBConnHelper.createH2tcpDS( H2DB.Schema, H2DB.DbUser, H2DB.DbPwd);
        //return DBConnHelper.createH2fileDS( H2DB.Schema, H2DB.DbUser, H2DB.DbPwd);
        PGSimpleDataSource _ds = new PGSimpleDataSource();
        _ds.setServerNames(new String[]{DB.Host});
        _ds.setPortNumbers(new int[]{DB.Port});
        _ds.setUser(DB.DbUser);
        _ds.setPassword(DB.DbPwd);
        _ds.setDatabaseName(DB.Schema);
        return _ds;

//        return DBConnHelper.createMysqlDS(DB.Host, DB.Port, DB.Schema, DB.DbUser, DB.DbPwd);
    }

    public static DataSource getPGDataSource() {

        //return DBConnHelper.createH2memDS( H2DB.Schema, H2DB.DbUser, H2DB.DbPwd);
        // return DBConnHelper.createH2tcpDS( H2DB.Schema, H2DB.DbUser, H2DB.DbPwd);
        //return DBConnHelper.createH2fileDS( H2DB.Schema, H2DB.DbUser, H2DB.DbPwd);
        //return DBConnHelper.createMysqlDS(DB.Host, DB.Port, DB.Schema, DB.DbUser, DB.DbPwd);
        return DBConnHelper.createPGDS(DB.Host, DB.Port, DB.Schema, DB.DbUser, DB.DbPwd);
    }


}
