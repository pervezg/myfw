package myapp.factory;

import my.fw.model.Report;
import my.fw.repo.MapRepo;
import my.fw.reports.QueryDef;
import my.fw.service.ISimpleSvc;

public class SvcFactory {

    public static ISimpleSvc<QueryDef,String> getReportRepo(){
        return new MapRepo<QueryDef>();
    }
}
