package myapp.factory;

import my.fw.app.AppFactory;
import my.fw.base.app.App;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

// common init methods for example app , mostly calling AppFactory
public class ExampleAppInit {
    public static App thisApp;
    static boolean initDone = false;

    // global init  , is run only once
    public static App initApp(Class<?> klass, App app) {
        return initApp(app, true, true);
    }

    public static App initApp(App app, boolean initLogs, boolean initDB) {
        if (initDone) return app;
        app = AppFactory.loadAppProperties(app);
        startup(app, initLogs, initDB);
        thisApp = app;
        initDone = true;
        return app;
    }

    // init without reading files - just use properties
    public static App initAppFromProps(App app, Properties props, boolean initLogs, boolean initDB) {
        if (initDone) return app;
        app = AppFactory.setAppProperties(app, props);
        startup(app, initLogs, initDB);
        thisApp = app;

        initDone = true;
        return app;
    }

    // main statup method
    static void startup(App app, boolean initLogs, boolean initDB) {
        //MyProperties.populateStatic();
        if (initLogs) {
            AppFactory.initLogs(app);
            Logger _log = LoggerFactory.getLogger(ExampleAppInit.class);
            logProper(app.getProps(), _log);
        }
        if (initDB) AppFactory.initDs(app);
    }

    public static void logProper(Properties p, Logger l) {
        p.forEach((k, v) -> l.info("{} : {}", k, v));
    }

}
