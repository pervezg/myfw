package myapp.txn;

import myapp.service.PersonService;
import myapp.service.RoleSvc;
import myapp.model.Person;
import myapp.model.PersonRole;
import my.fw.model.Result;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

// Example of transactions related to persons.
@Component
public class PersonTxn {

    PersonService psvc = new PersonService();


    // testing DECLERATIVE  transactional support
    // we will call this method with a very large role name in which case the personrole insert will fail
    // and cause the person insert to rollback
    @Transactional
    public Result AddPersonD(Person p, List<String> roles) {
        return _addPerson(p, roles);
    }


    // testing PROGRAMATIC  txn method
    public Result AddPersonP( Person p, List<String> roles) {
        return psvc.runInTxn((ts) -> _addPerson(p, roles));
    }


    // used for declarative or prog  txn
    //
    private Result _addPerson(Person p, List<String> roles) {
        RoleSvc rosvc = new RoleSvc();
        p = psvc.save(p);
        if (roles != null) {
            for (String r : roles) {
                PersonRole pr = rosvc.AssignRole(r, p.getId(), null, null);
            }
        }
        return Result.SuccessWithData(p);
    }

}
