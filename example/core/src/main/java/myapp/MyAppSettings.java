package myapp;

import my.fw.base.constants.MYConstants;

public class MyAppSettings {
    public static class DB {
        public static String dsname = "name";
        public static String schema = "schema";
        public static String host = "host";
        public static String port = "port";
        public static String username = "username";
        public static String password = "password";
        public static String url = "url";
        public static String driverClassName = "driver-class-name";
    }

    public static class User {
        // if true then userid will be converted to lowercase
        public static boolean idIsCaseSensitive = false;
        // status of new user if none is provided
        public static String defaultStatus = MYConstants.Status.Active;
    }
    public static class Api{
        public static String base="";
    }
}
