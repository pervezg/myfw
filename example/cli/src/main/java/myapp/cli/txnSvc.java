package myapp.cli;

import myapp.service.RoleSvc;
import myapp.model.Role;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class txnSvc {
    private RoleSvc roleSvc;

    public txnSvc() {
        roleSvc = new RoleSvc();
    }

    @Transactional(value = "")
    public void txn() {
        //   var l = roleSvc.findAll();
        roleSvc = new RoleSvc();
        DebugUtils.showTransactionStatus("_____________________________________________txn");
        var r1 = new Role("tom");
        var r2 = new Role("00000000001111111111000000000011111111110000000000111111111100000000001111111111000000000011111111110000000000111111111100000000001111111111");
        r1 = roleSvc.save(r1);
        r2 = roleSvc.save(r2);
    }


}
