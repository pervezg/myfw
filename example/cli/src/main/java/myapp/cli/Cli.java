package myapp.cli;

import myapp.service.RoleSvc;
import myapp.constants.MyAppConst;
import myapp.factory.AppInit;
import myapp.model.Person;
import myapp.txn.PersonTxn;
import my.fw.base.app.App;
import my.fw.model.Result;
import myapp.cli.constants.MyCliConst;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;

//https://dzone.com/articles/bountyspring-transactional-management
//https://dzone.com/articles/spring-transactional-amp-exceptions
// https://dzone.com/articles/spring-transaction-management
//https://dzone.com/articles/spring-transaction-management-an-unconventional-gu
// http://blog.timmattison.com/archives/2012/04/19/tips-for-debugging-springs-transactional-annotation/
//https://codete.com/blog/5-common-spring-transactional-pitfalls/
//https://stackoverflow.com/questions/9034101/how-to-get-transactionmanager-reference-in-spring-programmatic-transaction
@Component
//@EnableTransactionManagement

public class Cli implements CommandLineRunner {

    static App mycliApp = new App(MyCliConst.moduleName, MyAppConst.appCode);
    private static Logger LOG = LoggerFactory
            .getLogger(Cli.class);

    @Bean
    public DataSource dataSource() {
        return AppInit.init();
    }

    @Bean
    public PlatformTransactionManager txManager() {
        return new DataSourceTransactionManager(dataSource());
    }

    @Autowired
    private PlatformTransactionManager transactionManager;

    private RoleSvc roleSvc;
    // service must be autowired for transaction to work
    @Autowired
    PersonTxn _txn;

    //final TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);

    @Override
    public void run(String... args) throws Exception {
        LOG.info("EXECUTING : command line runner");
        TxnTest();

    }


    //try same test using declarative and templated txn
    public void TxnTest() {

        try {
            Result r1 = PersonTxnTest(false, true);
            if (r1.status) System.out.println("Success: DECL created person " + ((Person) r1.getData()).getUserid());
            Result r2 = PersonTxnTest(true, true);
        } catch (Exception e) {
            System.out.println("run: Exception calling DECL transaction: " + e.getMessage());
        }
        try {
            Result r1 = PersonTxnTest(false, false);
            if (r1.status) System.out.println("Success: PROG created person " + ((Person) r1.getData()).getUserid());
            Result r2 = PersonTxnTest(true, false);
        } catch (Exception e) {
            System.out.println("run: Exception calling PROG transaction: " + e.getMessage());
        }

    }


    // add a person and assign a very large role. this will cause a rollback.
    public Result PersonTxnTest(boolean triggerError, boolean decltxn) {
        Person p = new Person();
        // create a good or bad role based on flag
        String badRole = triggerError ? UUID.randomUUID().toString() + UUID.randomUUID().toString() + UUID.randomUUID().toString() + UUID.randomUUID().toString() : UUID.randomUUID().toString();
        var roles = new ArrayList<String>(Arrays.asList("admin", badRole));
        p.setUserid(UUID.randomUUID().toString());

        return decltxn ? _txn.AddPersonD(p, roles) : _txn.AddPersonP(p, roles);
    }

}
