package myapp.cli;

import myapp.factory.AppInit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
// we need component scan so that services can be recognized as beans . else we need to add a config class to the core lib
@ComponentScan(basePackages ={"app.txn","myapp.*"})

public class CliBootApp {
    public static void main(String[] args) {
        AppInit.init();
        SpringApplication.run(CliBootApp.class, args);
    }

}
