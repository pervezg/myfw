package myapp.api;

import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(1)
class MyCommandLineRunner implements CommandLineRunner {


    @Override
    public void run(String... args) throws Exception {
       // AppInit.init();
    }

}