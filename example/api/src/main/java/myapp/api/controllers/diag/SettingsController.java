package myapp.api.controllers.diag;

import myapp.factory.ExampleAppInit;
import myapp.restlib.diag.AbstractSettingsController;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SettingsController extends AbstractSettingsController {
    public SettingsController() {
        super(ExampleAppInit.thisApp);
    }
}
