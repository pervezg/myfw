package myapp.api.controllers;

import my.fw.rest.BaseRestController;
import my.fw.rest.BaseSvcController;
import myapp.model.BankCustomer;
import myapp.service.BankCustomerService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//@RestController
@RequestMapping("/bank/customer")
public class PersonController extends BaseSvcController<BankCustomer,String> {
    public PersonController() {
        super(new BankCustomerService());
    }
}
