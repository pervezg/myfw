package myapp.api.events;

import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ApplicationContextEvent;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

// simple listener to log ALL application events .
// note: DO NOT USE LOGGER since this can be called before log in initialized
@Component
public class ContextRefreshedListener
        implements ApplicationListener<ApplicationEvent> {
    @Override
    public void onApplicationEvent(ApplicationEvent cse) {
        System.out.println(">>>>>>>Handling : "+cse.getClass().getSimpleName().toString());
    }
}