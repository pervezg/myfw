package myapp.api;

import my.fw.app.AppFactory;
import my.fw.base.app.App;
import myapp.api.constants.MyApiConst;
import myapp.constants.MyAppConst;
import myapp.factory.ExampleAppInit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.autoconfigure.metrics.MetricsAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;
import org.springframework.boot.context.event.ApplicationPreparedEvent;
import org.springframework.boot.context.event.ApplicationStartingEvent;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.web.context.support.ServletRequestHandledEvent;

import javax.servlet.ServletContext;
import javax.sql.DataSource;


@SpringBootApplication(exclude = {RedisAutoConfiguration.class, MetricsAutoConfiguration.class})
//@PropertySource(value = "myapp.properties")
public class MyExampleAPIApplication {
    static App myapiApp = new App(MyApiConst.moduleName, MyAppConst.appCode);


    public static void main(String[] args) {
        System.out.println(">>>>>>>>.in main method of Application");
        //ExampleAppInit.initApp(MyExampleAPIApplication.class, myapiApp);

        SpringApplication app = new SpringApplication(MyExampleAPIApplication.class);

        app.addListeners((ApplicationStartingEvent event) -> {
            System.out.println(">>>>>>>>.in ContextRefreshedEvent of Application");
            ExampleAppInit.initApp(MyExampleAPIApplication.class, myapiApp);
        });
        app.addListeners((ApplicationPreparedEvent event) -> {
            AppFactory.initLogs(myapiApp);
            AppFactory.disableExtraLogs(myapiApp);
        });

        app.addListeners((ServletRequestHandledEvent e) -> {
            System.out.println(("++++++" + e.getRequestUrl()));
        });
        app.addListeners((ContextRefreshedEvent e) -> {
            var c = e.getApplicationContext();
            System.out.println(("C++++++"));
        });

       // System.setProperty("spring.config.name", "myapp.properties");

        app.run(args);
    }

//    // this method is bound to application startup event and used to initialize the app factory
//    public static void appStartup(SpringApplication app, ApplicationEvent event) throws IOException {
//        //System.out.println(">>>>>>>Handling    " + event.getClass().getSimpleName().toString());
//        AppFactory.init(myapiApp);
//        //AppFactory.initAppProperties(MyExampleAPIApplication.class, myapiApp);
//        AppFactory.initDs(myapiApp);
//    }


//    @Bean
//    public DataSource getDataSource() {
//        System.out.println(">>>>>>>Getting DS");
//
//        DataSource ds = DBConnHelper.createMysqlDS(AppInit.DB.Host, AppInit.DB.Port, AppInit.DB.Schema, AppInit.DB.DbUser, AppInit.DB.DbPwd);
//        DatabaseConnectionManager.logDataSource(ds);
//        DatabaseConnectionManager.setDefaultDS(ds);
//        return ds;
//    }


}
