DROP TABLE if exists bank_customer;
CREATE TABLE bank_customer
(
    id        VARCHAR(50) NOT NULL,
    cust_type CHAR(1)     NOT NULL,
    fullname  VARCHAR(50) NULL,
    phone     VARCHAR(45) NOT NULL,
    email     VARCHAR(45) NOT NULL,
    PRIMARY KEY (id),
    UNIQUE INDEX id_UNIQUE (id ASC) VISIBLE
)
    COMMENT = 'customer info for bank customers';
