
DELETE FROM PERSON;
DELETE FROM PERSON_ROLE;
COMMIT;

INSERT INTO PERSON
(userid, email, fname, lastname, fullname, reportsto, status, modifiedon, createdon,createdby)
VALUES ('admin', 'admin@example.com', 'Super', 'Admin', 'Super Admin', 0, 'ACTIVE', now(), now(),'admin'),
       ('support', 'support@example.com', 'Support', 'Agent', 'Support Agent', 0, 'ACTIVE', now(), now(),'admin')
;


DELETE
from PERSON_ROLE;
COMMIT;
INSERT INTO PERSON_ROLE
    (personid, rolename, comment, startts, endts, modifiedon)
select PERSON.id, 'admin', 'initial data', now(), '9999-12-31 23:59:59', now()
from PERSON
where PERSON.userid = 'admin';
INSERT INTO PERSON_ROLE
    (personid, rolename, comment, startts, endts, modifiedon)
select PERSON.id, 'support', 'initial data', now(), '9999-12-31 23:59:59', now()
from PERSON
where PERSON.userid = 'admin';
INSERT INTO PERSON_ROLE
    (personid, rolename, comment, startts, endts, modifiedon)
select PERSON.id, 'support', 'initial data', now(), '9999-12-31 23:59:59', now()
from PERSON
where PERSON.userid = 'support';


DELETE
from PERSON_PROPS;
COMMIT;

INSERT INTO PERSON_PROPS (personid, name, value, modifiedon)
select PERSON.id, 'gender', 'M', now()
from PERSON
where PERSON.userid = 'admin';
INSERT INTO PERSON_PROPS (personid, name, value, modifiedon)
select PERSON.id, 'location', 'Mumbai', now()
from PERSON
where PERSON.userid = 'admin';