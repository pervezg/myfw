DELETE
FROM ROLES;
COMMIT;

INSERT INTO ROLES
    (name, title, description)
VALUES ('admin', 'Administrator', 'system administrator'),
       ('support', 'Support Staff', ''),
       ('helpdesk', 'Helpdesk Agent', ''),
       ('developer', 'Programmer', ''),
       ('tester', 'QA Tester', ''),
       ('hr', 'HR', ''),
       ('accounts', 'Accounts ', ''),
       ('it', 'IT', '')
;
