DROP TABLE IF EXISTS PERSON ;
CREATE TABLE PERSON (
                          id SERIAL NOT NULL ,
                          userid VARCHAR(64) NULL,
                          email VARCHAR(64) NULL,
                          fname VARCHAR(128) NULL,
                          lastname VARCHAR(64) NULL,
                          fullname VARCHAR(45) NULL,
                          reportsto INT NULL,
                          status CHAR(10) NULL,
                          startdate DATE NULL,
                          modifiedon timestamp NULL,
                          createdon timestamp NULL,
                          PRIMARY KEY (id)
);
CREATE UNIQUE INDEX idx_PERSON_userid  ON PERSON (userid) ;

DROP TABLE IF EXISTS PERSON_PROPS ;
CREATE TABLE PERSON_PROPS (
                                id SERIAL NOT NULL ,
                                personid INT NOT NULL,
                                name VARCHAR(64) NOT NULL,
                                value VARCHAR(256),
                                modifiedon timestamp NULL,
                                PRIMARY KEY (id)
);
CREATE UNIQUE INDEX idx_PERSONPROPS  ON PERSON_PROPS (personid,name) ;


DROP TABLE IF EXISTS ROLES;
CREATE TABLE ROLES (
                       name VARCHAR(64),
                       title VARCHAR(255),
                       description VARCHAR(4000),
                       createdon timestamp NULL,
                       modifiedon timestamp NULL,
                       PRIMARY KEY (name)
);

DROP TABLE IF EXISTS PERSON_ROLE;
CREATE TABLE PERSON_ROLE (
                             id SERIAL NOT NULL ,
                             personid INT,
                             rolename VARCHAR(64),
                             comment VARCHAR(256),
                             startts timestamp NULL,
                             endts timestamp NULL,
                             modifiedon timestamp NULL,
                             PRIMARY KEY (id)
);
CREATE UNIQUE INDEX idx_PERSONROLE  ON PERSON_ROLE (personid,rolename) ;

drop table if exists  login;
CREATE TABLE LOGIN (
                       id VARCHAR(64) NOT NULL,
                       personid INT NOT NULL,
                       password VARCHAR(256) NOT NULL,
                       lastlogints timestamp NULL DEFAULT now(),
                       modifiedon timestamp NULL,
                       status CHAR(10) NOT NULL,
                       failcount INT NULL DEFAULT 0,
                       PRIMARY KEY (id));
