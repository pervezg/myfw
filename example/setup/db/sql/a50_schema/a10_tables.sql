DROP TABLE IF EXISTS `PERSON` ;
CREATE TABLE `PERSON` (
                          `id` INT NOT NULL AUTO_INCREMENT,
                          `userid` VARCHAR(64) NULL,
                          `email` VARCHAR(64) NULL,
                          `fname` VARCHAR(128) NULL,
                          `lastname` VARCHAR(64) NULL,
                          `fullname` VARCHAR(45) NULL,
                          `reportsto` INT NULL,
                          `status` CHAR(10) NULL,
                          `startdate` DATE NULL,
                          `modifiedon` DATETIME NULL,
                          `createdby` VARCHAR(64) NOT NULL ,
                          `createdon` DATETIME NULL,
                          PRIMARY KEY (`id`)
);
CREATE UNIQUE INDEX `idx_PERSON_userid`  ON `PERSON` (userid) ;

DROP TABLE IF EXISTS `PERSON_PROPS` ;
CREATE TABLE `PERSON_PROPS` (
                                id INT NOT NULL AUTO_INCREMENT,
                                personid INT NOT NULL,
                                name VARCHAR(64) NOT NULL,
                                value VARCHAR(256),
                                modifiedon DATETIME NULL,
                                PRIMARY KEY (`id`)
);
CREATE UNIQUE INDEX `idx_PERSONPROPS`  ON `PERSON_PROPS` (personid,name) ;


DROP TABLE IF EXISTS ROLES;
CREATE TABLE ROLES (
                       name VARCHAR(64),
                       title VARCHAR(255),
                       description VARCHAR(4000),
                       createdon DATETIME NULL,
                       modifiedon DATETIME NULL,
                       PRIMARY KEY (name)
);

DROP TABLE IF EXISTS PERSON_ROLE;
CREATE TABLE PERSON_ROLE (
                             `id` INT NOT NULL AUTO_INCREMENT,
                             personid INT,
                             rolename VARCHAR(64),
                             comment VARCHAR(256),
                             startts DATETIME NULL,
                             endts DATETIME NULL,
                             modifiedon DATETIME NULL,
                             PRIMARY KEY (`id`)
);
CREATE UNIQUE INDEX `idx_PERSONROLE`  ON `PERSON_ROLE` (personid,rolename) ;

CREATE TABLE LOGIN (
                       `id` VARCHAR(64) NOT NULL,
                       `personid` INT NOT NULL,
                       `password` VARCHAR(256) NOT NULL,
                       `lastlogints` DATETIME NULL DEFAULT now(),
                       `modifiedon` DATETIME NULL,
                       `status` CHAR(10) NOT NULL,
                       `failcount` INT NULL DEFAULT 0,
                       PRIMARY KEY (`id`));
