-- create example database and load test data
set @dbname='myexample' ;
drop database if exists myexample ;
create database myexample ;
use myexample;
source ./a50_schema/a10_tables.sql \p;
source ./a50_schema/a20_views.sql \p;
source ./a50_schema/a30_indexes.sql \p;
source ./a50_schema/c10_procs.sql \p;
source ./b50_masterdata/roles.sql  \p;
source ./t50_testdata/person.sql  \p;
show tables;
