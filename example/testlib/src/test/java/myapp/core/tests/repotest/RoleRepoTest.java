package myapp.core.tests.repotest;

import my.fw.base.date.MYDate;
import myapp.constants.DBConst;
import myapp.core.tests.TestInit;
import myapp.model.Role;
import myapp.repo.RoleRepo;
import myapp.service.RoleSvc;
import myapp.test.testlib.helpers.SQLTestHelper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.dao.DuplicateKeyException;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)

public class RoleRepoTest {

    private RoleRepo repo;
    private RoleSvc svc;

    public static void deleteTestRoles(String prefix) throws IOException, SQLException {
        SQLTestHelper.deleteLike(DBConst.Tables.Roles, "name", prefix);
//        ScriptRunner sqlRunner = new ScriptRunner(DatabaseConnectionManager.getDS().getConnection(), true, true);
//
//        String delete = "delete from ROLES where name like '%" + prefix + "%'; \n";
//        sqlRunner.runScript(new StringReader(delete));
//        SQLTestHelper.deleteLike("ROLES",prefix);

    }

    // INSERTS A number of test roles.
    public static void insertTestRoles(String prefix, int count) throws SQLException, IOException {
        var sql = "INSERT INTO ROLES (name,title,description,createdon) VALUES ('%s','%s','%s','%s') ;\n";
        String insert = "";
        for (int i = 0; i < count; i++) {
            var createdon = MYDate.toDbDate(MYDate.now());
            var rolename = prefix + i;
            insert += String.format(sql, rolename, rolename, rolename, createdon);
        }
        deleteTestRoles(prefix);
        SQLTestHelper.runSQL(insert);
    }

    @BeforeAll
    public void setUp() throws Exception {
        TestInit.initTestEnv();
        svc = new RoleSvc();
        repo = new RoleRepo();
    }

    // test exists, count, fineone and findall etc.
    @Test
    public void findTest() throws IOException, SQLException {
        String prefix = "findtestrole";
        Role role = null;
        int count = 50;
        var now = MYDate.now();

        deleteTestRoles(prefix);

        long precount = repo.count();
        // findone , expect null
        var role1 = repo.findOne(prefix + 1);
        assertNull(role1);

        assertFalse(repo.exists(prefix + 1));
        insertTestRoles(prefix, count);


        assertTrue(repo.exists(prefix + 1));

        // check counts before and after insert
        long postcount = repo.count();
        assertEquals(precount + count, postcount);

        // check every role
        for (int i = 0; i < count; i++) {
            String rolename = prefix + i;
            role = svc.findOne(rolename);
            assertEquals(rolename, role.getId());
            assertEquals(rolename, role.getName());
        }

        role1 = repo.findOne(prefix + 1);
        assertNotNull(role1);
        assertEquals(prefix + 1, role1.getId());
        assertEquals(prefix + 1, role1.getName());

        // find like and compare count
        List<Role> roles = repo.findLike("name", prefix);
        assertEquals(count, roles.size());

        // add one more and compare count. use findall+filter to get list
        role = svc.save(new Role(prefix + (count + 1)));
        roles = svc.findAll().stream().filter(r -> r.getName().startsWith(prefix)).collect(Collectors.toList());
        assertEquals(count + 1, roles.size());

        // delete 1 role , check count
        svc.delete(role.getId());
        roles = repo.findLike("name", prefix);
        assertEquals(count, roles.size());

        //  roles = repo.findAfterDate("createdon", now);

    }

    @Test
    public void findByDateTest() throws IOException, SQLException {
        String prefix = "bydate";
        Role role = null;
        int count = 10;
        List<Role> roles = null;
        var now = MYDate.now();
        deleteTestRoles(prefix);

        // expect 0 roles before now
        roles = repo.findBeforeDate("createdon", now).stream().filter(r -> r.getName().startsWith(prefix)).collect(Collectors.toList());
        assertEquals(0, roles.size());


        // insert roles but move clock ahead by 1 min for each record
        for (int i = 0; i < count; i++) {
            String rolename = prefix + i;
            MYDate.AddMins(1);
            role = svc.save(new Role(prefix + i));
            assertEquals(rolename, role.getId());
        }
        MYDate.Reset();

        // find all roles after the starting TS, expect count
        roles = repo.findAfterDate("createdon", now).stream().filter(r -> r.getName().startsWith(prefix)).collect(Collectors.toList());

        assertEquals(count, roles.size());


        // find all roles after the first TS, expect count
        role = roles.get(0);
        roles = repo.findAfterDate("createdon", role.getCreatedOn()).stream().filter(r -> r.getName().startsWith(prefix)).collect(Collectors.toList());
        ;
        assertEquals(count, roles.size());


        // get the last role and use its ts for after - expect 1
        role = roles.get(count - 1);
        roles = repo.findAfterDate("createdon", role.getCreatedOn());
        // we should find only 1 role because we have ensured different minutes for each role.
        assertEquals(1, roles.size());


        // get the last role and use its ts for before - expect count
        roles = repo.findBeforeDate("createdon", role.getCreatedOn()).stream().filter(r -> r.getName().startsWith(prefix)).collect(Collectors.toList());
        assertEquals(count, roles.size());

        // expect count rows between now and last record
        roles = repo.findBetweenDates("createdon", now, role.getCreatedOn()).stream().filter(r -> r.getName().startsWith(prefix)).collect(Collectors.toList());
        assertEquals(count, roles.size());
        // swap the dates , expect 0
        roles = repo.findBetweenDates("createdon", role.getCreatedOn(), now);
        assertEquals(0, roles.size());

    }

    @Test
    public void saveTest() {
        Role r = new Role();
        r.setName(UUID.randomUUID().toString());
        r.setDescription("savetest in RoleRepo test");
        Date _now = MYDate.now();
        // move clock so that we can check timestamps
        MYDate.AddMins(1);
        Role r1 = svc.save(r);
        MYDate.Reset();
        assertNotNull(r1);
        assertNotNull(r1.getCreatedOn());
        assertNotNull(r1.getModifiedOn());

        assertNull(r1.getTitle());
        assertEquals(r.getName(), r1.getName());
        assertEquals(r1.getId(), r.getId());
        assertEquals(r1.getDescription(), r.getDescription());
        assertTrue(_now.before(r1.getCreatedOn()));
        assertTrue(_now.before(r1.getModifiedOn()));

        r1 = svc.findOne(r.getId());
        assertNotNull(r1);
        assertNotNull(r1.getCreatedOn());
        assertNotNull(r1.getModifiedOn());

        assertNull(r1.getTitle());
        assertEquals(r.getName(), r1.getName());
        assertEquals(r1.getId(), r.getId());
        assertEquals(r1.getDescription(), r.getDescription());
        assertTrue(_now.before(r1.getCreatedOn()));
        assertTrue(_now.before(r1.getModifiedOn()));

    }

    @Test
    public void dupTest() {
        Role r = new Role();
        r.setName(UUID.randomUUID().toString());
        r.setDescription("duptest in RoleRepo test");
        Role r1 = svc.save(r);
        assertNotNull(r1);
        AtomicReference<Role> r2 = null;
        // now expect an exception
        DuplicateKeyException ex = assertThrows(DuplicateKeyException.class,
                () -> r2.set(svc.save(r)));
    }

    @Test
    public void deleteTest() throws IOException, SQLException {
        String prefix = "deletetest";
        int count = 10;

        deleteTestRoles(prefix);

        insertTestRoles(prefix, count);

        // find, delelete, find
        assertTrue(repo.exists(prefix + 1));
        int i = repo.delete(prefix + 1);
        assertEquals(1, i);
        assertFalse(repo.exists(prefix + 1));


    }
}
