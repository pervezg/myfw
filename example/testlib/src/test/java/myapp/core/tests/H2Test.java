package myapp.core.tests;

import my.fw.sqlrepo.db.DBConnHelper;
import my.fw.sqlrepo.db.DatabaseConnectionManager;
import myapp.model.Person;
import myapp.repo.PersonRepo;
import myapp.repo.UserRepo;
import org.h2.jdbcx.JdbcDataSource;
import org.junit.jupiter.api.Disabled;

import javax.sql.DataSource;
import java.util.List;

@Disabled

public class H2Test {

    //@Test
    public void test1() {

        String url = "jdbc:h2:mem:";
        url = "jdbc:h2:~/test";
        JdbcDataSource ds = new JdbcDataSource();
        ds.setURL(url);
        ds.setUser("sa");
        DatabaseConnectionManager.setDefaultDS(ds);
        UserRepo repo = new UserRepo();
    }

   // @Test
    public void test2() {

        DataSource ds = createH2memDS("myfw", 0, "myfw", "sa", null);
        DatabaseConnectionManager.setDefaultDS(ds);
        PersonRepo repo = new PersonRepo();
        List<Person> people = repo.findAll();
    }

    public static DataSource createH2memDS(String host, int port, String schema, String userName, String password) {
        String url = "jdbc:h2:mem:" + schema ;
      //  url+=";INIT=RUNSCRIPT FROM 'classpath:schema.sql'";
      //  url+="\\;RUNSCRIPT FROM 'classpath:data.sql'";

        return DBConnHelper.newds(DBConnHelper.h2Driver,url,null,userName,null);

    }

}
