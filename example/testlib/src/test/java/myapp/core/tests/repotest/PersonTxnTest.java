package myapp.core.tests.repotest;

import my.fw.model.Result;
import myapp.core.tests.TestInit;
import myapp.model.Person;
import myapp.service.PersonService;

import myapp.test.testlib.helpers.SQLTestHelper;
import myapp.test.testlib.helpers.TestHelper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.dao.DuplicateKeyException;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)

public class PersonTxnTest {

    private PersonService svc;

    @BeforeAll
    public void setUp() throws Exception {
        TestInit.initTestEnv();
        svc = new PersonService();
    }

//    @Autowired
//    PersonTxn _txn;
//
//    @Test
//    public void PersonTxnTest() {
//        Person p = new Person();
//        p = TestHelper.newPerson("", "", "");
//        var roles = new ArrayList<String>(Arrays.asList("admin"));
//        p.setUserid("txntest");
//        _txn.AddPersonD(p, roles);
//    }

    @Test
    public void NoTxn() {
        // add 3 persons 2 of which have same userid
        // result is 2 records
        String prefix = "goodtxn";
        String u1 = prefix + 1;
        String u2 = prefix + 2;
        SQLTestHelper.deleteLike("PERSON", "userid", prefix);
        Exception ex = null;

        try {
            create2Persons(prefix);
        } catch (Exception e) {
            ex = e;
        }

        Person p11 = svc.findByUserid(u1);
        assertNotNull(p11);
        assertEquals(u1, p11.getUserid());

        Person p21 = svc.findByUserid(u2);
        assertNotNull(p21);
        assertEquals(u2, p21.getUserid());

        assertEquals(DuplicateKeyException.class.getName(),ex.getClass().getName());

    }

    @Test
    public void BadTxn() {
        // add 2 persons with SAME id , both must FAIL
        String prefix = "goodtxn";
        String u1 = prefix + 1;
        String u2 = prefix + 2;
        SQLTestHelper.deleteLike("PERSON", "userid", prefix);

        Result r = svc.runInTxn(ts -> create2Persons(prefix));
              // u1 and u2 should both be missing
        Person p11 = svc.findByUserid(u1);
        assertNull(p11);
        Person p21 = svc.findByUserid(u2);
        assertNull(p21);

        assertNotNull(r);
        assertNotNull(r.getException());
        assertFalse(r.isSuccess());
        assertEquals(DuplicateKeyException.class.getName(), r.getException().getClass().getName());
    }

    public Result create2Persons(String prefix) {
        // try create 2 users but save user2 twice. this will cause an exception
        Person p1 = TestHelper.newPerson(prefix + "1");
        Person p2 = TestHelper.newPerson(prefix + "2");

        p1 = svc.save(p1);
        p2 = svc.save(p2);
        Person p3 = svc.save(p2);
        return Result.Success();
    }
}
