package myapp.core.tests.cachetest;

import myapp.factory.AppInit;
import myapp.repo.RoleRepo;
import org.junit.jupiter.api.BeforeAll;

public class TableCacheTest {

    private RoleRepo rolerepo;

    @BeforeAll
    public void setUp() throws Exception {
        AppInit.init();
        rolerepo = new RoleRepo();
    }

//    @Test
//    public void simple() throws IOException, SQLException {
//        String prefix = "cachetest";
//        int count = 10;
//        RoleRepoTest.deleteTestRoles(prefix);
//        RoleRepoTest.insertTestRoles(prefix, count);
//        IReadonlyCache<Role, String> _cache = ECacheFactory.addTableCache(Role.class);
//
//        List<Role> l = _cache.findAll();
//        Assert.assertNotNull(l);
//        List<Role> l2 = rolerepo.findAll();
//        Assert.assertEquals(l.size(), l2.size());
//
//        var f1 = l.stream().filter(r -> r.getName().startsWith(prefix)).collect(Collectors.toList());
//        Assert.assertEquals(f1.size(), count);
//
//        Role r1 = _cache.findOne(prefix + 1);
//        Assert.assertNotNull(r1);
//
//        Role r2=_cache.findOne(prefix+count*9);
//        Assert.assertNull(r2);
//    }
}
