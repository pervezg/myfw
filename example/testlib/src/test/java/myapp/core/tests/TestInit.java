package myapp.core.tests;

import my.fw.base.app.App;
import my.fw.base.system.MyProperties;
import myapp.constants.MyAppConst;
import myapp.factory.ExampleAppInit;
import org.apache.commons.lang3.StringUtils;

import java.util.Properties;

// initialization for test classes
public class TestInit {

    public static void initTestEnv(){
        Properties props = TestEnvSettings.mysqlLocalProps();
        initTestEnv(null,true);
    }
    public static void initTestEnv(String testname,boolean initlogs){
        Properties props = TestEnvSettings.mysqlLocalProps();
        // add env props so that we can override settings
        Properties envprops = MyProperties.env2Props(MyAppConst.appCode,true);
        if(envprops.size()<1) System.out.println("initTestEnv No env settings detected");
        envprops.forEach((k,v)->System.out.println(" initTestEnv Env prop:  "+k+ " = "+v));
        props.putAll(envprops);
        initFromProps(testname,props,initlogs,true);
    }

    // init using stadard dev settings
    public static App init(String testname, boolean initLogs, boolean initDB) {
        App app = getApp(testname);
        return ExampleAppInit.initApp(app, initLogs, initDB);
    }

    public static App initFromProps(String testname, Properties props, boolean initLogs, boolean initDB) {
        App app = getApp(testname);
        return ExampleAppInit.initAppFromProps(app, props, initLogs, initDB);
    }

    public static App getApp(String testname) {
        String module = StringUtils.isBlank(testname) ? "coretest" : testname;
        return new App(testname, MyAppConst.appCode);
    }


}
