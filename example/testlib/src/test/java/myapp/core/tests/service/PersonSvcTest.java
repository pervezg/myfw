package myapp.core.tests.service;

import myapp.core.tests.TestInit;
import myapp.model.Person;
import myapp.service.PersonService;
import myapp.test.testlib.helpers.TestHelper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.dao.DuplicateKeyException;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)

public class PersonSvcTest {
    PersonService personService;
    String namePrefix = "svctest";

    @BeforeAll
    public void setUp() throws Exception {
        TestInit.initTestEnv();
        personService = new PersonService();
    }

    @Test
    void getAll() {
        List<Person> l = personService.findAll();
        assertNotNull(l);
    }

    @Test
    void insertPerson() {

        Person px = personService.findByUserid(namePrefix);
        if (px != null) personService.delete(px.getId());
        // 1 . insert a new rec
        Person p1 = insertPersonTest(namePrefix);

        // now delete
        int i = personService.delete(p1.getId());
        assertEquals(i, 1);

        // insert again
        p1 = insertPersonTest(namePrefix);

        // now test for duplicates
        DuplicateKeyException ex = assertThrows(DuplicateKeyException.class, () -> insertPersonTest(namePrefix));

    }

    Person insertPersonTest(String userid) {
        Person p = TestHelper.newPerson(userid);
        p = personService.save(p);
        assertEquals(p.getUserid(), userid);
        assertTrue(p.getId() > 0);
        return p;
    }
}
