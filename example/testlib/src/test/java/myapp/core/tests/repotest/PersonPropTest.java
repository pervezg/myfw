package myapp.core.tests.repotest;

import my.fw.sqlrepo.EntityInfoFactory;
import my.fw.sqlrepo.service.GenericService;
import myapp.core.tests.TestInit;
import myapp.model.Person;
import myapp.model.PersonProps;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.List;
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class PersonPropTest<PARENT> {
    private GenericService<PersonProps, Integer> svc;

    @BeforeAll
    public void setUp() throws Exception {
        TestInit.initTestEnv();
        svc = new GenericService<PersonProps, Integer>(PersonProps.class, EntityInfoFactory.getEntityInfo(PersonProps.class));
    }



    @Test
    public void Simple() {
        // var l = svc.findAll();
        var repo = svc.getRepo();
        var childei = EntityInfoFactory.getEntityInfo(PersonProps.class);
        repo.setChildmapper((rs, i) -> EntityInfoFactory.reflectionMapper(PersonProps.class, rs, childei));

        var parentei = EntityInfoFactory.getEntityInfo(Person.class);
        var sql = "SELECT * FROM PERSON LEFT JOIN  PERSON_PROPS on PERSON.id=PERSON_PROPS.personid";
      //  var sql1 = "SELECT PERSON.* , PERSON_PROPS.* FROM PERSON LEFT JOIN  PERSON_PROPS on PERSON.id=PERSON_PROPS.personid";

        //SqlRowSet rs = repo.query4rs(sql);
        List<Person> res = repo.joinQuery(sql, (rs)->EntityInfoFactory.extractData(Person.class,PersonProps.class,rs));
        //  List<Map<String, Object>> res1 = repo.queryres(sql1, new rse());

    }

}
