package myapp.core.tests;

import my.fw.sqlrepo.db.DBConnHelper;

import java.util.Properties;

public class TestEnvSettings {
    public static String apiurl="myapp";
    public static class DB {
        public static String dsNAme = "spring_data_jdbc_repository_test";
        public static String Schema = "myexample";
        public static String Host = "192.168.29.191";
        public static int Port = 3306;
        public static String DbUser = "root";
        public static String DbPwd = "Netc_1234";
    }

    public static Properties basicProps(){
        Properties props  = new Properties();
        props.setProperty("home","/home/example");
        props.setProperty("log.level","INFO");


        return props;
    }
    public static Properties mysqlLocalProps(){
        Properties props  = basicProps();
        props.setProperty("datasource."+ DBConnHelper.PropNames.url,DBConnHelper.mysqlUrl("localhost",0,DB.Schema));
        props.setProperty("datasource."+ DBConnHelper.PropNames.username,DB.DbUser);
        props.setProperty("datasource."+ DBConnHelper.PropNames.password,DB.DbPwd);
        props.setProperty("datasource."+ DBConnHelper.PropNames.driverClassName,DBConnHelper.MySqlConsts.driver);
        return props;
    }
}
