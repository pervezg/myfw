package myapp.core.tests;

import my.fw.base.date.MYDate;
import myapp.model.Person;
import myapp.repo.PersonRepo;
import myapp.repo.PersonRoleRepo;
import myapp.service.PersonService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.springframework.data.domain.Sort.Direction.ASC;
import static org.junit.jupiter.api.Assertions.*;
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class PersonRepoTest {
    private static final String LASTNAME = "sample";
    PersonRepo repo;
    private PersonService svc;

    @BeforeAll
    public void setUp() throws Exception {
        //Server server = Server.createTcpServer().start();
        TestInit.initTestEnv();
        repo = new PersonRepo();
        svc = new PersonService();
    }

    @Test
    public void find1Col() {
        setupData();
        long i1 = repo.count();
        i1 = repo.count("status", "ACTIVE");
        i1 = repo.count("lastname", "sample");
        i1 = repo.count("reportsto", "0");
        i1 = repo.count("reportsto", "1");

        List<Person> l1 = repo.findAll("id", 64);
        List<Person> l2 = repo.findAll("userid", "admin");
        assertNotNull(l2);
        ;
        assertEquals(1, l2.size());
        assertEquals("admin", l2.get(0).getUserid());

        l1 = repo.findAll("userid", "admin", "status", "ACTIVE");
        assertEquals(1, l1.size());

        l1 = repo.findAll("userid", "admin", "status", "XXXX");
        assertEquals(0, l1.size());

        l1 = repo.findLike1("fullname", "adm");
        l2 = repo.findLike("fullname", "adm");
        assertNotNull(l2);


    }

    //@Test
    public void pagingTest() throws SQLException, IOException {

        //ScriptRunner sr = new ScriptRunner(DatabaseConnectionManager.getDS().getConnection(),true,true);
        //sr.runScript(new StringReader("Delete from MYFW.PERSON "));
        //addMany("pagin
        // g","paging",100);

        Sort so =  Sort.by(new Sort.Order(ASC, "fname"));
        Page<Person> pp = null;
        pp = repo.findWhereClause("status='ACTIVE'", PageRequest.of(0, 10), so);
        pp = repo.findWhereClause("status='ACTIVE'", PageRequest.of(1, 10),so);
        pp = repo.findWhereClause("status='ACTIVE'", PageRequest.of(2, 10),so);
        assertTrue(pp.getTotalElements() == 10);
    }

    //@Test
    public void jointest() {
        setupData();
        List<Person> l = repo.findWithInnerJoin("userid", "admin", "id", "PERSON_ROLE", "personid");

        l = repo.findWhereChild("rolename", "support", "id", "PERSON_ROLE", "personid");
        l = svc.findHavingRole("admin");
        l = svc.findHavingRole("support");

        assertNotNull(l);

    }

    @Test
    public void personRoleTest(){
     var   prrepo = new PersonRoleRepo();
     var l = prrepo.findAll();


    }

    @Test
    public void dateSearch() {
        deleteall(LASTNAME);
        Person p1 = newPerson("first", "first", LASTNAME);
        Person p2 = newPerson("second", "second", LASTNAME);
        Person p3 = newPerson("third", "third", LASTNAME);
        p1.setStartDate(new Date("2000/01/01"));
        p2.setStartDate(new Date("2000/10/01"));
        p3.setStartDate(new Date("2000/12/01"));

        p1 = repo.save(p1);
        p2 = repo.save(p2);
        p3 = repo.save(p3);
        List<Person> l1 = repo.findBeforeDate("startdate", new Date("2000/05/01"));
        List<Person> l2 = repo.findAfterDate("startdate", new Date("2000/05/01"));
        List<Person> l3 = repo.findBetweenDates("startdate", new Date("2000/02/01"), new Date("2000/12/01"));

    }

    public void setupData() {
        addMany("paging", "paging", 100);
        //p = repo.save(p);
    }


    public List<Person> addMany(String prefix, String lastname, int count) {
        List<Person> l = new ArrayList<>();
        deleteall(lastname);
        for (int i = 0; i < count; i++) {
            String uid = prefix + i;
            Person p = newPerson(uid, "myapp/core/tests", lastname);
            p = svc.save(p);
            l.add(p);
        }
        return l;
    }

    public void deleteall(String lastname) {
        List<Person> l1 = repo.findAll("lastname", lastname);
        l1.forEach(t -> repo.delete(t.getId()));
    }

    public Person newPerson(String userid, String f, String l) {
        Person p = new Person();
        p.setId(0);
        p.setUserid(userid);
        p.setStatus("ACTIVE");
        p.setFirstname(f);
        p.setLastname(l);
        p.setFullname(f + " " + l);
        p.setEmail(userid + "@email.com");
        p.setReportsto(0);
        p.setStartDate(MYDate.today());
        p.setCreatedBy("prersonrepotest");
        return p;
    }

}
