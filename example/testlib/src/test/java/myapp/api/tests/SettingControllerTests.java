package myapp.api.tests;

import myapp.api.MyExampleAPIApplication;
import myapp.factory.ExampleAppInit;
import myapp.test.testlib.TestInit;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

//https://reflectoring.io/spring-boot-test/
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = MyExampleAPIApplication.class)
@AutoConfigureMockMvc
class SettingControllerTests {
    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        TestInit.initTestEnv();
    }

    @Test
    @Disabled
    public void settingsControllerTestWithMocMvc() throws Exception {
        String propname = "ramdomprop";
        String val = RandomStringUtils.random(10);
        String home = ExampleAppInit.thisApp.getProps().getProperty("home");

        mockMvc.perform(get("/settings/props")).andExpect(status().isOk()).andExpect(content().string(home));
        ExampleAppInit.thisApp.getProps().setProperty(propname, val);
        mockMvc.perform(get("/settings/props")).andExpect(status().isOk()).andExpect(content().string(val));

    }
}
