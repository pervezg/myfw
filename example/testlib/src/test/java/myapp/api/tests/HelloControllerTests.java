package myapp.api.tests;

import myapp.api.MyExampleAPIApplication;
import myapp.api.controllers.HelloController;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = MyExampleAPIApplication.class)
@AutoConfigureMockMvc
class HelloControllerTests {
    String name = "someone";
    String bigname = "someoneWithBigNameMoreThan20";
    String badname = "some_!-+";
    @Autowired
    HelloController helloController;
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void contextLoads() {
    }

    // direct call to controller method
    @Test
    void helloControllerTestWithAutowiredOK() throws Exception {

        assertThat(helloController).isNotNull();
        String x = helloController.helloString(null);
        assertThat(x).isEqualTo("hello");
        assertThat(helloController.helloString(name)).isEqualTo("hello " + name);
        testComplete();
    }

    @Test
    void helloControllerTestWithAutowiredBigNameException() {
        assertThatExceptionOfType(javax.validation.ConstraintViolationException.class)
                .isThrownBy(() -> helloController.helloString(bigname));
        testComplete();
    }

    @Test
    void helloControllerTestWithAutowiredBadNameException() {
        assertThatExceptionOfType(javax.validation.ConstraintViolationException.class)
                .isThrownBy(() -> helloController.helloString(badname));
        testComplete();
    }


    // via mvc
    @Test
    void helloControllerTestWithMocMvc() throws Exception {

        mockMvc.perform(get("/hello")).andExpect(status().isOk()).andExpect(content().string("hello"));
        mockMvc.perform(get("/hello" + "/" + name)).andExpect(status().isOk()).andExpect(content().string("hello" + " " + name));
        testComplete();
    }
 @Test
    void helloControllerTestWithMocMvcValidationError() throws Exception {
        mockMvc.perform(get("/hello" + "/" + badname)).andExpect(status().is(HttpStatus.CONFLICT.value()));
        mockMvc.perform(get("/hello" + "/" + bigname)).andExpect(status().is(HttpStatus.CONFLICT.value()));
        testComplete();
    }

    void testComplete() {
        System.out.println("Test Completed ------");
    }

}
