package myapp.test.testlib.helpers;

import myapp.model.Person;

public class TestHelper {
    public static Person newPerson(String f, String l, String userid, int r) {
        Person p = new Person();
        p.setId(0);
        p.setUserid(userid);
        p.setStatus("ACTIVE");
        p.setFirstname(f);
        p.setLastname(l);
        p.setFullname(f + " " + l);
        p.setEmail(f + "." + l + "@email.com");
        p.setReportsto(r);
        return p;
    }

    public static Person newPerson(String f, String l, String userid) {
        return newPerson(f, l, userid, 0);
    }

    public static Person newPerson(String userid) {
        return newPerson(userid+ "first",  userid+" lname",userid, 0);
    }
}
