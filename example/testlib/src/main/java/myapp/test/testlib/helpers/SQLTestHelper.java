package myapp.test.testlib.helpers;

import my.fw.sqlrepo.db.DatabaseConnectionManager;
import my.fw.sqlrepo.sql.ScriptRunner;

import java.io.StringReader;
import java.sql.SQLException;

public class SQLTestHelper {

    static ScriptRunner sqlRunner;

    static {
        try {
            sqlRunner = new ScriptRunner(DatabaseConnectionManager.getDS().getConnection(), true, true);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public SQLTestHelper() throws SQLException {
    }

    public static void deleteLike(String table,String col, String prefix) {
        String delete = "delete from " + table + " where "+col+" like '%" + prefix + "%'; \n";
        runSQL(delete);
    }

    public static void runSQL(String sql) {
        try {
            sqlRunner.runScript(new StringReader(sql));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }
}
