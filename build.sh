# runs maven to copy all jar/war files from dependent project into the 'dist' directory
# https://stackoverflow.com/questions/71069/can-maven-be-made-less-verbose
dist=`readlink -f ./dist`
echo Building myfw  projects
export mvnopts="-DskipTests -B"
export JAVA_HOME=/usr/lib/jvm/java-11
mvn $mvnopts clean install  $*

git status >dist/gitinfo.txt
git log -1  >>dist/gitinfo.txt
