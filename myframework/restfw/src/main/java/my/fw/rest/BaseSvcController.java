package my.fw.rest;

import javax.servlet.http.HttpSession;

import my.fw.error.MyErrors;
import my.fw.model.Result;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import my.fw.api.model.BaseWebResponse;
import my.fw.service.ISimpleSvc;

@RestController
public class BaseSvcController<T, ID> extends BaseRestController {
	protected ISimpleSvc<T, ID> _svc;
	public boolean readOnlyMode = false;

	public BaseSvcController() {

	}
	public BaseSvcController(ISimpleSvc<T, ID> svc) {
		this._svc = svc;
	}
	// findall and return as webresponse
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public BaseWebResponse findAll(HttpSession session) {
		String _method = "findAll: ";
		boolean f = isAuthenticated();
		try {
			Iterable<T> l = _svc.findAll();
			return SuccessResponse(l, null);
		} catch (Exception e) {
			log.error(_method+ "Exception :",e);
			return ErrorResponse(e, _method);
		}

	}

	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public BaseWebResponse findOne(@PathVariable("id") ID id) {
		String _method = "findOne: " ;

		try {
			T t = _svc.findOne(id);
			return SuccessResponse(t, null);
		} catch (Exception e) {
			log.error(_method+ "Exception :",e);
			return ErrorResponse(e, _method);
		}

	}

	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public BaseWebResponse delete(@PathVariable("id") ID id) {
		String _method = "delete: ";

		try {
			int i = _svc.delete(id);
			return SuccessResponse(i, null);
		} catch (Exception e) {
			log.error(_method+ "Exception :",e);
			return ErrorResponse(e, _method);
		}

	}

	@PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public BaseWebResponse update(@RequestBody T t) {
		String _method = "update: ";
		try {
			// ideally we should be comparing current and new object and only updating
			// changed fields
			Result r = _svc.validate(t);
			if (!r.isSuccessful())
				return ErrorResponse(r);
			int i = _svc.update(t);
			if (i == 0) {
				// send an error or notfound????
			}
			T tt = _svc.findOne(_svc.getID(t));
			return SuccessResponse(tt, null);
		} catch (Exception e) {
			log.error(_method+ "Exception :",e);
			return ErrorResponse(e, _method);
		}

	}

	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public BaseWebResponse save(@RequestBody T t) {
		Result r = saveReturnResult(t);
		return Response(r);
	}

	// does save with try/catch and sends more sensible result
	private Result saveReturnResult(T t) {

		String _method = "save: ";
		try {
			t = _svc.setDefaults(t);
			Result r = _svc.validate(t);
			if (!r.isSuccessful())
				return r;
			T tt = _svc.save(t);
			return Result.SuccessWithData(tt);
		} catch (DuplicateKeyException e) {
			log.error(_method+ "Exception :",e);
			return Result.Error(e, "Duplicate");
		} catch (Exception e) {
			log.error(_method+ "Exception :",e);
			return Result.Error(e, "Exception");
		}

	}

	private Result updateReturnResult(T t,boolean keepNulls){
		String _method = "update: ";

		try {
			ID id = _svc.getID(t);
			Result r = _svc.validate(t);
			if (!r.isSuccessful())
				return r;
			int i = _svc.update(t);
			if (i == 0) {
				return Result.Error(MyErrors.NotFound,"Object not found , cannot update. id: "+id);
			}
			T tt = _svc.findOne(_svc.getID(t));
			return Result.SuccessWithData(tt);
		} catch (Exception e) {
			log.error(_method+ "Exception :",e);
			return Result.Error(e, "Exception");
		}
	}
}
