package my.fw.rest.reports;

import my.fw.reports.*;
import my.fw.service.ISimpleSvc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

// base reports controller , should be extended by application's reports controller
public class ReportsController {
    static Logger log = LoggerFactory.getLogger(ReportsController.class);

    private static ReportRunner reportRunner;
    private static ISimpleSvc<QueryDef, String> repSvc;

    public ReportsController() {

    }

    public ReportsController(ISimpleSvc<QueryDef, String> rRepo) {
        init(rRepo);
    }

    //  init reports info
    public void init(ISimpleSvc<QueryDef, String> rsvc) {
        setSvc(rsvc);
    }

    public void setSvc(ISimpleSvc<QueryDef, String> rsvc) {
        repSvc = rsvc;
        reportRunner = new ReportRunner(rsvc);
    }

    @GetMapping
    public Iterable<QueryDef> getAll() {
        return repSvc.findAll();
    }

    @GetMapping("/{name}")
    public QueryDef getOne(@PathVariable String name) {
        return repSvc.findOne(name);
    }


    // run method for 'regular' users , added userid etc to params
    // we can add a separate run action for admin users in future
    @PostMapping("run")
    public QueryResult run(@RequestBody QueryRequest rr, @RequestHeader Map<String, String> headers) {
        // add standard parameters to the report request

        QueryResult result = runReport(rr, headers);
        return result;
    }


    private QueryResult runReport(QueryRequest rr, Map<String, String> headers) {
        addStdParams(rr, headers);
        return reportRunner.runReport(rr);
    }


    // add userid and role if available
    // hack - need to check if headers are case sensitive since we only get lowercase names with spring
    static QueryRequest addStdParams(QueryRequest rr, Map<String, String> headers) {
        if (rr.params == null) rr.params = new HashMap<>();
        String s = getHeader("userid", headers);
        if (s == null) {
            s = getHeader("user_id", headers);
        }
        if (s != null) {
            rr.params.put("user_id", s);
        }

        return rr;
    }

    private static String getHeader(String name, Map<String, String> headers) {
        String s = headers.get(name);
        if (s == null || "null".equals(s) || s == "") return null;
        return s;
    }


}

