package my.fw.rest.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import java.util.Map;
import java.util.Set;
// shows various spring config
public class SpringConfigController {


    @Autowired
    private RequestMappingHandlerMapping requestMappingHandlerMapping;

    //return all configured controllers/actions
    @RequestMapping( value = "endPoints", method = RequestMethod.GET )
    public Set<RequestMappingInfo> getEndPointsInView(Model model )
    {
        Map<RequestMappingInfo, HandlerMethod> routes = requestMappingHandlerMapping.getHandlerMethods();
        return  routes.keySet();
    }
}
