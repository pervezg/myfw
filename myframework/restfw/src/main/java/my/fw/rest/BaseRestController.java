package my.fw.rest;

import my.fw.api.model.BaseWebResponse;
import my.fw.model.BaseObject;
import my.fw.model.OPSession;
import my.fw.model.Result;
import my.fw.servlet.ServletHelper;

// contains common helper methods 
public class BaseRestController extends BaseObject {
	// used to disable updates
	protected boolean isReadonly = false;

	// Authentication method
	protected boolean isAuthenticated() {
		OPSession op = getOPSession();
		// TODO Pervez we should open the session and check for timeout
		return (op != null);
	}

	protected OPSession getOPSession() {
		return ServletHelper.retreiveSession(SpringRestHelper.getHttpSession());
	}

	protected BaseWebResponse Response(Result r) {
		return new BaseWebResponse(r);
	}

	protected BaseWebResponse ErrorResponse(Result r) {
		return new BaseWebResponse(r.isSuccessful(), r.message);
	}

	protected BaseWebResponse ErrorResponse(Exception e, String msg) {
		return  BaseWebResponse.Error(e,msg);
	}

	protected BaseWebResponse SuccessResponse(Object o, String msg) {
		BaseWebResponse r = new BaseWebResponse(true, msg);
		r.data = o;
		return r;
	}

}
