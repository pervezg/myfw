package my.fw.sqlrepo.sql;

import my.fw.model.BaseObject;
import my.fw.model.Result;
import org.slf4j.Logger;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import javax.sql.DataSource;
import java.lang.reflect.InvocationTargetException;

// helper methods for running and checking transactions
// since TransTemplate is tied to a datasource, the txnrunner is ds specific

public class TxnRunner extends BaseObject {
    private static final boolean transactionDebugging = true;
    private static final boolean verboseTransactionDebugging = true;
    private DataSourceTransactionManager tm;
    private DataSource ds;
    private TransactionTemplate tt;


    public TxnRunner(DataSource ds) {
        init(ds);
    }

    public TxnRunner(DataSource ds, Logger log) {
        this(ds);
        this.log=log;
    }

    private void init(DataSource ds) {
        this.ds = ds;
        this.tm = new DataSourceTransactionManager(ds);
        this.tt = new TransactionTemplate(tm);
    }

    public TransactionTemplate getTxnTemplate() {
        return tt;
    }

    // run the action in a transaction
    public Result runInTxn(TransactionTemplate transactionTemplate, TransactionCallback<Result> action) {
        final String _method = "runInTxn";
        try {
            TRACE(_method, "ready to run action ");
            Result r = transactionTemplate.execute(action);
            return r;
        } catch (Exception e) {
            ERROR( _method, "Exception executing transaction. See the Result object for full execption");
            return Result.Error(e, "Exception executing transaction");
        }
    }

    public Result runInTxn(TransactionCallback<Result> action) {
        return runInTxn(tt, action);
    }

    public static void showTransactionStatus(String message) {
        System.out.println(((transactionActive()) ? "[+] " : "[-] ") + message);
    }

    // Some guidance from: http://java.dzone.com/articles/monitoring-declarative-transac?page=0,1
    public static boolean transactionActive() {
        try {
            ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
            Class tsmClass = contextClassLoader.loadClass("org.springframework.transaction.support.TransactionSynchronizationManager");
            Boolean isActive = (Boolean) tsmClass.getMethod("isActualTransactionActive", null).invoke(null, null);

            return isActive;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }

        // If we got here it means there was an exception
        throw new IllegalStateException("ServerUtils.transactionActive was unable to complete properly");
    }

    public static void transactionRequired(String message) {
        // Are we debugging transactions?
        if (!transactionDebugging) {
            // No, just return
            return;
        }

        // Are we doing verbose transaction debugging?
        if (verboseTransactionDebugging) {
            // Yes, show the status before we get to the possibility of throwing an exception
            showTransactionStatus(message);
        }

        // Is there a transaction active?
        if (!transactionActive()) {
            // No, throw an exception
            throw new IllegalStateException("Transaction required but not active [" + message + "]");
        }
    }
}
