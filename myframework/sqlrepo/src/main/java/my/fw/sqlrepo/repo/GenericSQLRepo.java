package my.fw.sqlrepo.repo;

import my.fw.sqlrepo.db.DBConnHelper;
import my.fw.sqlrepo.db.DatabaseConnectionManager;
import my.fw.model.BaseObject;
import my.fw.model.IEntityWithId;
import my.fw.sqlrepo.*;
import my.fw.sqlrepo.sql.SQLDialect;
import my.fw.sqlrepo.sql.SQLGenerator;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;

public class GenericSQLRepo<T, ID> extends BaseObject implements ISQLRepo<T, ID>, AutoCloseable {

    // jdbc objects
    @Autowired
    protected DataSource ds;
    protected JdbcTemplate jdbcTemplate;

    // used in stored procedure call to set schema name
    // postgres needs both schema and catalog
    protected String catalogName;
    protected String schemaName;

    // name of table for this repo
    protected String tableName;

    // column name of primary key
    protected String PKName;

    // set to true to populate automatically generated id
    protected Boolean PKIsAutogen = false;
    // mapper object for this entity
    private RowMapper<T> mapper;
    private RowUnmapper<T> unmapper;

    private Class<T> klass;

    public void setChildmapper(RowMapper<T> childmapper) {
        this.childmapper = childmapper;
    }

    private RowMapper<T> childmapper;
    // query strings for various methods
    private String q_GetByName;
    private String q_where_byname;
    private String q_selectStar;
    private String q_delete;

    SQLGenerator SQL_;
    private SQLDialect dialect;

    public GenericSQLRepo() {
    }

    public GenericSQLRepo(String tableName, String PKname, RowMapper<T> mapper) {
        this(tableName, PKname, mapper, DatabaseConnectionManager.getDS());
    }

    public GenericSQLRepo(String tableName, String PKname, RowMapper<T> mapper, RowUnmapper<T> _unmapper, DataSource ds) {
        this(tableName, PKname, mapper, ds);
        this.unmapper = _unmapper;
    }

    public GenericSQLRepo(String tableName, String PKname, RowMapper<T> mapper, RowUnmapper<T> _unmapper) {
        this(tableName, PKname, mapper, DatabaseConnectionManager.getDS());
        this.unmapper = _unmapper;
    }

    public GenericSQLRepo(String tableName, String PKname, RowMapper<T> mapper, DataSource _ds) {
        this.tableName = tableName.trim();
        this.PKName = PKname.trim();
        this.mapper = mapper;
        createQueries();
        setDS(_ds);
        TRACE("CTOR", "Generic Repo create for table :" + tableName + " PK: " + PKname);
    }

    public GenericSQLRepo(Class<T> klass, EntityInfo einfo) {
        init(klass, einfo.tableName, einfo.pk, einfo.pkAutogen, (rs, i) -> EntityInfoFactory.lambdaMapper(klass, rs, einfo),
                (t) -> EntityInfoFactory.lambdaUnmapper(klass, einfo, t), DatabaseConnectionManager.getDS());
    }

    public GenericSQLRepo(Class<T> klass, EntityInfo einfo, DataSource _ds) {
        init(klass, einfo.tableName, einfo.pk, einfo.pkAutogen, (rs, i) -> EntityInfoFactory.lambdaMapper(klass, rs, einfo),
                (t) -> EntityInfoFactory.lambdaUnmapper(klass, einfo, t), _ds);
    }

    public void init(Class<T> klass, String tableName, String PKname, boolean pkautogen, RowMapper<T> _mapper, RowUnmapper<T> _unmapper, DataSource _ds) {
        this.klass = klass;
        this.tableName = tableName.trim();
        this.PKName = PKname.trim();
        this.PKIsAutogen = pkautogen;
        this.mapper = _mapper;
        this.unmapper = _unmapper;
        createQueries();
        setDS(_ds);
        TRACE("CTOR", "Generic Repo create for table :" + tableName + " PK: " + PKname);
    }


    @Override
// only close connection, ds does not need to be closed
    public void close() throws Exception {
        // we dont have anything to close if we are not holding a connection , so just
        // use this for tracing
        TRACE("close", "Closing repo");
    }

    private void createQueries() {
        q_where_byname = " where " + PKName + " = ? ";
        q_selectStar = "select " + tableName + ".* from " + tableName + "  ";
        q_GetByName = q_selectStar + q_where_byname;
        q_delete = "delete  from " + tableName + "  ";
        /// q_count = "select count(*) from " + tableName;
    }

    private void setDS(DataSource _ds) {
        if (_ds == null) {
            // get the default ds
            _ds = DatabaseConnectionManager.getDS();
        }
        this.ds = _ds;
        if (ds == null) {
            // throw new Exception("unable to determine datasource");
        }
        try {
            Connection conn = ds.getConnection();
            jdbcTemplate = new JdbcTemplate(ds);
            this.catalogName = conn.getCatalog();
            this.schemaName = conn.getSchema();
            this.dialect = DBConnHelper.getSQLDialect(conn);
            this.SQL_ = new SQLGenerator(dialect, this.tableName);
            conn.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    ///// - main repo methods

    public DataSource getDS() {
        return this.ds;
    }

    @Override
    public String getTableName() {
        return this.tableName;
    }

    @Override
    public String getPKColumnName() {
        return this.PKName;
    }

    public boolean exists(ID id) {
        Integer i = countWhere(SQL_.equals(PKName), id);
        return i > 0;
    }

    public long count(String colName, String value) {
        return countWhere(SQL_.equals(colName), value);
    }

    public Integer countWhere(String where, Object... args) {
        String sql = SQL_.count(tableName) + SQL_.where(where);
        return jtCount(sql, args);
    }

    @Override
    public long count() {
        return jtCount(SQL_.count(tableName));
    }

    public List<T> find(String colName, String value) {
        return findAll(colName, value);
    }

    public List<T> find(String colName, int value) {
        return findAll(colName, value);
    }

    @Override
    public List<T> findAll(String col1Name, String value1) {
        String sql = SQL_.selectStar() + SQL_.where(SQL_.equals(col1Name));
        return jtQuery(sql, new Object[]{value1}, mapper);
    }

    @Override
    public List<T> findAll(String col1Name, int value1) {
        String sql = SQL_.selectStar() + SQL_.where(SQL_.equals(col1Name));
        return jtQuery(sql, new Object[]{value1}, mapper);
    }

    @Override
    public List<T> findAll(String col1Name, String value1, String col2Name, String value2) {
        String where = SQL_.where(SQL_.equals(col1Name) + " AND " + SQL_.equals(col2Name));
        String sql = SQL_.selectStar() + where;
        return jtQuery(sql, new Object[]{value1, value2}, mapper);
    }


    public List<T> findWithInnerJoin(String colName, String value, String pkCol, String fTab, String fCol) {
        return joinInnerWhereParent(colName, value, pkCol, fTab, fCol);
    }

    public List<T> findWhereChild(String colName, String value, String pkCol, String fTab, String fCol) {
        return joinInnerWhereChild(colName, value, pkCol, fTab, fCol);
    }

    // filter table on column and join to foreign tabe
    public List<T> joinInnerWhereParent(String colName, String value, String pkCol, String fTab, String fCol) {
        String join = SQL_.joinInner(pkCol, fTab, fCol);
        String where = SQL_.where(SQL_.equals(colName));
        String sql = SQL_.selectStar() + join + where;
        return jtQuery(sql, new Object[]{value}, mapper);
    }

    public List<T> joinInnerWhereChild(String colName, String value, String pkCol, String fTab, String fCol) {
        String join = SQL_.joinInner(pkCol, fTab, fCol);
        String where = SQL_.where(SQL_.equals(fTab, colName));
        String sql = SQL_.selectStar() + join + where;
        return jtQuery(sql, new Object[]{value}, mapper);
    }

    @Override
    public T findOne(ID _id) {
        String sql = SQL_.selectStar() + SQL_.where(SQL_.equals(PKName));
        List<T> l = jtQuery(sql, new Object[]{_id}, mapper);

        if (l != null) {
            int i = l.size();
            if (i > 1) {
                // throw new Exception( "Query by primary key " + PKName + " on table " +
                // tableName + " returned more than 1 rows");
            }
            if (i == 1)
                return l.get(0);
        }
        return null;
    }

    public T findOne(String col, String value) {
        String sql = SQL_.selectStar() + SQL_.where(SQL_.equals(col));
        List<T> l = jtQuery(sql, new Object[]{value}, mapper);

        if (l != null) {
            int i = l.size();
            if (i > 1) {
                // throw new Exception( "Query by primary key " + PKName + " on table " +
                // tableName + " returned more than 1 rows");
            }
            if (i == 1)
                return l.get(0);
        }
        return null;
    }

    public List<T> findLike(String colName, String colValue) {
        String val = "%" + colValue.trim() + "%";
        String sql = SQL_.selectStar() + SQL_.where(SQL_.like(colName));
        return jtQuery(sql, new Object[]{val}, mapper);
    }

    public List<T> findLike1(String colName, String colValue) {
        String val = "%" + colValue.trim() + "%";
        String sql = SQL_.selectStar() + " where " + colName + " like ?";
        return jtQuery(sql, new Object[]{val}, mapper);
    }

    public List<T> findAll() {
        return jtQuery(SQL_.selectStar(), new Object[]{}, mapper);
    }

    // find with children
    // sql should contain the full join
    public List<T> findWithChildren(String sql, EntityInfo childei, Object... args) {

        return jtQuery(sql, args, mapper)
                ;
    }

    @Override
    // run select with the provided where clause and params
    // this is useful if we want sorting etc.
    public List<T> findWhereClause(String where, Object[] params) {
        if (!where.toLowerCase().trim().startsWith("where"))
            where = " where " + where;
        String sql = q_selectStar + where;
        return jtQuery(sql, params, mapper);
    }

    public List<T> findWhereClause(String where) {
        return findWhereClause(where, new Object[]{});
    }

    public T save(T t) {
        String _method = "save";
        if (unmapper == null)
            throw new RuntimeException("Insert into table " + tableName + " is not possible. rowunmapper is missing");
        Map<String, Object> map = unmapper.mapColumns(t);
        int fieldcount = map.size();
        SimpleJdbcInsert simpleJdbcInsert = getInsertTemplate().withTableName(this.tableName);
        if (PKIsAutogen) {
            simpleJdbcInsert.usingGeneratedKeyColumns(PKName);
            Number _key = simpleJdbcInsert.executeAndReturnKey(map);
            if (t instanceof IEntityWithId) {
                IEntityWithId _e = (IEntityWithId) t;
                _e.setId(_key.intValue());

                TRACE(_method, "Insert to table {} : {} fields , completed .Autogenerated Id set to {} ", tableName,
                        fieldcount, _key);

            } else {
                TRACE(_method,
                        "ERROR : Insert to table {} was done for an object without IEntityWithId interface . The returned object does not have the autogenerated id  {} ",
                        tableName, _key);
            }
        } else {
            int x = simpleJdbcInsert.execute(map);
            TRACE(_method, "Insert to table {} : {} fields , resulted in {} rows. ", tableName, fieldcount, x);
        }
        return t;
    }

    private SimpleJdbcInsert getInsertTemplate() {
        SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(this.ds);
        if (StringUtils.isNotBlank(this.catalogName)) {
            simpleJdbcInsert.withCatalogName(this.catalogName);
        }
        if (StringUtils.isNotBlank(this.schemaName)) {
            simpleJdbcInsert.setSchemaName(this.schemaName);
        }
        return simpleJdbcInsert;
    }

    public int deleteByName(String _name) {
        String query = q_delete + q_where_byname;
        try {
            return jtUpdate(query, new Object[]{_name});
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    public int delete(ID _name) {
        String sql = q_delete + q_where_byname;
        try {
            TRACE("update", sql);
            return jtUpdate(sql, new Object[]{_name});
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    // update single col using PK
    public int updateByID(ID id, String colname1, String colvalue1) {
        String sql = "update table " + tableName + " set " + colname1 + " ? where " + PKName + "= ?  ";
        TRACE("update", sql);
        return jtUpdate(sql, new Object[]{colvalue1, id});
    }

    // update single col using PK
    public int update(T t) {
        return update(t, null, false);
    }

    // update table using value from object , but only those columns named in
    // collection
    public int update(T t, Collection<String> colsToUpdate, boolean includeNulls) {

        Map<String, Object> map = unmapper.mapColumns(t);
        if (map.containsKey(PKName)) {
            ID id = (ID) map.get(PKName);
            return update(id, map, colsToUpdate, includeNulls);
        }
        return 0;
    }

    // update table using value from map , but only those columns named in
    // collection
    public int update(ID id, Map<String, Object> map, Collection<String> colsToUpdate, boolean includeNulls) {

        // remove extra columns if non-zero. zero cols means update all
        if (colsToUpdate != null && colsToUpdate.size() > 0)
            map = selectCols(map, colsToUpdate);

        // remove the PK... we are getting it as the first parameter .. makes code more
        // readable
        // this will prevent updating of PK - which we need to review
        map.remove(PKName);

        // create set clause
        String setClause = setCaluse(map);

        // return if no updates
        if (map.size() == 0)
            return 0;

        List<Object> l = new ArrayList<Object>(map.values());
        // id is added as the the last item since it is the last ? in where clause
        l.add(id);
        String sql = "update  " + tableName + setClause + q_where_byname;
        return jtUpdate(sql, l.toArray());
    }

    // returns a new map with just selected columns
    private Map<String, Object> selectCols(Map<String, Object> map, Collection<String> colsToUpdate) {
        HashMap<String, Object> result = new HashMap<String, Object>();
        HashMap<String, Object> values = new HashMap<String, Object>();
        ArrayList<String> cols = new ArrayList<String>();

        // convert collection to lowercase
        colsToUpdate.forEach(s -> cols.add(s.toLowerCase()));

        // convert the map to lowecase keys
        map.forEach((k, v) -> {
            values.put(k.toLowerCase(), v);
        });
        for (String colname : cols) {
            if (values.containsKey(colname)) {
                result.put(colname, values.get(colname));
            }
        }
        return result;
    }

    // very simple SP call method
    // we need to set catalog name else mysql looks at all databases on the server
    public Map<String, Object> callSP(String spName, Object... args) {
        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(this.ds).withCatalogName(catalogName)
                .withProcedureName(spName);
        return simpleJdbcCall.execute(args);
    }

    private String setCaluse(Map<String, Object> map) {
        String setClause = "  set  ";
        Boolean isfirst = true;
        for (String f : map.keySet()) {
// need to skip primary key
            String c = f.toLowerCase().trim();
            if (!c.equals(PKName)) {
                setClause += isfirst ? " " : ",";
                isfirst = false;
                setClause += c + " = ? ";
            }
        }
        return setClause;
    }

    // common method to trace and run jdbctemplate.update
    private int jtUpdate(String sql, Object... args) {
        if (jdbcTemplate == null)
            jdbcTemplate = new JdbcTemplate(ds);
        // TRACE("UPDATE", sql);
        int i = jdbcTemplate.update(sql, args);
        TRACE("UPDATE", i + " rows : " + sql);
        return i;
    }

    private List<T> jtQuery(String sql, Object[] args, RowMapper<T> _mapper) {
        if (jdbcTemplate == null)
            jdbcTemplate = new JdbcTemplate(ds);
        // TRACE("SELECT", sql);
        List<T> l = jdbcTemplate.query(sql, args, _mapper);
        TRACE("SELECT", " rows:" + l.size() + ":" + sql);
        return l;
    }

    // specialized query that returns int
    private Integer jtCount(String sql, Object... args) {
        TRACE("count", sql);
        Integer i = jdbcTemplate.queryForObject(sql, Integer.class, args);
        TRACE("count", "Count = " + i);
        return i;
    }

    @Override
    public ID getId(T t) {
        return null;
    }


    public List<T> findBetweenDates(String colname, String colvalue, String dateCol, Date startDate, Date endDate) {
        return findBetweenDates(colname, colvalue, dateCol, SQL_.toDbDate(startDate), SQL_.toDbDate(endDate));
    }

    public List<T> findBetweenDates(String dateCol, Date startDate, Date endDate) {
        return findBetweenDates(null, null, dateCol, SQL_.toDbDate(startDate), SQL_.toDbDate(endDate));
    }

    public List<T> findBeforeDate(String dateCol, Date date) {
        return findBeforeDate(null, null, dateCol, SQL_.toDbDate(date));
    }

    public List<T> findBeforeDate(String dateCol, String dateString) {
        return findBeforeDate(null, null, dateCol, dateString);
    }

    public List<T> findAfterDate(String dateCol, Date date) {
        return findAfterDate(null, null, dateCol, SQL_.toDbDate(date));
    }

    public List<T> findAfterDate(String dateCol, String dateString) {
        return findAfterDate(null, null, dateCol, dateString);
    }

    public List<T> findBeforeDate(String colname, String colvalue, String dateCol, String startDate) {
        String where = null;
        String[] args = null;

        String dateClause = SQL_.dateBefore(dateCol);
        if (colname != null) {
            String colClause = SQL_.equals(colname);
            where = colClause + " AND " + dateClause;
            args = new String[]{colvalue, startDate};
        } else {
            where = dateClause;
            args = new String[]{startDate};
        }
        return findWhereClause(where, args);
    }

    // find by single col +date range
    // if colname is null then find by date range
    public List<T> findBetweenDates(String colname, String colvalue, String dateCol, String startDate, String endDate) {
        String where = null;
        String[] args = null;

        String dateClause = SQL_.dateBetween(dateCol);
        if (colname != null) {
            String colClause = SQL_.equals(colname);
            where = colClause + " AND " + dateClause;
            args = new String[]{colvalue, startDate, endDate};
        } else {
            where = dateClause;
            args = new String[]{startDate, endDate};
        }
        return findWhereClause(where, args);
    }

    public List<T> findAfterDate(String colname, String colvalue, String dateCol, String startDate) {
        String where = null;
        String[] args = null;

        String dateClause = SQL_.dateAfter(dateCol);
        if (colname != null) {
            String colClause = SQL_.equals(colname);
            where = colClause + " AND " + dateClause;
            args = new String[]{colvalue, startDate};
        } else {
            where = dateClause;
            args = new String[]{startDate};
        }
        return findWhereClause(where, args);
    }

    public Page<T> findWhereClause(String where, Pageable page, Sort sort) {
        String query = SQL_.selectStar() + SQL_.where(where);
        if (sort != null) {
            String order = SQL_.orderByClause(sort);
            query = query + order;
        }
        if (page != null) {
            query = SQL_.findWhere(query, page);
        }
        ;

        List<T> data = jdbcTemplate.query(query, mapper);
        return new PageImpl<>(data, page, data.size());
    }

    public SqlRowSet query4rs(String sql, Object... args) {
        return jdbcTemplate.queryForRowSet(sql, args);
    }

    public <E> E joinQuery(String sql, ResultSetExtractor<E> rse, Object... args) {
        return jdbcTemplate.query(sql, rse);
    }

}
