package my.fw.sqlrepo.cache;

import my.fw.base.cache.IReadonlyCache;
import my.fw.model.BaseObject;
import my.fw.model.IObjWithId;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

//  findall from table anc cache it
public class GenericSimpleMapCache<T extends IObjWithId<ID>, ID> extends BaseObject implements IReadonlyCache<T, ID> {

    protected Class<T> _class;
    protected String cacheName;
    int maxEntries = 6000;
    private Map<ID, T> _cache;

    public GenericSimpleMapCache() {

    }

    // adding class name to make naming more java centric
    public GenericSimpleMapCache(Class<T> c) {
        this._class = c;
        this.cacheName = c.getName();
        _cache = new HashMap<>();
    }

    public T findOne(ID id) {
        T o = _cache.get(id);
        return o;
    }

    public <T extends IObjWithId<ID>, ID> void loadCache(Map<ID, T> _e, List<T> _all) {
        _e.clear();
        for (T t : _all) {
            _e.put(t.getId(), t);
        }
    }

    public void loadCache(List<T> all) {
        loadCache(this._cache, all);
        TRACE("loadcache", "Loaded cache:" + cacheName + " count:" + all.size());
    }

    public List<T> findAll() {
        return _cache.values().stream().collect(Collectors.toList());
    }

    @Override
    public String getName() {
        return cacheName;
    }
}
