package my.fw.sqlrepo;

import my.fw.sqlrepo.repo.RowUnmapper;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

public class EntityMapper<T> implements RowMapper<T>, RowUnmapper<T> {
    private final Class<T> _class;
    private final EntityInfo ei;

    public EntityMapper(Class<T> klass) {

        this._class = klass;
        this.ei = EntityInfoFactory.getEntityInfo(_class);
    }

    @Override
    public T mapRow(ResultSet rs, int rowNum) throws SQLException {
        return EntityInfoFactory.reflectionMapper(_class, rs, ei);
    }

    @Override
    public Map<String, Object> mapColumns(T t) {
        return EntityInfoFactory.reflectionUmapper(_class,ei,t);
    }
}
