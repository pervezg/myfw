package my.fw.sqlrepo.repo;

import java.util.Map;

// opposite of spring rowmapper , converts pojo to column map
public interface RowUnmapper<T> {
	
	Map<String, Object> mapColumns(T t);

}
