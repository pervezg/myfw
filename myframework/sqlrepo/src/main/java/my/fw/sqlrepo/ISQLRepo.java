package my.fw.sqlrepo;

import my.fw.repo.ISimpleRepo;

import java.util.List;

import javax.sql.DataSource;

// basic sql repo
public interface ISQLRepo<T, ID> extends ISimpleRepo<T, ID> {
	DataSource getDS();

	String getTableName();

	String getPKColumnName();

	// select where col=value
	Iterable<T> findAll(String col1Name, String value1);

	Iterable<T> findAll(String col1Name, int value1);

	Iterable<T> findAll(String col1Name, String value1, String col2Name, String value2);

	Iterable<T> findLike(String col1Name, String value1);

	// find from table using the where string
	// this is useful if we want sorting etc.
	// or we have a complex join with all cols coming from the T table.


	Iterable<T> findWhereClause(String where, Object[] params);


	// int deleteWhere(String where, Object... args);

}
