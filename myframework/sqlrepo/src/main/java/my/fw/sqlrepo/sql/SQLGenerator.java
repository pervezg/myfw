package my.fw.sqlrepo.sql;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

import static java.lang.String.format;

// generates SQL expressions
// supports different dialects
// all sql generated is for param based queries
public class SQLGenerator {

    private String tableName;
    public SQLDialect dialect = SQLDialect.MYSQL;

    public static String mysqlDTformat = "yyyy-MM-dd HH:mm:ss";

    static final String AND = " AND ";
    static final String OR = " OR ";
    static final String COMMA = ", ";
    static final String QQQ = " ? ";
    static final String PARAM = " = ?";
    static final String STAR = "*";

    public static final SimpleDateFormat mysqlSimpleFormatter = new SimpleDateFormat(mysqlDTformat);

    private static final String _like = " ( %s LIKE %s ) ";

    private static final String _ge = " ( %s >= %s ) ";
    private static final String _le = " ( %s <= %s ) ";
    private static final String _between = " ( %s BETWEEN %s AND %s ) ";
    private static final String _equals = " ( %s = %s ) ";
    private static final String _selectAll = " SELECT %s FROM %s ";
    private static final String H2DateFunc = "parsedatetime(  ? ,'yyyy-MM-dd HH:mm:ss')";
    private static final String MYSQLDateFunc = "STR_TO_DATE( ? ,'%Y-%m-%d %H:%i:%s')";
    private String dateFormatFunc = H2DateFunc;


    // query strings for various methods
    private String q_GetByName;
    private String q_where_bypk;
    private String q_selectStar;
    private String q_delete;
    private String PKName;

    public SQLGenerator(SQLDialect dialect, String tableName) {
        setTableName(tableName);
        setDialect(dialect);
        createQueries();
    }

    private void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public void setDialect(SQLDialect dialect) {
        this.dialect = dialect;
        if (dialect == SQLDialect.H2) dateFormatFunc = H2DateFunc;
        if (dialect == SQLDialect.MYSQL) dateFormatFunc = MYSQLDateFunc;

    }

    private void createQueries() {
        q_selectStar = "select " + tableName + ".* from " + tableName + "  ";
        q_where_bypk = " where " + PKName + " = ? ";
        q_delete = "delete  from " + tableName + "  ";
        /// q_count = "select count(*) from " + tableName;
    }

    public static String like(String colName) {
        return format(_like, colName, QQQ);
    }

    public String dateBetween(String colName) {
        return format(_between, tableName + "." + colName, dbDateFormatFunc(), dbDateFormatFunc());
    }

    public String dateBefore(String colName) {
        return format(_le, tableName + "." + colName, dbDateFormatFunc());
    }

    public String dateAfter(String colName) {
        return format(_ge, tableName + "." + colName, dbDateFormatFunc());
    }

    public String equals(String colName) {
        return format(_equals, tableName + "." + colName, QQQ);
    }
    public String equals(String tabName,String colName) {
        return format(_equals, tabName + "." + colName, QQQ);
    }
    public String count(String tablename) {
        return "select count(*) from " + tablename;
    }

    public String selectAll(String tableName) {
        return selectAll(tableName + "." + STAR, tableName, null);
    }

    public String selectAll(String tableName, String colsToSelect, Sort s) {
        return format(_selectAll, colsToSelect, tableName);
    }

    // returns string prefix with where if required.
    public String where(String where) {
        where = (where.toLowerCase().trim().startsWith("where")) ? " " + where :
                " WHERE " + where;
        return where;
    }

    public String orderByClause(Sort sort) {
        return " ORDER BY " + orderByExpression(sort);
    }

    public String joinInner(String pkCol, String fTab, String fkcol) {
        // from primary  INNER JOIN foreigntable ON primary.pk=foreign.fk;
        String join = format(" INNER JOIN %s ON %s.%s =%s.%s ", fTab, tableName, pkCol, fTab, fkcol);
        return join;
    }

    public String joinOuter(String pkCol, String fTab, String fkcol) {
        String join = format(" LEFT OUTER JOIN %s ON %s.%s =%s.%s ", fTab, tableName, pkCol, fTab, fkcol);
        return join;
    }

    protected String orderByExpression(Sort sort) {
        StringBuilder sb = new StringBuilder();

        for (Iterator<Order> it = sort.iterator(); it.hasNext(); ) {
            Order order = it.next();
            sb.append(order.getProperty()).append(' ').append(order.getDirection());

            if (it.hasNext())
                sb.append(COMMA);
        }
        return sb.toString();
    }

    // convert to mysql date format string
    public static String toDbDate(Date dt) {
        if (dt == null)
            return null;
        return mysqlSimpleFormatter.format(dt);
    }

    public String dbDateFormatFunc() {
        return this.dateFormatFunc;
    }

    public String selectStar() {
        return q_selectStar;
    }

    public String findWhere (String where, Pageable page){
        return format("%s LIMIT %d OFFSET %d",where,page.getPageSize(),page.getOffset());
    }

}
