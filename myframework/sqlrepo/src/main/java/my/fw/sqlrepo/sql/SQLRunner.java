package my.fw.sqlrepo.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import my.fw.sqlrepo.db.DatabaseConnectionManager;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import my.fw.model.BaseObject;
// TODO - consider using rowset
//https://stackoverflow.com/questions/6599625/resultset-vs-rowset-which-one-to-choose-and-when
// contains some core jdbctemplate based methods to make repo objects simpler
public class SQLRunner extends BaseObject {
	// jdbc objects
	protected Connection conn;
	protected DataSource ds;
	protected JdbcTemplate jdbcTemplate;
	// used in stored procedure call to set schema name
	protected String catalogName;

	public SQLRunner(DataSource _ds) {
		setDS(_ds);
	}

	protected void setDS(DataSource _ds) {
		if (_ds == null) {
			// get the default ds
			_ds = DatabaseConnectionManager.getDS();
		}
		this.ds = _ds;
		if (ds == null) {
			throw new RuntimeException("SQLRunner - unable to determine datasource");
		}
		try {
			this.conn = ds.getConnection();
			jdbcTemplate = new JdbcTemplate(ds);
			this.catalogName = conn.getCatalog();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	// run query using jdbctemplate
	public <T> List<T> runSelect(String sql, Object[] args, RowMapper<T> _mapper) {
		if (jdbcTemplate == null)
			jdbcTemplate = new JdbcTemplate(ds);
		List<T> l = jdbcTemplate.query(sql, args, _mapper);
		TRACE("SELECT", " rows:" + l.size() + ":" + sql);
		log.debug("RUNSQL-SELECT: rows:{} sql:{}",  l.size(), sql);

		return l;
	}
	// common method to trace and run jdbctemplate.update
	private int runUpdate(String sql, Object... args) {
		if (jdbcTemplate == null)
			jdbcTemplate = new JdbcTemplate(ds);
		int i = jdbcTemplate.update(sql, args);
		log.debug("RUNSQL-UPDATE: rows:{} sql:{}", i, sql);
		return i;
	}
	// same as update but with different logs
	private int RunDelete(String sql, Object... args) {
		if (jdbcTemplate == null)
			jdbcTemplate = new JdbcTemplate(ds);
		int i = jdbcTemplate.update(sql, args);
		log.debug("RUNSQL-DELETE: rows:{} sql:{}", i, sql);
		return i;
	}
	// get
	public <T> List<T> runQueryPS(Connection con, String sql, Object[] args, RowMapper<T> _mapper) {
		List<T> data = new ArrayList<T>();
		try {
			ResultSet rs = getResultSet(con, sql, args);
			int rowNum = 1;
			while (rs.next()) {
				T t = _mapper.mapRow(rs, rowNum);
				data.add(t);
			}
			rs.close();
		} catch (Exception e) {
		}
		return data;
	}

	// run a query and get a result set
	public ResultSet getResultSet(String sql, Object[] args) {
		try (Connection con = ds.getConnection()) {
			return getResultSet(con, sql, args);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	// run a query and get a result set
	public ResultSet getResultSet(Connection con, String sql, Object[] args) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = con.prepareStatement(sql);
			for (int i = 0; i < args.length; i++) {
				ps.setString(i + 1, args[i].toString());
			}
			INFO("runreport", "Running sql " + sql);
			rs = ps.executeQuery();
			// ps.close();
		} catch (Exception e) {
			ERROR(e,"getResultSet","Exception preparing or running sql");
			e.printStackTrace();
		} finally {
		}
		return rs;
	}

	public List<String> getColumns(ResultSet rs) {
		ArrayList<String> cols = new ArrayList<String>();
		try {
			ResultSetMetaData md = rs.getMetaData();
			if (rs != null) {
				for (int i = 1; i <= md.getColumnCount(); i++) {
					cols.add(md.getColumnLabel(i));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return cols;

	}

	// specialized query that returns int
	private Integer getCount(String sql, Object... args) {
		TRACE("count", sql);
		Integer i = jdbcTemplate.queryForObject(sql, Integer.class, args);
		// TRACE("count", "Count = " + i);
		log.debug("RUNSQL-COUNT: rows:{} sql:{}", i, sql);
		return i;
	}


	// run select with result set handler
	public <E> E runSelect(String sql, ResultSetExtractor<E> rse, Object... args) {
		return jdbcTemplate.query(sql, rse);
	}
}
