package my.fw.sqlrepo.util;

import my.fw.base.system.OSHelper;
import my.fw.sqlrepo.sql.SQLRunner;
import org.springframework.jdbc.core.ColumnMapRowMapper;
import org.springframework.jdbc.core.SingleColumnRowMapper;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.*;
import java.util.*;

// generates JPA entity code from db schema (mysql only)
// creates a complete java class (but some imports might be missing
// includes - class, fields , jpa annotations , getter/setter
// @id annotation and imports for some datatypes will be missing
//
public class JpaEntityGenerator {

    DataSource ds;
    SQLRunner runner;
    static String pkSql = "SELECT t.table_name, k.column_name FROM information_schema.table_constraints t JOIN information_schema.key_column_usage k USING(constraint_name,table_schema,table_name) WHERE t.constraint_type='PRIMARY KEY' AND t.table_schema= ? order by t.table_name";
    static String tblSql = "SELECT table_name FROM information_schema.tables  WHERE table_schema = ? ";

    public static boolean convertToCamelCase = true;
    // make fields public or private
    public static boolean publicAccessor = false;

    // import for all entities
    static String StdImports = "\n" +
            "import javax.persistence.*;\n" +
            "import java.math.BigInteger;\n" +
            "import java.math.BigDecimal;\n" +
            "import java.util.Date;\n";
//import java.math.BigInteger;
// import java.math.BigDecimal;

// need to handle blob

    public JpaEntityGenerator() {
    }

    public JpaEntityGenerator(DataSource ds) {
        this.ds = ds;
        this.runner = new SQLRunner(ds);
    }

    public void setDs(DataSource ds) {
        this.ds = ds;
        runner = new SQLRunner(ds);
    }

    // generate for all tables in schema
    public String genAllEntity(String schema) throws SQLException {
        String t = "";
        Collection<String> l = genAllEntityList(schema).values();
        for (String s : l) {
            t += s;
        }
        return t;
    }

    // generate for all tables in schema
    public Map<String, String> genAllEntityList(String schema) throws SQLException {
        Connection conn = ds.getConnection();
        List<String> tablist = getTableNames(conn, schema);
        Map<String, String> pkList = getPrimaryKeys(schema);
        Map<String, String> entList = new HashMap<String, String>();
        String t = "";
        tablist.forEach(y -> entList.put(y, genJpa(conn, y, pkList.get(y))));
        conn.close();
        return entList;
    }

    // place in separate files
    public Map<String, String> genAllEntityList(String schema, String dir, boolean singleFile) throws SQLException, IOException {
        dir = OSHelper.join(dir, "jpamodels");
        dir = OSHelper.createDirIfMissing(dir);
        Map<String, String> entList = genAllEntityList(schema);
        String finalDir = dir;
        entList.forEach((k, v) -> {
            try {
                OSHelper.saveToFile(finalDir, snakeToCamel(k, true) + ".java", v);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        return entList;
    }


    public List<String> getTableNames(Connection conn, String schema) {
        List<String> tablist = runner.runSelect(tblSql, new Object[]{schema}, new SingleColumnRowMapper<String>());
        return tablist;
    }


    public static String genJpa(Connection conn, String table, String pkCol) {
        PreparedStatement preparedStatement = null;
        String query = "SELECT * from " + table + " where 1 = 2 ";
        String s = null, t = null;
        try {
            t = "\n";
            t += StdImports;
            s = genJpaFields(conn, table, pkCol, true);
            t += "\n\n@Entity \n@Table(name=\"" + table + "\")\n";
            t += "// PK " + pkCol + "\n";
            t += "public class " + snakeToCamel(table, true) + "{ \n\n";
            t += s;
            t += "\n}\n\n ";

        } catch (Exception throwables) {
            throwables.printStackTrace();
        } finally {
        }

        return t;
    }

    // get metadata for a fake query so that we have all table details
    // then generate all the fileld names with mapping
    // also generate getter/setter
    public static String genJpaFields(Connection conn, String table, String pkCol, boolean includeSetterGetter) throws SQLException {
        PreparedStatement preparedStatement = null;
        String query = "SELECT * from " + table + " where 1 = 2 ";
        preparedStatement = conn.prepareStatement(query);
        ResultSet rs = preparedStatement.executeQuery();
        ResultSetMetaData md = rs.getMetaData();
        String s = "";
        String g = "";
        for (int i = 1; i <= md.getColumnCount(); i++) {
            String colName = md.getColumnName(i);
            boolean ispk = colName.equalsIgnoreCase(pkCol);
            String f = genField(md, i, ispk);

            s += f;
            g += genSetterGetter(md, i);
        }
        rs.close();
        preparedStatement.close();
        return s + g;
    }

    public static String genField(ResultSetMetaData md, int i, boolean isId) throws SQLException {
        String s = "";
        s += isId ? "@Id\n" : "";
        s += "@Column(name=\"" + md.getColumnName(i) + "\")  // " + md.getColumnTypeName(i) + " -  " + md.getColumnDisplaySize(i) + " \n";
        //s += isId ? "\n" : "// PK does not exist or is multi column \n";
        s += publicOrPrivate() + (getClass(md, i)) + "  " + snakeToCamel(md.getColumnName(i), false) + ";\n";
        s += "\n";
        return s;
    }

    public static String genSetterGetter(ResultSetMetaData md, int i) throws SQLException {
        String colName = md.getColumnName(i);
        String _m = snakeToCamel(md.getColumnName(i), true);
        String getter = "get" + _m;
        String setter = "set" + _m;
        String _class = getClass(md, i);

        String g = "public " + _class + "  " + getter + "(){\n";
        g += "return this." + snakeToCamel(md.getColumnName(i), false) + ";\n}\n";
        g += "\n";

        String s = "public void  " + setter + "(" + _class + " " + colName + "){\n";
        s += " this." + snakeToCamel(md.getColumnName(i), false) + "= " + colName + ";\n}\n";
        s += "\n";

        return g + s;
    }

    private static String getClass(ResultSetMetaData md, int i) throws SQLException {
        String c = md.getColumnClassName(i);
        String s = c;
        switch (c) {
            case "java.lang.String":
                s = "String";
                break;
            case "java.lang.Integer":
                s = "Integer";
                break;
            case "java.math.BigInteger":
                s = "BigInteger";
                break;
            case "java.math.BigDecimal":
                s = "BigDecimal";
                break;
            case "java.lang.Long":
                s = "Long";
                break;
            case "java.lang.Double":
                s = "Double";
                break;
            case "java.lang.Float":
                s = "Float";
                break;
            case "java.sql.Timestamp":
                s = "Date";
                break;
            case "java.sql.Date":
                s = "Date";
                break;
            case "java.lang.Boolean":
                s = "Boolean";
                break;
        }
        return s;
    }

    public static String snakeToCamel(String str, boolean classname) {
        if (!convertToCamelCase) return str;
        str = str.toLowerCase();
        if (classname) {
            str = str.substring(0, 1).toUpperCase()
                    + str.substring(1);
        }
        StringBuilder builder
                = new StringBuilder(str);
        int max = builder.length();
        for (int i = 0; i < builder.length(); i++) {

            // Check char is underscore
            if (builder.charAt(i) == '_') {

                builder.deleteCharAt(i);
                // ignore if _ is the last char
                if (i < builder.length()) {
                    builder.replace(
                            i, i + 1,
                            String.valueOf(
                                    Character.toUpperCase(
                                            builder.charAt(i))));
                }
            }
        }

        return builder.toString();
    }

    public static String publicOrPrivate() {
        if (publicAccessor) return "public ";
        return "private ";
    }

    // create a map of table and pk col name . if PK is multi col then indicate with COMPLEXKEY
    public Map<String, String> getPrimaryKeys(String schema) throws SQLException {
        Map<String, String> result = new HashMap<>();
        List<Map<String, Object>> l = runner.runSelect(pkSql, new Object[]{schema}, new ColumnMapRowMapper());
        l.forEach(r -> {
            String tab = r.get("TABLE_NAME").toString().toLowerCase();
            String col = r.get("COLUMN_NAME").toString();
            if (result.containsKey(tab)) col = "COMPLEXKEY";
            result.put(tab, col);
        });
        return result;
    }


}
