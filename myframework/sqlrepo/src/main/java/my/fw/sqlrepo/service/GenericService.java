package my.fw.sqlrepo.service;

import my.fw.base.date.MYDate;
import my.fw.model.*;
import my.fw.service.ISimpleSvc;
import my.fw.sqlrepo.EntityInfo;
import my.fw.sqlrepo.repo.GenericSQLRepo;
import my.fw.sqlrepo.sql.TxnRunner;
import org.apache.commons.lang3.StringUtils;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.*;

// generic the primary object used to perform action on an entity
// all the sql work is done by genericrepo which should not be used directly
public class GenericService<T extends IObjWithId<ID>, ID> extends BaseObject
        implements ISimpleSvc<T, ID>, AutoCloseable {
    public static final String unknownUser = "unknown";
    protected TxnRunner txnRunner;
    protected GenericSQLRepo<T, ID> _repo;
    // used to controlling setting of TS in record, is set firsttime save or update
    // is called.
    protected boolean objCheckDone = false;
    protected boolean hasCreateTS = false;
    protected boolean hasCreatedBy = false;
    protected boolean hasUpdateTS = false;
    protected boolean hasUpdateBy = false;
    private EntityInfo einfo;

    public GenericService(GenericSQLRepo<T, ID> r) {
        this._repo = r;
    }

    public GenericService(GenericSQLRepo<T, ID> r, EntityInfo ei) {
        this.einfo = ei;
        this._repo = r;
    }

    public GenericService(Class<T> klass, EntityInfo ei) {
        this(new GenericSQLRepo<T, ID>(klass, ei), ei);
    }

    // get a transaction template
    protected TransactionTemplate getTT() {
        return getTxnRunner().getTxnTemplate();
    }

    protected TxnRunner getTxnRunner() {
        if (this.txnRunner == null) txnRunner = new TxnRunner(_repo.getDS(), this.log);
        return txnRunner;
    }

    public GenericSQLRepo<T, ID> getRepo() {
        return _repo;
    }

    public T findOne(ID id) {
        id = prepare(id);
        return _repo.findOne(id);
    }

    public int delete(ID id) {
        id = prepare(id);
        return _repo.delete(id);
    }

    //
    public ID prepare(ID id) {
        return id;
    }

    @Override
    public void close() throws Exception {
        if (_repo != null)
            _repo.close();
    }

    @Override
    public List<T> findAll() {
        return _repo.findAll();
    }

    @Override
    public T save(T t) {
        return save(t, null);
    }

    @Override
    public T save(T t, String createdBy) {
        checkObj(t);
        t = setCreateTSIfMissing(t);
        t = setCreatedByIfMissing(t, createdBy);
        t = setModifiedTS(t);
        t = _repo.save(t);
        return t;
    }

    @Override
    public int update(T t) {
        return update(t, false, unknownUser);
    }

    public int update(T t, boolean includeNulls) {
        return update(t, includeNulls, unknownUser);
    }

    @Override
    public int update(T t, boolean includeNulls, String modifiedBY) {
        checkObj(t);
        t = setModifiedTS(t);
        t = setModifiedBy(t, modifiedBY);
        return _repo.update(t);
    }

    // update only selected cols
    public int update(T t, Collection<String> colsToUpdate) {
        checkObj(t);
        t = setModifiedTS(t);
        // make sure modifiedts is added to list of cols
        if (hasUpdateTS) {
            if (!colsToUpdate.contains(ColNames.updateTS))
                colsToUpdate.add(ColNames.updateTS);
        }
        if (hasUpdateBy) {
            if (!colsToUpdate.contains(ColNames.updateBy))
                colsToUpdate.add(ColNames.updateBy);
        }
        return _repo.update(t, colsToUpdate, false);
    }

    @Override
    public ID getID(T t) {
        return t.getId();
    }

    @Override
    public T setID(T t, ID id) {
        t.setId(id);
        return t;
    }

    // check the object ONCE , on first time save is called , for supported
    // interfaces
    protected void checkObj(T t) {
        if (objCheckDone)
            return;
        if (t instanceof IObjWithCreateTS)
            hasCreateTS = true;
        if (t instanceof IObjWithUpdateTS)
            hasUpdateTS = true;
        if (t instanceof IObjWithUpdateBy)
            hasUpdateBy = true;
        if (t instanceof IObjWithCreatedBy)
            hasCreatedBy = true;
        objCheckDone = true;

    }

    // set createTS if missing
    protected T setCreateTSIfMissing(T t) {
        if (hasCreateTS) {
            IObjWithCreateTS _t = (IObjWithCreateTS) t;
            if (_t.getCreatedOn() == null)
                _t.setCreatedOn(MYDate.now());
        }
        return t;
    }

    // set created by if supported
    protected T setCreatedByIfMissing(T t, String userid) {
        if (hasCreatedBy) {
            IObjWithCreatedBy _t = (IObjWithCreatedBy) t;
            // only set userid if it is empty since we may have set it earlier !!!
            if (StringUtils.isBlank(_t.getCreatedBy())) {
                if (StringUtils.isBlank(userid)) userid = unknownUser;
                _t.setCreatedBy(userid);
            }
        }
        return t;
    }

    // set modifiedTS if missing
    protected T setModifiedTS(T t) {
        if (hasUpdateTS) {
            IObjWithUpdateTS _t = (IObjWithUpdateTS) t;
            // we dont check for modifiedts being null since the col can contain the
            // previous tS
            _t.setModifiedOn(MYDate.now());
        }
        return t;
    }

    // set modifiedTS if missing
    protected T setModifiedBy(T t, String modifiedBY) {
        if (hasUpdateBy) {
            if (modifiedBY == null)
                modifiedBY = "UNKNOW";
            IObjWithUpdateBy _t = (IObjWithUpdateBy) t;
            _t.setUpdateBy(modifiedBY);
        }
        return t;
    }

    @Override
    public ID toID(Object o) {
        return null;
    }

    @Override
    public T expand(T t) {
        return t;
    }

    // special method for a single column, adds other fields as required
    protected int updateSingleColumn(ID id, String modifiedBy, String colname, Object value) {
        Collection<String> colstoupdate = new ArrayList<>();
        colstoupdate.add(colname);

        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put(colname.toLowerCase().trim(), value);
        if (hasUpdateTS) {
            map.put(ColNames.updateTS, MYDate.now());
            colstoupdate.add(ColNames.updateTS);
        }
        if (hasUpdateBy) {
            map.put(ColNames.updateBy, modifiedBy);
            colstoupdate.add(ColNames.updateBy);
        }
        return _repo.update(id, map, colstoupdate, true);
    }

    // helper to create a list of column names
    protected ArrayList<String> newColList(String... args) {
        return new ArrayList<String>(Arrays.asList(args));
    }

    @Override
    public Result validate(T t) {
        // this method should be overridden in each implementation
        return Result.Success();
    }

    @Override
    public long count() {
        return _repo.count();
    }

    @Override
    public boolean exists(ID id) {
        return _repo.exists(id);
    }

    @Override
    public T setDefaults(T t) {
        return t;
    }

    public Result runInTxn(TransactionCallback<Result> action) {
        return getTxnRunner().runInTxn(action);
    }

    @Override
    public T format(T t) {
        return t;
    }

    public static class ColNames {
        public static final String updateBy = "modifiedby";
        public static final String updateTS = "modifiedon";
        public static final String createTS = "createdon";
        public static final String createdBY = "createdby";
    }
}
