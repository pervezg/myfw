package my.fw.sqlrepo.db;


import my.fw.base.logging.MYLogFactory;
import my.fw.base.logging.MYLogHelper;
import org.slf4j.Logger;

import javax.sql.DataSource;
import java.sql.Connection;
import java.util.Map;

//Datasource Factory
// common class for creating and storing datasources
public class DSFactory {
    static Logger log = MYLogFactory.getLogger("DSFactory");

    private static DataSource defaultDS;

    public static DataSource getDefaultDS() {
        return defaultDS;
    }

    // sets the default DS name . at this time we assume that the entire app uses only one DS
    // TODO Pervez - enhance to manange multiple DS in a list
    public static DataSource setDefaultDS(DataSource ds) {
        defaultDS = ds;
        return ds;
    }

    public static void logDataSource(DataSource ds) {
        Map<String, String> map = DBConnHelper.getDataSourceInfo(ds);
        MYLogHelper.logMap(log, "DBInfo", map);
    }

    private static void logConnection(Connection c) {
        Map<String, String> map = DBConnHelper.getConnectionInfo(c);
        MYLogHelper.logMap(log, "DBInfo", map);
    }

}
