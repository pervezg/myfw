package my.fw.sqlrepo;

import javax.swing.text.ParagraphView;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.nio.channels.Pipe;
import java.util.function.BiConsumer;
import java.util.function.Function;

public class FieldInfo {
    public String fieldName;

    public String colName;
    public boolean unique;
    public String typeName;
    public Type type;

    public String getterName;
    public String setterName;
    public Method getterMethod;
    public Method setterMethod;
    public PropertyDescriptor pd;
    public Function lambdaGetter;
    public BiConsumer lambdaSetter;
}
