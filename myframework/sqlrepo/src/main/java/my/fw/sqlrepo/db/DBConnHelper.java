package my.fw.sqlrepo.db;


import my.fw.base.logging.MYLogHelper;
import my.fw.base.system.MyProperties;
import my.fw.sqlrepo.sql.SQLDialect;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

// helper methods to create Datasource objects
public class DBConnHelper {
    public static class MySqlConsts {
        public static String product = "mysql";
        public static String driver = "com.mysql.cj.jdbc.Driver";
        private static int defaultPort = 3306;
    }

    public static class PGConsts {
        public static String product = "postgresql";
        public static String driver = "org.postgresql.Driver";
        private static int defaultPort = 5432;
    }

    public static String mysqlDriver = "com.mysql.jdbc.Driver";
    public static String h2Driver = "org.h2.Driver";
    private static int MYSQLPORT = 3306;

    public static class PropNames {
        public static String dsname = "name";
        public static String schema = "schema";
        public static String catalog = "catalog";

        public static String host = "host";
        public static String port = "port";
        public static String username = "username";
        public static String password = "password";
        public static String url = "url";
        public static String driverClassName = "driver-class-name";
    }

    public static String getDBProd(DataSource ds) {
        Map<String, String> info = getDataSourceInfo(ds);
        return info.get("DBPROD");
    }

    // convert db prod string into dialect. default is mysql
    public static SQLDialect getSQLDialect(DataSource ds) {
        SQLDialect dialect = SQLDialect.H2;
        try {
            if (ds == null)
                return null;
            Connection c = ds.getConnection();
            if (c != null && !c.isClosed()) {
                dialect = getSQLDialect(c);
                c.close();
            }
        } catch (Exception e) {
        }
        return dialect;
    }

    // convert db prod string into dialect. default is mysql
    public static SQLDialect getSQLDialect(Connection c) {
        SQLDialect dialect = SQLDialect.H2;

        try {
            if (c != null && !c.isClosed()) {
                String dbprod = c.getMetaData().getDatabaseProductName().toLowerCase();
                switch (dbprod) {
                    case "mysql":
                        return SQLDialect.MYSQL;
                    case "h2":
                        return SQLDialect.H2;
                    case "postgresql":
                        return SQLDialect.POSTGRES;
                }
            }
        } catch (SQLException e) {
            // e.printStackTrace();
        }
        return dialect;
    }

    public static void logDataSource(Logger log, DataSource ds) {
        Map<String, String> map = getDataSourceInfo(ds);
        MYLogHelper.logMap(log, "DBInfo", map);
    }

    private static void logConnection(Logger log, Connection c) {
        Map<String, String> map = getConnectionInfo(c);
        MYLogHelper.logMap(log, "DBInfo", map);
    }

    // dump info on db connection in a map.
    // we need to open a connection to get this info.
    public static Map<String, String> getDataSourceInfo(DataSource ds) {
        Map<String, String> map = new HashMap<String, String>();

        try {

            if (ds == null)
                return map;
            Connection c = ds.getConnection();
            if (c != null && !c.isClosed()) {
                map = getConnectionInfo(c);
                c.close();
            }
            map.put("DS Class", ds.getClass().toString());

        } catch (Exception e) {
            // e.printStackTrace();
            map.put("Exception getting Connection Information", e.getMessage());
        }
        return map;
    }

    // get a map with connection info
    public static Map<String, String> getConnectionInfo(Connection c) {
        Map<String, String> map = new HashMap<String, String>();
        try {
            if (c != null && !c.isClosed()) {
                DatabaseMetaData md = c.getMetaData();
                map.put("DBServer", md.getDatabaseProductName());
                map.put("DB Version", md.getDatabaseProductVersion());
                map.put("Driver Name", md.getDriverName());
                map.put("Driver Version", md.getDriverVersion());
                map.put("DB Version", md.getDatabaseProductVersion());
                map.put("Url", md.getURL());
                map.put("catalog/db name (getCatalog", c.getCatalog());
                map.put("Schema/db name (getSchema)", c.getSchema());
                map.put("NetworkTimeout", Integer.toString(c.getNetworkTimeout()));
                map.put("User", md.getUserName());
                map.put("CatalogTerm", md.getCatalogTerm());
                map.put("ProcedureTerm", md.getProcedureTerm());
            }
        } catch (SQLException e) {
            map.put("Exception getting Connection Information", e.getMessage());
        }
        return map;
    }

    public static String mysqlUrl(String host, int port, String schema, String userName, String password) {
        String url = "jdbc:mysql://" + userName + ":" + password + "@" + host + ":" + port + "/" + schema + "?useSSL=false";
        ;
        return url;
    }

    public static String mysqlUrl(String host, String port, String schema) {
        int p = MYSQLPORT;
        if (port != null && !port.isEmpty()) {
            p = Integer.parseUnsignedInt(port);
        }
        //String url = "jdbc:mysql://" + host + ":" + p + "/" + schema + "?useSSL=false";
        return mysqlUrl(host, p, schema);
    }

    public static String pgUrl(String host, String port, String schema) {
        int p = PGConsts.defaultPort;
        if (port != null && !port.isEmpty()) {
            p = Integer.parseUnsignedInt(port);
        }
        //String url = "jdbc:mysql://" + host + ":" + p + "/" + schema + "?useSSL=false";
        return pgUrl(host, p, schema);
    }

    public static String mysqlUrl(String host, int port, String schema) {
        if (port == 0) port = MYSQLPORT;
        String url = "jdbc:mysql://" + host + ":" + port + "/" + schema + "?useSSL=false";
        return url;
    }

    public static String pgUrl(String host, int port, String schema) {
        if (port == 0) port = PGConsts.defaultPort;
        String url = "jdbc:postgresql://" + host + ":" + port + "/" + schema;
        return url;
    }

    public static String h2memUrl(String host, int port, String schema) {
        String url = "jdbc:h2:mem:" + schema + ";";
        return url;
    }

    // create a spring datasource using drivername and url - this permits us to have no DB dependencies in my.fw
    public static DataSource newds(String driverClassName, String dbUrl, String schema, String userName, String pwd) {
        DriverManagerDataSource d = new DriverManagerDataSource();
        d.setDriverClassName(driverClassName);
        d.setUrl(dbUrl);
        d.setUsername(userName);
        if (schema != null && schema != "") {
            d.setCatalog(schema);
            d.setSchema("public");
        }

        if (pwd != null && !pwd.isEmpty())
            d.setPassword(pwd);
        return d;
    }

    // create mysql ds using driver name . note that the driver name changes with mysql versions
    public static DataSource createMysqlDS(String host, int port, String schema, String userName, String password) {
        String url = mysqlUrl(host, port, schema);
        return createMysqlDS(url, schema, userName, password);
    }

    public static DataSource createPGDS(String host, int port, String schema, String userName, String password) {
        String url = pgUrl(host, port, schema);
        return newds(PGConsts.driver, url, schema, userName, password);
    }

    public static DataSource createMysqlDS(String dbUrl, String schema, String userName, String pwd) {
        return newds(mysqlDriver, dbUrl, schema, userName, pwd);
    }

    public static DataSource createH2memDS(String schema, String userName, String password) {
        String url = "jdbc:h2:mem:" + schema + ";";
        url += "INIT=RUNSCRIPT FROM 'classpath:scripts/create.sql'";
        return newds(h2Driver, url, schema, userName, password);
    }

    public static DataSource createH2fileDS(String schema, String userName, String password) {
        String url = "jdbc:h2:d:/h2db/test/" + schema + ";";
        // url+="INIT=RUNSCRIPT FROM 'classpath:scripts/create.sql'";
        return newds(h2Driver, url, schema, userName, password);
    }

    public static DataSource createH2tcpDS(String schema, String userName, String password) {
        String url = "jdbc:h2:localhost/~/" + schema + ";AUTO_SERVER=TRUE";
        // url+="INIT=RUNSCRIPT FROM 'classpath:scripts/create.sql'";
        return newds(h2Driver, url, schema, userName, password);
    }

    public static DataSource getDSFromProps(String prefix, Properties props) {
        var filtered = MyProperties.filterPrefix(prefix, props, true);
        var ds = getDSFromProps(filtered);
        return ds;
    }

    public static DataSource getDSFromProps(Properties props) {
        var map = MyProperties.toMap(props);
        //DBConnHelper.newds();
        String host = map.get(PropNames.host);
        String portStr = map.get(PropNames.port);
        String schema = map.get(PropNames.schema);
        String driver = map.get(PropNames.driverClassName);
        String url = map.get(PropNames.url);
        String userid = map.get(PropNames.username);
        String pwd = map.get(PropNames.password);
        if (driver == null) {
            driver = mysqlDriver;
        }
        if (StringUtils.isBlank(url))
            url = DBConnHelper.mysqlUrl(host, portStr, schema);
        return DBConnHelper.newds(driver, url, schema, userid, pwd);

    }

    private static void exIfNull(String fieldname, String value) {
        if (value == null || value.isEmpty())
            throw new RuntimeException("Parameter '" + fieldname + "' is missing or empty");

    }
}
