package my.fw.sqlrepo;

import my.fw.base.logging.MYLogFactory;
import my.fw.model.IEntityWithProperties;
import my.fw.model.IPropertyOfEntity;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.ConstructorUtils;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.slf4j.Logger;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.annotation.Annotation;
import java.lang.invoke.*;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Function;

//https://stackoverflow.com/questions/10009052/invoking-setter-method-using-java-reflection
//https://stackoverflow.com/questions/2638590/best-way-of-invoking-getter-by-reflection
//https://www.baeldung.com/java-performance-mapping-frameworks
//https://www.christianschenk.org/blog/java-bean-mapper-performance-tests/
//https://stackoverflow.com/questions/16428817/convert-a-mapstring-string-to-a-pojo
//https://stackoverflow.com/questions/34069386/methodhandle-to-a-getter-setter-from-another-class-gives-a-nosuchfielderror

//https://www.freecodecamp.org/news/a-faster-alternative-to-java-reflection-db6b1e48c33e/
//https://www.infoq.com/articles/Invokedynamic-Javas-secret-weapon/
//http://cr.openjdk.java.net/~vlivanov/talks/2015-Indy_Deep_Dive.pdf
//https://dzone.com/articles/dismantling-invokedynamic
//https://gist.github.com/raphw/881e1745996f9d314ab0
//https://www.baeldung.com/java-method-handles
//https://www.jboss.org/optaplanner/blog/2018/01/09/JavaReflectionButMuchFaster.html
//https://stackoverflow.com/questions/19557829/faster-alternatives-to-javas-reflection/19563000
//   https://www.javacodegeeks.com/2019/09/think-twice-before-using-reflection.html
//https://dzone.com/articles/hacking-lambda-expressions-in-java
//https://stackoverflow.com/questions/22200676/how-to-list-all-properties-exposed-by-a-java-class-and-its-ancestors-in-eclipse

/**
 * EntityInfoFactory generates and caches EntityInfo objects which are used to generate automappers between JDBC  Resulsets and Java persistence Entity objects
 * MYFW uses it to generate mappers and unmappers to use with spring JDBCtemplates
 */
public class EntityInfoFactory {

    static Logger log = MYLogFactory.getLogger(EntityInfoFactory.class);
    public static boolean ignoreMappingErrors = true;
    // stores generated EI for reuse
    private static Map<String, EntityInfo> cache = Collections.synchronizedMap(new HashMap<>());

    /**
     * @param klass The class of the Entity that needs to be described
     * @return an EntityInfo object
     * <p>
     * This implementation uses Apache commons beanutils and is replaced by a lambdafactory based implementation which is supposed to be faster
     */
    @Deprecated
    private static EntityInfo getEntityInfoBeanUtil(Class klass) {
        if (cache.containsKey(klass.getName())) return cache.get(klass.getName());
        Map<String, Column> cols = new HashMap<>();
        EntityInfo ei = new EntityInfo();
//        Field[] fields = c.getDeclaredFields();
        // using fieldutils since it gets fields from superclasses too.
        Field[] fields = FieldUtils.getAllFields(klass);
        Annotation[] a = klass.getAnnotations();
        Table _tab = (Table) klass.getAnnotation(Table.class);
        ei.entityName = klass.getName();
        ei.entityClass = klass;
        if (_tab != null) {
            ei.tableName = _tab.name();
            if (ei.tableName == null || ei.tableName.isEmpty()) ei.tableName = ei.entityName;
        } else {
            var ex = new RuntimeException("EntityInfoFactory : Class " + klass.getName() + " does not have a @Table annotation . Cannot create entity info");
            log.error("Exception", ex);
            throw ex;
        }
        for (Field f : fields) {

            Column _col = f.getAnnotation(Column.class);
            if (_col != null) {
                FieldInfo fi = new FieldInfo();
                fi.fieldName = f.getName();
                fi.colName = _col.name();
                if (fi.colName == null || fi.colName.isEmpty()) {
                    fi.colName = fi.fieldName;
                }
                fi.unique = _col.unique();
                fi.type = f.getType();
                fi.typeName = f.getType().getName();

                Id _id = f.getAnnotation(Id.class);
                if (_id != null) {
                    GeneratedValue _genval = f.getAnnotation(GeneratedValue.class);
                    ei.pk = fi.colName;
                    if (_genval != null)
                        ei.pkAutogen = true;
                }
                // add to list of fields so that we can easily lookup by propertyname
                ei.fields.put(fi.fieldName, fi);
                // add to list of cols for caseinsentive lookup of colname
                ei.columns.put(fi.colName.toLowerCase(), fi);
            }
        }
        cache.put(klass.getName(), ei);
        return ei;
    }

    public static EntityInfo getEntityInfo(Class klass) {
        return getEntityInfo(klass, true);
    }

    /**
     * @param klass The class of the Entity that needs to be described
     * @return an EntityInfo object
     * <p>
     * This implementation also adds lambda setters/getter for each property
     * The class must have Table and Column annotations
     */
    // new entity info that includes lambda getter/setter
    public static EntityInfo getEntityInfo(Class klass, boolean addLambdas) {
        if (cache.containsKey(klass.getName())) return cache.get(klass.getName());
        Map<String, Column> cols = new HashMap<>();
        EntityInfo ei = new EntityInfo();
        boolean idFound = false;

        //  try {
        // using fieldutils since it gets fields from superclasses too.
        Field[] fields = FieldUtils.getAllFields(klass);

        // using PD to find getter/setter. we can to eliminate the uses of both Fieldutiles and Proputiles
        // since it seems redundant but will do so later.
        PropertyDescriptor[] propInfo = PropertyUtils.getPropertyDescriptors(klass);
        Annotation[] a = klass.getAnnotations();
        Table _tab = (Table) klass.getAnnotation(Table.class);
        ei.entityName = klass.getName();
        ei.entityClass = klass;
        if (_tab != null) {
            ei.tableName = _tab.name();
            if (ei.tableName == null || ei.tableName.isEmpty()) ei.tableName = ei.entityName;
        } else {
            var ex = new RuntimeException("EntityInfoFactory : Class " + klass.getName() + " does not have a @Table annotation . Cannot create entity info");
            log.error("Exception", ex);
            throw ex;
        }

        for (Field f : fields) {
            Column _col = f.getAnnotation(Column.class);
            if (_col != null) {
                FieldInfo fi = new FieldInfo();
                fi.fieldName = f.getName();
                fi.type = f.getType();
                fi.typeName = f.getType().getName();

                // use fieldname as colname unless annotation has a name
                fi.colName = _col.name();
                if (fi.colName == null || fi.colName.isEmpty()) {
                    fi.colName = fi.fieldName;
                }
                fi.unique = _col.unique();

                // check for ID and GeneratedValue annotations

                Id _id = f.getAnnotation(Id.class);
                if (_id != null) {
                    // use idfound to detect multiple id cols
                    if (idFound) {
                        var ex = new RuntimeException("EntityInfoFactory : Class " + klass.getName() + " has multiple @Id annotations which is not supported in this version . Cannot create entity info");
                        log.error("Exception", ex);
                        throw ex;
                    }
                    ei.pk = fi.colName;
                    idFound = true;
                    ei.pkAutogen = f.getAnnotation(GeneratedValue.class) != null;
                }

                if (addLambdas) {
                    Optional<PropertyDescriptor> opd = Arrays.stream(propInfo).filter(p -> fi.fieldName.equals(p.getName())).findFirst();
                    if (opd.isPresent()) {
                        PropertyDescriptor pd = opd.get();
                        fi.getterMethod = pd.getReadMethod();
                        fi.setterMethod = pd.getWriteMethod();
                        try {
                            if (fi.getterMethod != null) {
                                fi.getterName = fi.getterMethod.getName();
                                fi.lambdaGetter = LambdaHelper.createGetter(fi.getterMethod);
                            }
                            if (fi.setterMethod != null) {
                                fi.setterName = fi.setterMethod.getName();
                                fi.lambdaSetter = LambdaHelper.createSetter(fi.setterMethod);

                            }
                        } catch (Exception e) {
                            // perhaps we can supress this exeption to be more error tolerant
                            var ex = new RuntimeException("EntityInfoFactory : Class " + klass.getName() + " exception creating getter/setter lambda for field" + fi.fieldName, e);
                            log.error("Exception", ex);
                            throw ex;
                        }
                    }
                }
                // add to list of fields so that we can easily lookup by propertyname
                ei.fields.put(fi.fieldName, fi);
                // add to list of cols for caseinsentive lookup of colname
                ei.columns.put(fi.colName.toLowerCase(), fi);
            }
        }
        cache.put(klass.getName(), ei);
        return ei;
    }
//
//    public static EntityInfo addLambdaSetterGetter(EntityInfo ei) throws Throwable {
//        ei.fields = addGettersAndSetterLambdas(ei.fields);
//        return ei;
//    }

//    private static Map<String, FieldInfo> addGettersAndSetterLambdas(Map<String, FieldInfo> fimap) throws Throwable {
//        for (String key : fimap.keySet()) {
//            var fi = fimap.get(key);
//            fi.lambdaGetter = LambdaHelper.createGetter(fi.getterMethod);
//            fi.lambdaSetter = LambdaHelper.createSetter(fi.setterMethod);
//        }
//        return fimap;
//    }

    // maps a SINGLE row from a  resultset entry to entity object using Beanutils/reflection
    // maps subset of columns in a rs to an entity
    // this is useful when we need to map the result of a join to different entities

    /**
     * * maps a SINGLE row from a  resultset entry to entity object using Beanutils/reflection
     *
     * @param klass    Class of entity to be returned
     * @param rs       ResultSet , provided by the jdbctemplate that calls this mappter
     * @param ei       EntityInfo object
     * @param startpos starting col to use - set to 0 to start from first col
     * @param endpos   ending col - use 0 to consume all columns
     *                 start/stop are useful when we are processing the result of a join containing a.*, b.* , c.* and we only want to pick items for 1 table
     * @param <T>
     * @return
     */
    public static <T> T reflectionMapper(Class<T> klass, ResultSet rs, EntityInfo ei, int startpos, int endpos) {
        try {
            if (startpos == 0) startpos = 1;
            int cc = rs.getMetaData().getColumnCount();
            if (endpos == 0 || endpos < startpos || endpos > cc) endpos = cc;

            T p = ConstructorUtils.invokeConstructor(klass, null);
            for (int i = startpos; i <= endpos; i++) {
                String colname = rs.getMetaData().getColumnLabel(i).toLowerCase();
                if (ei.columns.containsKey(colname)) {
                    FieldInfo fi = ei.columns.get(colname);
                    Object value = rs.getObject(i);
                    if (value != null)
                        BeanUtils.setProperty(p, fi.fieldName, value);
                }
            }
            return p;
        } catch (Exception e) {
            log.error("reflectionMapper", e);
        }
        return null;
    }

    public static <T> T reflectionMapper(Class<T> klass, ResultSet rs) {
        EntityInfo ei = getEntityInfo(klass);
        return reflectionMapper(klass, rs, ei);
    }

    public static <T> T reflectionMapper(Class<T> klass, ResultSet rs, EntityInfo ei) {
        return reflectionMapper(klass, rs, ei, 0, 0);
    }

    // Resultset excractor to use on jdbctemplate
    // processes ALL rows from a resultset
    // used to process parent-child joins
    // gets the whole result set  insteaad of 1 record
    public static <P extends IEntityWithProperties, C extends IPropertyOfEntity> List<P>
    extractData(Class<P> klass, Class<C> childklass, ResultSet resultSet) throws SQLException {
        List<P> r = new ArrayList<P>();
        EntityInfo parentei = getEntityInfo(klass);
        EntityInfo childei = getEntityInfo(childklass);

        int[][] pos = EntityInfoFactory.getpositions(resultSet, parentei, childei);
        P lastParent = null;
        while (resultSet.next()) {
            P parent = reflectionMapper(klass, resultSet, parentei, pos[0][0], pos[0][1]);
            C child = reflectionMapper(childklass, resultSet, childei, pos[1][0], pos[1][1]);
            if (lastParent == null) {
                lastParent = parent;
                r.add(lastParent);
            }

            if (parent.getId() != lastParent.getId()) {
                // new parent , so save the old one.
                r.add(lastParent);
                lastParent = parent;
            }

            if (child.getName() != null) {
                lastParent.putProp(child);
            }
        }
        return r;
    }

    /**
     * Unmapper - used in SQL insert to convert properties into a map of values
     * this implementation uses reflection
     *
     * @param klass
     * @param ei
     * @param t
     * @param <T>
     * @return
     */
    public static <T> Map<String, Object> reflectionUmapper(Class<T> klass, EntityInfo ei, T t) {
        LinkedHashMap<String, Object> row = new LinkedHashMap<>();
        FieldInfo fi = null;
        try {
            for (Map.Entry<String, FieldInfo> field : ei.fields.entrySet()) {
                fi = field.getValue();
                Object v = PropertyUtils.getProperty(t, fi.fieldName);
                row.put(fi.colName, v);
            }
        } catch (Exception e) {
            String msg = "reflectionUmapper: Class " + klass.getName() + " field : " + fi.fieldName;
            log.error(msg, e);
            if (!ignoreMappingErrors) throw new RuntimeException(msg, e);
        }
        return row;
    }

    /**
     * Unmapper - used in SQL insert to convert properties into a map of values
     * this implementation uses previously created lambda functions . supposed to perform better than reflection
     *
     * @param klass
     * @param ei
     * @param t
     * @param <T>
     * @return
     */

    public static <T> Map<String, Object> lambdaUnmapper(Class<T> klass, EntityInfo ei, T t) {
        LinkedHashMap<String, Object> row = new LinkedHashMap<>();
        FieldInfo fi = null;
        try {
            for (Map.Entry<String, FieldInfo> field : ei.fields.entrySet()) {
                fi = field.getValue();
                Object v = null;
                if (fi.lambdaGetter != null) {
                    v = fi.lambdaGetter.apply(t);
                }
                row.put(fi.colName, v);
            }
        } catch (Exception e) {
            String msg = "lambdaUnmapper: Class " + klass.getName() + " field : " + fi.fieldName;
            log.error(msg, e);
            if (!ignoreMappingErrors) throw new RuntimeException(msg, e);
        }
        return row;
    }


    public static Map<String, Object> rsToMap(ResultSet rs) {
        Map<String, Object> map = new HashMap<>();
        try {
            int cc = rs.getMetaData().getColumnCount();
            for (int i = 1; i <= cc; i++) {
                String colname = rs.getMetaData().getColumnLabel(i).toLowerCase();
                map.put(colname, rs.getObject(i));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }
    // find the start and end cols for a table in a join
    // eg. select person.* , person_role.* from person join person_role
    // we want to send the first x rows to personmapper and the next y to personprop mapper

    public static int[][] getpositions(ResultSet resultSet, EntityInfo parentei, EntityInfo childei) {
        Map<Integer, Integer> pos = new HashMap<>();
        int[][] po = new int[2][2];
        List<String> usedCols;
        try {
            ResultSetMetaData md = resultSet.getMetaData();
            int startcol = 1;
            int endcol = 0;
            String currenttab = parentei.tableName;
            int totalcols = md.getColumnCount();
            for (int i = 1; i <= totalcols; i++) {
                var name = md.getColumnName(i);
                var tabname = md.getTableName(i);
                var type = md.getColumnTypeName(i);
                var label = md.getColumnLabel(i);
                var cc = md.isCaseSensitive(i);
                if (tabname.equalsIgnoreCase(currenttab)) {
                    endcol = i;
                } else {
                    // we hit the next tab
                    currenttab = childei.tableName;
                    pos.put(startcol, endcol);
                    po[0] = new int[]{startcol, endcol};
                    startcol = i;
                    endcol = i;
                }
                var x = md.getColumnDisplaySize(i);
                if (i == totalcols) {
                    pos.put(startcol, endcol);
                    po[1] = new int[]{startcol, endcol};
                }
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return po;
    }

    public static <T> T lambdaMapper(Class<T> klass, ResultSet rs, EntityInfo ei) {
        return lambdaMapper(klass, rs, ei, 0, 0);
    }

    public static <T> T lambdaMapper(Class<T> klass, ResultSet rs, EntityInfo ei, int startpos, int endpos) {
        FieldInfo fi = null;
        if (startpos == 0) startpos = 1;
        T p = null;
        try {
            int cc = rs.getMetaData().getColumnCount();
            if (endpos == 0 || endpos < startpos || endpos > cc) endpos = cc;
            p = ConstructorUtils.invokeConstructor(klass, null);
        } catch (Exception e) {
            String msg = "lambdaMapper: Class " + klass.getName();
            log.error(msg, e);
            throw new RuntimeException(msg, e);
        }

        try {
            for (int i = startpos; i <= endpos; i++) {
                String colname = rs.getMetaData().getColumnLabel(i).toLowerCase();
                if (ei.columns.containsKey(colname)) {
                    fi = ei.columns.get(colname);
                    Object value = rs.getObject(i);
                    if (value != null && fi.lambdaSetter != null) {
                        fi.lambdaSetter.accept(p, value);
                    }
                }
            }
            return p;
        } catch (Exception e) {
            String msg = "lambdaMapper: Class " + klass.getName() + " field : " + fi.fieldName;
            log.error(msg, e);
            if (!ignoreMappingErrors) throw new RuntimeException(msg, e);
        }
        return null;
    }


    public static class LambdaHelper {
        private static final MethodHandles.Lookup LOOKUP = MethodHandles.lookup();
        //https://dzone.com/articles/hacking-lambda-expressions-in-java


        public static Function createGetter(Method getter) throws Exception {
            if (getter == null) return null;
            return createGetter(LOOKUP, LOOKUP.unreflect(getter));
        }

        public static BiConsumer createSetter(Method setter) throws Exception {
            if (setter == null) return null;

            return createSetter(LOOKUP, LOOKUP.unreflect(setter));
        }

        public static Map<String, FieldInfo> getGetterAndSetterInfo(Class<?> klass) {
            Map<String, FieldInfo> result = new HashMap<String, FieldInfo>();
            BeanInfo info;
            try {
                info = Introspector.getBeanInfo(klass);
                for (PropertyDescriptor pd : info.getPropertyDescriptors()) {
                    if (!"class".equalsIgnoreCase(pd.getName())) {
                        FieldInfo fi = new FieldInfo();
                        fi.pd = pd;
                        fi.fieldName = pd.getName();
                        fi.type = pd.getPropertyType();
                        fi.getterMethod = pd.getReadMethod();
                        fi.setterMethod = pd.getWriteMethod();
                        if (fi.getterMethod != null) fi.getterName = fi.getterMethod.getName();
                        if (fi.setterMethod != null) fi.setterName = fi.setterMethod.getName();
                        result.put(fi.fieldName, fi);
                    }
                }
            } catch (IntrospectionException e) {
                //you can choose to do something here
            } finally {
                return result;
            }

        }

        private static Function createGetter(final MethodHandles.Lookup lookup, final MethodHandle getter) throws Exception {
            final CallSite site = LambdaMetafactory.metafactory(lookup,
                    "apply",
                    MethodType.methodType(Function.class),
                    MethodType.methodType(Object.class, Object.class), //signature of method Function.apply after type erasure
                    getter,
                    getter.type()); //actual signature of getter
            try {
                return (Function) site.getTarget().invokeExact();
            } catch (final Exception e) {
                throw e;
            } catch (final Throwable e) {
                throw new Error(e);
            }
        }

        private static BiConsumer createSetter(final MethodHandles.Lookup lookup, final MethodHandle setter) throws Exception {

            final CallSite site = LambdaMetafactory.metafactory(lookup,
                    "accept",
                    MethodType.methodType(BiConsumer.class),
                    MethodType.methodType(void.class, Object.class, Object.class), //signature of method BiConsumer.accept after type erasure
                    setter,
                    setter.type()); //actual signature of setter
            try {
                return (BiConsumer) site.getTarget().invokeExact();
            } catch (final Exception e) {
                throw e;
            } catch (final Throwable e) {
                throw new Error(e);
            }
        }
    }
}
