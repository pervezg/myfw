package my.fw.sqlrepo.repo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

public class ListRowMapper implements RowMapper<List<String>> {

	// extract a single row as an array of strings
	@Override
	public List<String> mapRow(ResultSet rs, int rowNum) throws SQLException {
		ArrayList<String> result = new ArrayList<String>();
		int cols = rs.getMetaData().getColumnCount();
		while (rs.next()) {
			for (int i = 1; i <= cols; i++) {
				result.add(rs.getString(i));
			}
		}
		return result;
	}

}
