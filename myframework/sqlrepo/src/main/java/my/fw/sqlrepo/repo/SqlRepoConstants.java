package my.fw.sqlrepo.repo;

public class SqlRepoConstants {
    public static class StdColumns {
        // standard names on datetime columns
        public static final String modifiedOn = "modifiedon";
        public static final String createdOn = "createdon";
        public static final String modifiedBy = "modifiedby";
        public static final String createdBy = "createdby";
    }
}
