package my.fw.sqlrepo;

import java.util.HashMap;
import java.util.Map;

public class EntityInfo {
    public String entityName;
    public String tableName;
    public String pk;
    public boolean pkAutogen;
    public Class entityClass;
  public   Map<String, FieldInfo> fields = new HashMap<>();
    public   Map<String, FieldInfo> columns = new HashMap<>();

}
