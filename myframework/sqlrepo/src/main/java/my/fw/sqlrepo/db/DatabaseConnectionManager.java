package my.fw.sqlrepo.db;

import my.fw.base.logging.MYLogFactory;
import my.fw.base.logging.MYLogHelper;
import org.slf4j.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.*;
import java.util.Map;


//https://stackoverflow.com/questions/13161747/where-do-i-have-to-place-the-jdbc-driver-for-tomcats-connection-pool
//http://tomcat.apache.org/tomcat-9.0-doc/jdbc-pool.html
//https://collaborate.pega.com/question/commons-dbcp-vs-tomcat-jdbc-pool
//http://vigilbose.blogspot.com/2009/03/apache-commons-dbcp-and-tomcat-jdbc.html

// copied from adminapi  , to serve as common component
public class DatabaseConnectionManager {

    static Logger log = MYLogFactory.getLogger("DBConnMgr");
    private static DataSource defaultDS;
    private static String defaultDSname = "";
    // used to track number of conn requests
    private static long connCount = 0;

    private DatabaseConnectionManager() {
    }

    public static DataSource getDefaultDS() {
        return defaultDS;
    }

    // sets the default DS name . at this time we assume that the entire app uses only one DS
    // TODO Pervez - enhance to manange multiple DS in a list
    public static DataSource setDefaultDS(DataSource ds) {
        defaultDS = ds;
        logDataSource(ds);
        return ds;
    }

    public static void setDefaultDSName(String dsName) {
        defaultDSname = fixContextName(dsName);
        defaultDS = getDataSrouceFromContext(dsName);
    }

    // get datasorce from java context using dsname
    // the dsname may or may not have context prefix
    public static DataSource getDataSrouceFromContext(String cxtname) {

        try {
            Context ctx = new InitialContext();
            String name = fixContextName(cxtname);
            DataSource ds = (DataSource) ctx.lookup(name);
            defaultDS = ds;
            log.info("DBConnMgr:Got datasource from JNDI context : " + name);
            logDataSource(defaultDS);
            return ds;

        } catch (NamingException e) {
            log.error("DBConnMgr:exception geting datasource from context " + defaultDSname, e);
        }
        return null;
    }

    // complete the context name if it is incomplete
    private static String fixContextName(String dsName) {
        String prefix = "java:/comp/env/jdbc/";
        if (dsName.startsWith(prefix))
            return dsName;
        return prefix + dsName;

    }


    public static void logDataSource(DataSource ds) {
        Map<String, String> map = DBConnHelper.getDataSourceInfo(ds);

        MYLogHelper.logMap(log, "DBInfo", map);
    }

    private static void logConnection(Connection c) {
        Map<String, String> map = DBConnHelper.getConnectionInfo(c);
        MYLogHelper.logMap(log, "DBInfo", map);
    }


    // gets the default ds
    public static DataSource getDS() {
        if (defaultDS == null) {
            defaultDS = getDataSrouceFromContext(defaultDSname);
        }

        return defaultDS;
    }

    public static Connection getConnection() throws SQLException {
        return getConnection(true);
    }

    public static Connection getConnection(boolean autocommitFlag) throws SQLException {

        if (defaultDS == null)
            getDataSrouceFromContext(defaultDSname);
        Connection connection = (Connection) defaultDS.getConnection();
        if (autocommitFlag == false) {
            connection.setAutoCommit(false);
        }
        connCount++;
        return connection;
    }

    public static void closeConnection(Connection connection) {
        try {
            if (connection != null && !connection.isClosed())
                connection.close();
        } catch (SQLException e) {
            log.error("DBConnMgr:exception closing connection ", e);
        }
    }

}
