package my.fw.reports;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

// a filter used in a report
public class QueryFilter {
    // name - unique inside a report
    public String name;
    public String label;
    public String filtertype;

    // query fragment with a ?
    public String sql;
    public List<ParamInfo> params;

    // list of valid options for filter value
    // example = for status filter we use NEW:"Pending approval", CLOSED:"Completed " etc.
    public Map<String, Object> options;

    public QueryFilter() {
        params = new ArrayList<>();
    }

    public QueryFilter(String name) {
        this();
        this.name = name;
    }

    public QueryFilter(String name, String colName) {
        this(name);
        this.filtertype = QueryFiltersSQL.FilterTypes.MATCHES;
        addParam(colName);
    }

    // new report with a single match filter - most common
    public QueryFilter(String name, String label, String type, String colName, String query) {
        this(name);
        this.filtertype = type;
        this.label = label;
        this.sql = query;
        addParam(colName);
    }

    public QueryFilter(String name, String label, String type, ParamInfo p) {
        this(name);
        this.filtertype = type;
        this.label = label;
        params.add(p);
    }

    // create and add a new param
    public QueryFilter addParam(String colName) {
        if (this.params == null) this.params = new ArrayList<>();
        params.add(new ParamInfo(colName, "string"));
        return this;
    }
}
