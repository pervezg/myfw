package my.fw.reports;

import java.util.HashMap;
import java.util.function.Function;

// sql version of report runner
public class QueryFiltersSQL  {

    public static class FilterTypes{
        public static final String MATCHES = "matches";
        public static final String LIKE = "like";
        public static final String BETWEEN = "between";
        public static final String BETWEENDATES = "betweendates";

    }

    static HashMap<String, Function<QueryFilter,String>> filterFunctions = new HashMap<>();

    static {
        filterFunctions.put(FilterTypes.MATCHES, QueryFiltersSQL::matches);
        filterFunctions.put(FilterTypes.LIKE, QueryFiltersSQL::like);
        filterFunctions.put(FilterTypes.BETWEEN, QueryFiltersSQL::between);
        filterFunctions.put(FilterTypes.BETWEENDATES, QueryFiltersSQL::between);


    }

    // get and run the filter function
    public static String getSql(QueryFilter f){
        if(f!=null && f.filtertype!=null && filterFunctions.containsKey(f.filtertype)){
            Function<QueryFilter, String> func = filterFunctions.get(f.filtertype);
            if(func!=null) return func.apply(f);
        }
        return null;
    }

    // creates x=? string
    static String matches(QueryFilter f) {
        if((f.params==null|| f.params.size()==0)) return  null;
        return f.params.get(0).getColname()+" = ? ";
    }
    static String between(QueryFilter f) {
        if((f.params==null|| f.params.size()==0)) return  null;
        return f.params.get(0).getColname()+" between  ?  and  ? ";
    }

    static String like(QueryFilter f) {
        if((f.params==null|| f.params.size()<2)) return  null;
        return f.params.get(0).getColname()+" like  ? ";
    }

}
