package my.fw.reports;


import my.fw.repo.MapRepo;
import my.fw.service.ISimpleSvc;
import my.fw.sqlrepo.db.DatabaseConnectionManager;
import my.fw.sqlrepo.sql.SQLRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.*;
import java.util.Map.Entry;

// reports service
// runs query and gets report data
public class ReportRunner {

    static Logger log = LoggerFactory.getLogger(ReportRunner.class);


    SQLRunner sqlRunner = new SQLRunner(DatabaseConnectionManager.getDS());
    ISimpleSvc<QueryDef,String> reportsRepo = new MapRepo<QueryDef>();

    public ReportRunner() {
    }

    public ReportRunner(ISimpleSvc<QueryDef,String> rrepo) {
        this.reportsRepo = rrepo;
    }


    public ISimpleSvc<QueryDef,String> getReportsRepo() {
        return reportsRepo;
    }

    public void setReportsRepo(MapRepo<QueryDef> reportsRepo) {
        this.reportsRepo = reportsRepo;
    }


    public QueryResult runReport(QueryRequest rr) {
        QueryDef r = reportsRepo.findOne(rr.name);
        if (r == null)
            return QueryResult.Error("NOTFOUND", "No such report : " + rr.name);
        return run(rr, r);
    }


    public QueryResult run(QueryRequest req, QueryDef rep) {
        String _method = "run";
        QueryResult result = QueryBuilder.buildQuery(req, rep);
        if (!result.isSuccessful()) return result;
        result = runReportQuery(result.sql, result.args);
        result.request = req;
        return result;

    }

    // runs an sql and returns results in a ReportResult object
    QueryResult runReportQuery(String sql, Collection<String> params) {
        String _method = "";
        QueryResult result = new QueryResult();
        result.sql = sql;
        Object[] args = {};
        if (params != null) args = params.toArray();
        List<List<String>> data = new ArrayList<List<String>>();
        ArrayList<HashMap<String, String>> table = new ArrayList<HashMap<String, String>>();
        HashMap<String, String> record = new HashMap<String, String>();
        try (Connection con = DatabaseConnectionManager.getConnection()) {
            log.trace(_method + "About to run : {}", sql);
            log.trace(_method + "Args {}", args);

            ResultSet rs = sqlRunner.getResultSet(con, sql, args);
            List<String> cols = sqlRunner.getColumns(rs);
            ResultSetMetaData md = rs.getMetaData();

            result.setColumns(cols);
            while (rs.next()) {
                ArrayList<String> rowData = new ArrayList<String>();
                for (int i = 1; i <= cols.size(); i++) {
                    String s = rs.getString(i);
                    rowData.add(s);
                    record.put(md.getColumnLabel(i), s);
                }
                data.add(rowData);
                table.add(record);
            }
            result.data = data;
            result.count = data.size();
            result.setSuccess(true);
            //jasper(result,table);

        } catch (Exception e) {
            result.setSuccess(false);
            result.setMessage("Exception");
            result.setException(e);
            log.error(_method + e.getMessage(), e);
        }
        return result;
    }

}
