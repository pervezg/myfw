package my.fw.connectors.gmail.setup;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(1)
public class EmailSetUp {

	@Value("${gmail.application.name}")
	private String applicationName;

	@Value("${gmail.token.directory.path}")
	private String tokenDirectoryPath;

	@Value("${gmail.credentials.file.path}")
	private String credentialsFilePath;

	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	public String getTokenDirectoryPath() {
		return tokenDirectoryPath;
	}

	public void setTokenDirectoryPath(String tokenDirectoryPath) {
		this.tokenDirectoryPath = tokenDirectoryPath;
	}

	public String getCredentialsFilePath() {
		return credentialsFilePath;
	}

	public void setCredentialsFilePath(String credentialsFilePath) {
		this.credentialsFilePath = credentialsFilePath;
	}

}