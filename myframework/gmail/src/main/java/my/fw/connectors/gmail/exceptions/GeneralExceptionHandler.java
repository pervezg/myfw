package my.fw.connectors.gmail.exceptions;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@SuppressWarnings({ "unchecked", "rawtypes" })
@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class GeneralExceptionHandler {

	private static final Logger logger = LoggerFactory.getLogger(GeneralExceptionHandler.class);

	@ExceptionHandler(Exception.class)
	public ResponseEntity<ExceptionResponse> handleException(Exception exception, WebRequest webRequest) {

		logger.error("Exception", exception);
		ExceptionResponse response = new ExceptionResponse("INTERNAL_SERVER_ERROR", exception.getMessage());
		return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(RuntimeException.class)
	public ResponseEntity<ExceptionResponse> handleException(RuntimeException exception, WebRequest webRequest) {

		logger.error("RuntimeException", exception);
		ExceptionResponse response = new ExceptionResponse("INTERNAL_SERVER_ERROR", exception.getMessage());
		return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(IOException.class)
	public ResponseEntity<ExceptionResponse> handleExceptions(IOException exception, WebRequest webRequest) {

		logger.error("IOException", exception);
		ExceptionResponse response = new ExceptionResponse("BAD REQUEST", exception.getMessage());
		return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(BadRequestException.class)
	public ResponseEntity<ExceptionResponse> handleExceptions(BadRequestException exception, WebRequest webRequest) {

		logger.error("BadRequestException", exception);
		ExceptionResponse response = new ExceptionResponse("BAD REQUEST", exception.getMessage());
		return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(NotFoundException.class)
	public ResponseEntity<ExceptionResponse> handleExceptions(NotFoundException exception, WebRequest webRequest) {

		logger.error("NotFoundException", exception);
		ExceptionResponse response = new ExceptionResponse("NOT FOUND", exception.getMessage());
		return new ResponseEntity(response, HttpStatus.NOT_FOUND);
	}

}
