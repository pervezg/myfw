package my.fw.connectors.gmail.service;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import my.fw.connectors.gmail.exceptions.BadRequestException;
import my.fw.connectors.gmail.exceptions.NotFoundException;
import my.fw.connectors.gmail.setup.EmailSetUp;
import my.fw.integration.AbstractBaseConnector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.client.util.Base64;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.GmailScopes;
import com.google.api.services.gmail.model.Message;

@Component
public class GmailService extends AbstractBaseConnector {

    private static final Logger logger = LoggerFactory.getLogger(GmailService.class);

//    @Autowired
//    private EmailSetUp emailsetup;

    private final JsonFactory JSON_FACTORY = GsonFactory.getDefaultInstance();

    /**
     * Global instance of the scopes required. If modifying these scopes, delete
     * your previously saved tokens/ folder.
     */
    private final List<String> SCOPES = Collections.singletonList(GmailScopes.GMAIL_SEND);

    public GmailService() {
        super("gmail");
        this.description="Gmail connector";
    }

    /**
     * Creates an authorized Credential object.
     *
     * @param HTTP_TRANSPORT The network HTTP Transport.
     * @return An authorized Credential object.
     * @throws IOException If the credentials.json file cannot be found.
     */
    private Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) throws IOException {
        // Load client secrets.
      //  InputStream in = GmailService.class.getResourceAsStream(emailsetup.getCredentialsFilePath());
        String fname=settings.get("gmail.credentials.file.path");
        String tokendir=settings.get("gmail.token.directory.path");
        InputStream in = GmailService.class.getResourceAsStream(fname);
        if (in == null) {
            logger.debug("FileNotFoundException: {} " , fname);
            throw new NotFoundException("Resource not found: " + fname);
        }
        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

        // Build flow and trigger user authorization request.
        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT, JSON_FACTORY,
                clientSecrets, SCOPES)
                .setDataStoreFactory(
                        new FileDataStoreFactory(new File(tokendir)))
                .setAccessType("offline").build();

        LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(8888).build();

        return new AuthorizationCodeInstalledApp(flow, receiver).authorize("user");
    }

    // Build a new authorized API client service.
    private Gmail clientService() throws GeneralSecurityException, IOException {

        final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
        Gmail service = new Gmail.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
                .setApplicationName(settings.get("gmail.application.name")).build();

        return service;
    }

    /**
     * Create a MimeMessage using the parameters provided.
     *
     * @param to       email address of the receiver
     * @param from     email address of the sender, the mailbox account
     * @param subject  subject of the email
     * @param bodyText body text of the email
     * @return the MimeMessage to be used to send email
     * @throws MessagingException
     */
    public MimeMessage createEmailWithText(String from, Set<String> toList, Set<String> ccList, String subject,
                                           String bodyText) throws MessagingException {

        logger.debug("From: " + from);
        logger.debug("To: " + toList.toString());
        logger.debug("CC: " + ccList.toString());
        logger.debug(subject);

        Set<InternetAddress> toAddresses = new HashSet<InternetAddress>();
        toList.stream().forEach((ele) -> {
            try {
                toAddresses.add(new InternetAddress(ele));
            } catch (AddressException e) {
            }
        });

        Set<InternetAddress> ccAddresses = new HashSet<InternetAddress>();
        ccList.stream().forEach((ele) -> {
            try {
                ccAddresses.add(new InternetAddress(ele));
            } catch (AddressException e) {
                logger.debug(e.getMessage());
            }
        });

        Properties props = new Properties();
        Session session = Session.getDefaultInstance(props, null);

        MimeMessage email = new MimeMessage(session);

        email.setFrom(new InternetAddress(from));
        email.addRecipients(javax.mail.Message.RecipientType.TO,
                toAddresses.toArray(new InternetAddress[toAddresses.size()]));
        email.addRecipients(javax.mail.Message.RecipientType.CC,
                ccAddresses.toArray(new InternetAddress[ccAddresses.size()]));
        email.setSubject(subject);
        email.setText(bodyText);

        return email;
    }

    /**
     * Send an email from the user's mailbox to its recipient.
     *
     * @param service      Authorized Gmail API instance.
     * @param userId       User's email address. The special value "me" can be used
     *                     to indicate the authenticated user.
     * @param emailContent Email to be sent.
     * @return The sent message
     * @throws MessagingException
     * @throws IOException
     * @throws GeneralSecurityException
     */
    public Message sendMessage(MimeMessage emailContent)
            throws MessagingException, IOException, GeneralSecurityException {

        String userId = "me";

        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        emailContent.writeTo(buffer);
        byte[] bytes = buffer.toByteArray();
        String encodedEmail = Base64.encodeBase64URLSafeString(bytes);
        Message message = new Message();
        message.setRaw(encodedEmail);

        message = clientService().users().messages().send(userId, message).execute();

        return message;
    }

    /**
     * Create a MimeMessage using the parameters provided.
     *
     * @param to       Email address of the receiver.
     * @param from     Email address of the sender, the mailbox account.
     * @param subject  Subject of the email.
     * @param bodyText Body text of the email.
     * @param file     Path to the file to be attached.
     * @return MimeMessage to be used to send email.
     * @throws MessagingException
     */
    public MimeMessage createEmailWithAttachment(String from, Set<String> toList, Set<String> ccList, String subject,
                                                 String bodyText, File file) throws MessagingException, IOException {

        logger.debug("From: " + from);
        logger.debug("To: " + toList.toString());
        logger.debug("CC: " + ccList.toString());
        logger.debug(subject);

        Set<InternetAddress> toAddresses = new HashSet<InternetAddress>();
        toList.stream().forEach((ele) -> {
            try {
                toAddresses.add(new InternetAddress(ele));
            } catch (AddressException ex) {
                logger.debug(ex.getMessage());
                throw new BadRequestException(ex.getMessage());
            }
        });

        Set<InternetAddress> ccAddresses = new HashSet<InternetAddress>();
        ccList.stream().forEach((ele) -> {
            try {
                ccAddresses.add(new InternetAddress(ele));
            } catch (AddressException ex) {
                logger.debug(ex.getMessage());
                throw new BadRequestException(ex.getMessage());
            }
        });

        Properties props = new Properties();
        Session session = Session.getDefaultInstance(props, null);

        MimeMessage email = new MimeMessage(session);

        email.setFrom(new InternetAddress(from));
        email.addRecipients(javax.mail.Message.RecipientType.TO,
                toAddresses.toArray(new InternetAddress[toAddresses.size()]));
        email.addRecipients(javax.mail.Message.RecipientType.CC,
                ccAddresses.toArray(new InternetAddress[ccAddresses.size()]));
        email.setSubject(subject);
        MimeBodyPart mimeBodyPart = new MimeBodyPart();
        mimeBodyPart.setContent(bodyText, "text/plain");

        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(mimeBodyPart);

        mimeBodyPart = new MimeBodyPart();
        DataSource source = new FileDataSource(file);

        mimeBodyPart.setDataHandler(new DataHandler(source));
        mimeBodyPart.setFileName(file.getName());

        multipart.addBodyPart(mimeBodyPart);
        email.setContent(multipart);

        return email;
    }

    @Override
    public Class<?> getType() {
        return GmailService.class;
    }
}
