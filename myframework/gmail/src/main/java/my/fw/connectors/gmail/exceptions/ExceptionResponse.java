package my.fw.connectors.gmail.exceptions;

public class ExceptionResponse {

	private String error;
	private String message;

	public ExceptionResponse(String error, String message) {
		this.error = error;
		this.message = message;
	}

	public String getError() {
		return error;
	}

	public String getMessage() {
		return message;
	}

}
