package my.fw.connectors.gmail.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class NotFoundException extends RuntimeException {

	private static final long serialVersionUID = 3602625673908594194L;

	public NotFoundException(String msg) {
		super(msg);
	}

}
