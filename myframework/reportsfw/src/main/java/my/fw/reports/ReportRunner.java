package my.fw.reports;

import my.fw.repo.MyMap;
import my.fw.sqlrepo.db.DatabaseConnectionManager;
import my.fw.sqlrepo.repo.ListRowMapper;
import my.fw.sqlrepo.sql.SQLRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.*;
import java.util.Map.Entry;

// reports service
// runs query and gets report data
public class ReportRunner {

    static Logger log = LoggerFactory.getLogger(ReportRunner.class);
    public static final String SortDirection = "sortdir";
    public static final String SortColumn = "sortcol";

    SQLRunner sqlRunner = new SQLRunner(DatabaseConnectionManager.getDS());
    ListRowMapper _mapper = new ListRowMapper();
    MyMap<QueryDef> reportsRepo = new MyMap<QueryDef>();

    public ReportRunner() {
    }

    public ReportRunner(MyMap<QueryDef> rrepo) {
        this.reportsRepo = rrepo;
    }


    public MyMap<QueryDef> getReportsRepo() {
        return reportsRepo;
    }

    public void setReportsRepo(MyMap<QueryDef> reportsRepo) {
        this.reportsRepo = reportsRepo;
    }


    public QueryResult runReport(QueryRequest rr) {
        QueryDef r = reportsRepo.findOne(rr.name);
        if (r == null)
            return QueryResult.Error("NOTFOUND", "No such report : " + rr.name);
        return run(rr, r);
    }


    public QueryResult run(QueryRequest req, QueryDef rep) {
        String _method = "run";
        QueryResult result = constructQuery(req, rep);
        if (!result.isSuccessful()) return result;

//        QueryResult runResult = runReportQuery(result.sql, result.args);
//        return runResult;
         result = runReportQuery(result.sql, result.args);
        return result;

    }

    // creates the query based on filters and params
    // we pass the Result object to various addXXX methods that incrementally build up the
    // fragments of the query
    public static QueryResult constructQuery(QueryRequest req, QueryDef rep) {
        String _method = "constructQuery";
        QueryResult result = new QueryResult();
        result.setSuccess(true);


        // first lets add mandatory params from reqinfo
        // return fail if any param is missing
        result = addParams(req, rep, result);
        if (!result.isSuccessful()) return result;


        // create a where clause using rfilters
        // for each filter,  add all the sql and params to list - we assume that there is an accurate mapping of args and
        // ? placeholders
        if (req.filterparams != null && req.filterparams.size() > 0) {
            result = addWheresWithFilterParams(req, rep, result);
        } else {
            if (req.filters != null && req.filters.size() > 0) {
                result = addWheresWithQueryFilter(req, rep, result);
            }
        }
        if (!result.isSuccessful()) return result;

        String orderby = getOrderby(req.sorting);
        String limits = getLimitOffset(req);
        // now construct the sql
        // 1. add wheres
        String sql = combine(rep.getQuery(), result.wheres);
        // 2. order by
        if (orderby != null) sql += " " + orderby;
        // 3. limit clause
        if (limits != null) sql += " " + limits;

        result.sql = sql;
        return result;
    }

    // adds mandatory params to the queryresult
    static QueryResult addParams(QueryRequest req, QueryDef rep, QueryResult result) {
        result.setSuccess(true);
        for (ParamInfo pi : rep.params) {
            String val = req.params.get(pi.name);
            if (val != null) {
                result.args.add(val);
            } else {
                String msg = "Report : " + rep.getName() + " is missing mandatory parameter : " + pi.name;
                log.warn(msg);
                result.status = false;
                result.message = result.message + " - " + msg;
            }
        }
        return result;
    }

    // using simplified filterparams mode
    static QueryResult addWheresWithFilterParams(QueryRequest req, QueryDef rep, QueryResult result) {
        String _method = "addWheresWithFilterParams";
        for (String filterName : req.filterparams.keySet()) {
            QueryFilter repFilter = getFilter(filterName, rep);
            if (repFilter != null) {
                String _w = QueryFiltersSQL.getSql(repFilter);
                if (_w != null) {
                    Collection<String> paramValues = req.filterparams.get(filterName);
                    if (paramValues != null && paramValues.size() != 0) {
                        result.wheres.add(_w);
                        result.args.addAll(paramValues);
                        log.trace(_method + "Report {} : Filter {} : Added SQL {}", req.name, filterName, _w);
                        log.trace(_method + "Report {} : Filter {} : Added Args {}", req.name, filterName, paramValues);
                    } else {

                        log.warn(_method + " Report {} : Cannot process filter {} due to missing params");
                        result.setSuccess(false);
                        result.message += "Cannot process filter " + filterName + " due to missing params";
                    }
                }
            } else {
                log.trace(_method + "Invalid Filter: Could not find filter {} in report {}", filterName, req.name);
            }
        }
        return result;
    }

    static QueryResult addWheresWithQueryFilter(QueryRequest req, QueryDef rep, QueryResult result) {
        String _method = "addWheresWithQueryFilter";
        for (QueryFilter reqf : req.filters) {
            QueryFilter repFilter = getFilter(reqf.name, rep);
            if (repFilter != null) {
                String _w = QueryFiltersSQL.getSql(repFilter);
                if (_w != null) {
                    log.trace(_method + "Report {} : Filter {} : Added SQL {}", req.name, reqf.name, _w);
                    result.wheres.add(_w);
                    for (Param p : reqf.params) {
                        if (p.value != null) {
                            result.args.add(p.value);
                        } else {
                            if (p.values != null) {
                                result.args.addAll(Arrays.asList(p.values));
                            }
                        }
                    }
                }
            } else {
                log.trace(_method + "Invalid Filter: Could not find filter {} in report {}", reqf.name, req.name);
            }
        }
        return result;
    }

    private static String combine(String sql, List<String> wheres) {

        // add where is missing
        // this is required since many sql will contain a where for joining tables
        if (!sql.toLowerCase().contains(" where ")) sql += " where ";
        for (String w : wheres) {
            sql += " and " + w + " ";
        }
        return sql;
    }


    static QueryFilter getFilter(String name, QueryDef rr) {
        if (rr.filters != null) {
            Optional<QueryFilter> of = rr.filters.stream().filter(f -> name.equalsIgnoreCase(f.name)).findFirst();
            if (of.isPresent()) return of.get();
        }
        return null;
    }

    // runs an sql and returns results in a ReportResult object
    QueryResult runReportQuery(String sql, Collection<String> params) {
        String _method = "";
        QueryResult result = new QueryResult();
        result.sql = sql;
        Object[] args = {};
        if (params != null) args = params.toArray();
        List<List<String>> data = new ArrayList<List<String>>();
        ArrayList<HashMap<String, String>> table = new ArrayList<HashMap<String, String>>();
        HashMap<String, String> record = new HashMap<String, String>();
        try (Connection con = DatabaseConnectionManager.getConnection()) {
            log.trace(_method + "About to run : {}", sql);
            log.trace(_method + "Args {}", args);

            ResultSet rs = sqlRunner.getResultSet(con, sql, args);
            List<String> cols = sqlRunner.getColumns(rs);
            ResultSetMetaData md = rs.getMetaData();

            result.setColumns(cols);
            while (rs.next()) {
                ArrayList<String> rowData = new ArrayList<String>();
                for (int i = 1; i <= cols.size(); i++) {
                    String s = rs.getString(i);
                    rowData.add(s);
                    record.put(md.getColumnLabel(i), s);
                }
                data.add(rowData);
                table.add(record);
            }
            result.data = data;
            result.count = data.size();
            result.setSuccess(true);
            //jasper(result,table);

        } catch (Exception e) {
            result.setSuccess(false);
            result.setMessage("Exception");
            result.setException(e);
            log.error(_method + e.getMessage(), e);
        }
        return result;
    }

    // returns a compound orderby clause , null if no elements in map
    private static String getOrderby(Map<String, String> sorting) {
        String orderby = null;
        boolean isFrist = true;
        // order by clause
        if (sorting != null && sorting.size() > 0) {
            for (Entry<String, String> s : sorting.entrySet()) {
                String col = s.getKey();
                // prepend , if not first
                col = isFrist ? col : " , " + col;

                String direction = s.getValue();
                direction = (direction == null || direction.isEmpty() || !direction.equalsIgnoreCase("desc")) ? "asc"
                        : "desc";

                if (isFrist) {
                    isFrist = false;
                    orderby = " order by " + col + " " + direction + " ";
                } else {
                    orderby += " , " + col + " " + direction + " ";
                }
            }
        }
        return orderby;
    }

    // return limit/offest clause else null
    public static String getLimitOffset(QueryRequest rep) {
        String l = null;
        if (rep.row_count > 0)
            l = " LIMIT " + (rep.row_offset > 0 ? rep.row_offset + " , " : "") + rep.row_count + " ";
        return l;
    }

}
