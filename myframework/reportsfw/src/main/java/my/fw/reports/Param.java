package my.fw.reports;

// paramater in a report query
// 1. we use this to describe a param and also get the values in a query
// 2. we are using to value objects so that its easy to pass a single or multiple values
public class Param {
    // name of the param , should match col name
    public String name;
    // datatype - just string for now
    public String datatype="string";
    // value or values depending datatype (e.g. where status in (a,b,v);
    public String value;
    public String[] values;

    public static class DataType{
        public static final String STRING="string";
        public static final String DATE="date";

    }

    public Param() {
    }

    public Param(String name, String datatype) {
        this.name = name;
        this.datatype = datatype;
    }
}
