package my.fw.reports;

import my.fw.model.BaseObjWithName;
import my.fw.model.IObjWithId;
import my.fw.model.IObjWithName;

import java.util.*;
import java.util.function.BiFunction;

// definition of a report
public class QueryDef extends BaseObjWithName implements IObjWithName, IObjWithId<String> {
	// descriptive title of the report
	public String title;

	public String query;

	// predefined//mandatory params that should be passed to else the query is incomplete .
	// for sql these are ? params that are part of the query
	// these are not filters even though they look similar
	// filters are expected to be modified by the caller whereas params are expected to
	// be passed via code
	// parmas should be in the order expected in the query
	public List<ParamInfo> params;

	// list of filters that this report can support
	// there are like params but are optional - query can run without filters but not without params
	public List<QueryFilter> filters;

	// a function that implements the report query
	public BiFunction<QueryRequest, QueryDef, QueryResult> queryFunc;

	public QueryDef() {
		filters =new ArrayList<>();
		params=new ArrayList<>();
	}

	public QueryDef(String name, BiFunction<QueryRequest, QueryDef, QueryResult> f) {
		super();
		this.name=name;
		this.queryFunc =f;
	}
	public QueryDef(String name, String sql) {
		super();
		this.query=sql;
		this.name=name;
	}

	// add or replace filter
	public void addFilter(QueryFilter f){
		if(filters ==null) filters = new ArrayList<>();
		filters.removeIf(y->y.name.equals(f.name));
		filters.add(f);
	}

	public QueryFilter getFilter(String name){
		if(name==null||name=="") return null;
		Optional<QueryFilter> f = filters.stream().filter(y -> name.equalsIgnoreCase(y.name)).findFirst();
		if(f.isPresent()) return f.get();
		return null;
	}

	public void addParam(ParamInfo f){
		if(params==null) params= new ArrayList<>();
		params.removeIf(y->y.name.equals(f.name));
		params.add(f);
	}


	public ParamInfo getParamString (String name){
		if(name==null||name=="") return null;
		Optional<ParamInfo> f = params.stream().filter(y -> name.equalsIgnoreCase(y.name)).findFirst();
		if(f.isPresent()) return f.get();
		return null;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	@Override
	public String getId() {
		return name;
	}

	@Override
	public void setId(String _id) {
		setName(_id);
	}

}
