package my.fw.reports;


import my.fw.model.Result;

import java.util.ArrayList;
import java.util.List;

public class QueryResult extends Result {


    // count and sql are only for debugging
    public long count;
    public String sql;
    public List<String> args;
    List<String> wheres = new ArrayList<>();
    // column headings
    private List<String> headings;
    private QueryDef query;

    public QueryResult() {
        this.args=new ArrayList<>();
        this.wheres=new ArrayList<>();
        this.headings=new ArrayList<>();
    }

    public QueryResult(boolean success, String errcode, String msg) {
        setSuccess(success);
        setErrorCode(errcode);
        setMessage(msg);
    }

    public static QueryResult Error(String errcode, String msg) {

        return new QueryResult(false, errcode, msg);
    }

    public QueryDef getQuery() {
        return query;
    }

    public void setQuery(QueryDef query) {
        this.query = query;
    }

    public List<String> getColumns() {
        return headings;
    }

    public void setColumns(List<String> columns) {
        this.headings = columns;
    }
}
