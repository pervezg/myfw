package my.fw.reports;

import java.util.*;

public class QueryRequest {

    // name of the query
    // this is required in cases like reports where we want to apply filters etc
    // to a predefined Query. In other cases where we just use queryrequest for filtering
    // inside a svc or repo we dont need to provide name
    public String name;

    // values of params that are supported by the report definition
    // missing params will cause exception while running report
    // excess params will be ignored
    public Map<String, String> params;

    // filters to be applied , currently all filters will combined with an AND
    // we can use QuertFilters or a simple map to pass filter info

    public List<QueryFilter> filters;
    // queryfilters are relativelty verbose so we can just use filterparams that pass
    // column-value[] objects
    public Map<String, Collection<String>> filterparams;


    // sorting info as pair of columnname:asc|desc
    public Map<String, String> sorting;

    // format of output - eg. html, json , pdf etc
    public String format;
    public int row_offset;
    public int row_count;

    public QueryRequest() {
        filters = new ArrayList<>();
        filterparams = new HashMap<>();
        params = new HashMap<>();
        sorting = new HashMap<String, String>();
    }

}
