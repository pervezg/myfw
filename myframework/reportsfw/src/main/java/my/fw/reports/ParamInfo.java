package my.fw.reports;

// adds display related info to a report Parsm
public class ParamInfo extends Param {
    public String label;
    public String desc;
    public String help;

    public ParamInfo() {
        super();
    }

    public ParamInfo(String name, String datatype) {
        super(name, datatype);
    }

    public ParamInfo(String name) {
        super(name,"string");
    }
}
