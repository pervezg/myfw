package my.fw.ehcache;

import my.fw.base.cache.IReadonlyCache;
import my.fw.model.IObjWithId;
import my.fw.sqlrepo.EntityInfo;
import my.fw.sqlrepo.EntityInfoFactory;
import my.fw.sqlrepo.repo.GenericSQLRepo;

import java.util.List;

public class GenericTableEhCache<T extends IObjWithId<ID>, ID> extends GenericSimpleEhcache<T, ID> implements IReadonlyCache<T,ID> {

    private EntityInfo einfo;

    public GenericTableEhCache() {

    }

    // creates a new chache , and loaded from table.
    public GenericTableEhCache(Class<T> c) {
        super(c);
        this.einfo = EntityInfoFactory.getEntityInfo(this._class);
        this.cacheName = ECacheFactory.getCacheName(c);
        loadCache();
        TRACE("CTOR", "Cache created:" + cacheName);
    }


    // we create the repo only when required to load
    private void loadCache() {
        try (GenericSQLRepo<T, ID> repo = new GenericSQLRepo<T, ID>(_class, einfo);) {
            List<T> l = repo.findAll();
            loadCache(l);
            TRACE("initCache", "Cache initialized:" + cacheName);
        } catch (Exception e) {
            ERROR(e, "loadcache", "");
        }
    }

}
