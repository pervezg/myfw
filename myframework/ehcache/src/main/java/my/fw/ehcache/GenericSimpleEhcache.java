package my.fw.ehcache;

import my.fw.base.cache.IReadonlyCache;
import my.fw.model.BaseObject;
import my.fw.model.IObjWithId;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;

import java.util.ArrayList;
import java.util.List;

// very basic wrapper on an ehcahce with findone and getall methods.
public class GenericSimpleEhcache<T extends IObjWithId<ID>, ID> extends BaseObject implements IReadonlyCache<T, ID> {

    protected Class<T> _class;
    protected String cacheName;
    int maxEntries = 6000;
    private Ehcache _cache;

    public GenericSimpleEhcache() {

    }

    // adding class name to make naming more java centric
    public GenericSimpleEhcache(Class<T> c) {
        this._class = c;
        this.cacheName = ECacheFactory.getCacheName(c);
        _cache = ECacheFactory.addNewEhache(this.cacheName);
    }

    public T findOne(ID id) {
        Element o = _cache.get(id);
        if (o != null) {
            T t = (T) o.getObjectValue();
            return t;
        }
        return null;
    }

    public <T extends IObjWithId<ID>, ID> void loadCache(Ehcache _e, List<T> _all) {
        _e.removeAll();
        for (T t : _all) {
            _e.put(new Element(t.getId(), t));
        }
    }

    public void loadCache(List<T> all) {
        loadCache(this._cache, all);
        TRACE("loadcache", "Loaded cache:" + cacheName + " count:" + all.size());
    }

    public List<T> findAll() {
        ArrayList<T> result = new ArrayList<T>();
        List keys = _cache.getKeys();
        for (Object id : keys) {
            Element o = _cache.get(id);
            result.add((T) o.getObjectValue());
        }
        return result;
    }

    @Override
    public String getName() {
        return cacheName;
    }
}
