package my.fw.ehcache;

import my.fw.base.cache.IReadonlyCache;
import my.fw.model.IObjWithId;
import my.fw.sqlrepo.ISQLRepo;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.config.CacheConfiguration;
import net.sf.ehcache.config.Configuration;
import net.sf.ehcache.config.Searchable;
import net.sf.ehcache.search.Attribute;

import java.util.HashMap;

public class ECacheFactory {

    public ECacheFactory() {
    }

    private static CacheManager cacheManager = null;
    private static HashMap<String, Attribute<?>> searchAttributes = new HashMap<String, Attribute<?>>();
    private static HashMap<String, IReadonlyCache> _cachemap;

    static {
        init();
    }

    public static void init() {
        Configuration cacheManagerConfig = new Configuration();
        cacheManager = new CacheManager(cacheManagerConfig);
        _cachemap = new HashMap<String, IReadonlyCache>();
    }

    public static void addCache(Ehcache e) {
        cacheManager.addCache(e);
    }

    // get a cache . we cannot create a new one since we dont have other params
    // designed to throw exception since we dont want to miss code that reaches for
    // uninitialized caches
    public static <T> IReadonlyCache getCache(Class<T> c) {
        return (getCache(getCacheName(c)));
    }

    public static <T> IReadonlyCache getCache(String c) {
        if (existsCache(c))
            return _cachemap.get(c);
        throw new RuntimeException("Cache for class " + c + " not found");
    }

    // get a cache or create a new one without exceptions
    public static <T> Ehcache getCacheQuiet(Class<T> c) {
        String name = getCacheName(c);
        return cacheManager.getCache(name);
    }

    public static <T> Boolean existsCache(Class<T> c) {
        String name = getCacheName(c);
        return existsCache(name);
    }

    public static <T> Boolean existsCache(String name) {
        return _cachemap.containsKey(name.trim().toLowerCase());
    }

    // get name based on class- common method for consistent naming of caches
    // we use simple name and lowercase since name needs to be matched from request
    // coming from API/UI
    public static <T> String getCacheName(Class<T> c) {
        return c.getSimpleName().toLowerCase();
    }

    public static Ehcache addNewEhache(String cacheName) {
        CacheConfiguration cacheConfig = new CacheConfiguration(cacheName, 6000).eternal(true);
        cacheConfig.addSearchable(new Searchable());
        Ehcache ecache = new Cache(cacheConfig);
        cacheManager.addCache(ecache);
        return ecache;
    }


    // add a new table cache or get existing one from cachemap
    public static <T extends IObjWithId<ID>, ID> GenericTableEhCache<T, ID> addTableCache(Class<T> c) {
        if (!existsCache(c)) {
            GenericTableEhCache<T, ID> _c = new GenericTableEhCache<T, ID>(c);
            _cachemap.put(getCacheName(c), _c);
        }
        return (GenericTableEhCache<T, ID>) getCache(c);
    }

    private static <T, ID> String getCacheName(ISQLRepo<T, ID> _r) {
        return _r.getTableName().toLowerCase();
    }

    public static <T extends IObjWithId<ID>, ID> Ehcache getCache(ISQLRepo<T, ID> _r) {
        String _cacheName = getCacheName(_r);
        return cacheManager.getCache(_cacheName);
    }
//
//	public static <T extends IObjWithId<ID>, ID> void loadCache(Ehcache _e, ISQLRepo<T, ID> _r) {
//		Iterable<T> _all = _r.findAll();
//		for (T t : _all) {
//			_e.put(new Element(t.getId(), t));
//		}
//	}

}
