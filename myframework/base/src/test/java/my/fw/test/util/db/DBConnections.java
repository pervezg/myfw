package my.fw.test.util.db;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class DBConnections {

    public static MysqlDataSource getmysqlDS(String host, String schema, String uid, String pwd) {
        return getmysqlDS(host, schema, uid, pwd, 3360);
    }

    public static MysqlDataSource getmysqlDS(String host, String schema, String uid, String pwd, int port) {
        MysqlDataSource dataSource = new MysqlDataSource();
        dataSource.setDatabaseName(schema);
        dataSource.setUser(uid);
        dataSource.setPassword(pwd);
        dataSource.setServerName(host);
        return dataSource;
    }



    // dump info on db connection in a map.
    // we need to open a connection to get this info.
    public static Map<String, String> getDataSourceInfo(DataSource ds) {
        Map<String, String> map = new HashMap<String, String>();

        try {

            if (ds == null)
                return map;
            Connection c = ds.getConnection();

            map.put("DS Class", ds.getClass().toString());

            // show mysql related info in log
            MysqlDataSource myds = (MysqlDataSource) ds;
            if (myds != null) {
                map.put("MySQL - DatabaseName", myds.getDatabaseName());
                map.put("MySQL - ConnAttr", myds.getConnectionAttributes());
                map.put("MySQL - Timeout", Integer.toString(myds.getConnectTimeout()));

            }
        } catch (Exception e) {
            // e.printStackTrace();
        }
        return map;
    }

    // utility method for use during testing
    public static Connection getMySQlConnection(String host, int port, String schema, String userName, String password)
            throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {

        DriverManager.registerDriver((java.sql.Driver) Class.forName("com.mysql.cj.jdbc.Driver").newInstance());

        port = port == 0 ? 3306 : port;
        String url = "jdbc:mysql://" + host + ":" + port + "/" + schema;

        Connection connection = DriverManager.getConnection(url, userName, password);
        return connection;
    }
}
