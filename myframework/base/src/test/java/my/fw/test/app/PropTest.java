package my.fw.test.app;

import my.fw.base.app.App;
import my.fw.base.system.MyProperties;
import org.junit.Test;

import java.io.IOException;
import java.util.Properties;

import static org.junit.Assert.*;

public class PropTest {
    String appname = "myapp";
    String modname = "testapp";
    public static String testHostName = "hostel";

//    // initialize app properties
//    @Test
//    public void AppTest() throws IOException, IllegalAccessException {
//
//        var app = new App(modname, appname);
//
//        // init from properties in myapp.properties file in test/resources
//        AppFactory.initAppProperties(this.getClass(), app);
//        AppFactory.getAppProperty(app, "log.level");
//        // we expect some props from the file but the count keeps changesing so just test for >0
//        assertTrue(app.propsFromFile.size() > 0);
//
//        // test specific values.
//        Properties p = app.getProps();
//        assertTrue(p.size() > 0);
//        assertEquals("moduleprop1", p.getProperty("module.prop1"));
//        assertEquals("true", p.getProperty("boolean.true"));
//        assertEquals("false", p.getProperty("boolean.false"));
//        assertEquals("1", p.getProperty("integer"));
//        assertEquals("\"''\"", p.getProperty("quotes"));
//        assertEquals("INFO", p.getProperty("log.level"));
//        assertEquals("DEBUG", p.getProperty(modname + "." + "log.level"));
//        assertEquals("DEBUG", AppFactory.getAppProperty(app, "log.level"));
//        assertNull(p.getProperty("noapp.prop1"));
//        assertNull(p.getProperty("comment"));
//
//        assertEquals(testHostName, TestProps.DB.host);
//        MyProperties.populateStatic(TestProps.class, p, "");
//        assertEquals( "localhost",TestProps.DB.host);
//
//    }
//



    @Test
    public void populateStaticTest() throws IllegalAccessException {
        Properties props = getprops();
        var t = new TestProps();
        assertEquals(TestProps.DB.host, testHostName);
        assertEquals(false, TestProps.DB.usessl);
        assertEquals(333, TestProps.DB.timeout);
        assertEquals(333, TestProps.DB.port);

        MyProperties.populateStatic(TestProps.class, props,null);
        assertEquals(3306, TestProps.DB.port);

        assertEquals("ACTIVE", TestProps.DB.Status.Active);
        assertEquals(false, TestProps.DB.usessl);
        //assertEquals(999999999, TestProps.DB.timeout);

        assertNotEquals(TestProps.DB.host, testHostName);

    }



    // simulated property file  which will overwrite different members in TestProps
    Properties getprops() {
        Properties props = new Properties();
        props.put("myhost", "hosty");
        props.put("timeout", 999.99);
        props.put("db.port", 3306);
        props.put("db.host", "otherhost");
        props.put("db.status.active", "ACTIVE");
        props.put("usessl", false);

        return props;

    }

    // sample test class for testing staticclasspopulating
    // used to map values from myapp.properties
    public static class TestProps {
        public static class DB {
            public static String host = PropTest.testHostName;
            public static int port = 333;
            public static long timeout = 333;
            public static boolean usessl = false;

            public static class Status {
                public static String Active = "";
            }
        }
        public static class Log {
            public static String level = "INFO";
        }
    }

}
