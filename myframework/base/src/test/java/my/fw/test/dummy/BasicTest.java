package my.fw.test.dummy;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Map;

import org.junit.Test;

import my.fw.base.crypto.SHAHelper;
import my.fw.base.system.OSHelper;

// very basic test so that we have atleast one test to use in CI pipelines
public class BasicTest {

	public BasicTest() {

	}

	@Test
	public void GetEnvTest() {
		// get all env vars
		// test that there are more then 0

		Map<String, String> evlist = OSHelper.getAllEnv();
		assertNotNull(evlist);
		assertTrue(evlist.size() > 0);
	}

	@Test
	// get a non existent EV and assert false
	public void GetFakeEV() {
		// get all env vars
		// test that there are more then 0

		String evlist = OSHelper.getEnvVar("dsfgdsgew23235fdgfdgdf");
		assertNull(evlist);
	}

	@Test
	public void SHATest() {
		String clear = "quickbrownfox";
		String test = "464cb013effbc2f6b98e8a136f3e5ac4f8a266f38dd5ea6292314d9b16b28e73";
		String sha = SHAHelper.sha256(clear);
		assertNotNull(clear);
		assertFalse(clear.isEmpty());
		assertEquals(test, sha);

	}

}
