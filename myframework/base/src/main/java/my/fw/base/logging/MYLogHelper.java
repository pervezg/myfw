package my.fw.base.logging;

import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;

public class MYLogHelper {

	// dump map to log in lines and json format

	public static void logMap(Logger log, String prefix, Map<String, String> sortedMap) {
		try {

			int i = 1;
			log.info(prefix + "  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
			for (Entry<String, String> entry : sortedMap.entrySet()) {
				log.info(i + " " + prefix + "\t-:" + entry.getKey() + "   \t\t:" + entry.getValue());
				i++;
			}
			log.info(prefix + "  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
			String s;
			//s = JSONHelper.serialize(sortedMap);
		//	log.info(s);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
