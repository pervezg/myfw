package my.fw.base.cache;

import java.util.List;

//simple in-memory cache using ehcache
public interface IReadonlyCache<T, ID> {

	// name of cache
	String getName();

	// get all elements
	List<T> findAll();

	// find one
	T findOne(ID id);

	// init cache from list
	void loadCache(List<T> all);

}
