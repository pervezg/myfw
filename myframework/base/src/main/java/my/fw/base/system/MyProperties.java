package my.fw.base.system;

import my.fw.base.app.App;
import my.fw.base.app.MYAppConstants;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

import java.io.*;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class MyProperties {
    // if true the convert prop names to lowercase and remove extra/special char
    public static boolean formatNames = true;
    public static boolean useCommonsConfig = true;

    // populates the environment for the app.
    // we look for env vars to locate the file and then
    // load properties
    // different prop (db , etc) are stored in different fields.

    public static App initAppProperties(App app, Class<?> klass) {
        // first create a list of propfilenames
        String appcode = app.getAppcode();
        Collection<String> fnames = app.getPropFileShortNames();

        // 1 - load all env vars that start with appcode e.g. APPCODEHOME
        // this will capture any env vars starting with appcode, including home etc
        app.propsFromEnv = env2Props(app.getAppcode(),true);

        String home = app.propsFromEnv.getProperty("home");
        if (StringUtils.isBlank(home)) {
            System.out.println("cannot determine home for app " + appcode);
            throw new RuntimeException();
        }

        // 2. now check if env contains a specific prop file name
        String propFileAbsPath = findPropFile(app, klass);

        if (!propFileAbsPath.isEmpty()) {
            app.setPropFileAbsPath(propFileAbsPath);
            Properties fileprops = loadFromFile(propFileAbsPath);
            Properties appprops = filterPrefix(app.getAppcode(), fileprops, true);
            app.propsFromFile = appprops;
        }


        // TODO  now we need to get db informatmation , init db and read settings


        // finally combine all props in order of precedence
        app.props.putAll(app.propsFromDb);
        app.props.putAll(app.propsFromFile);
        app.props.putAll(app.propsFromEnv);

        return app;
    }

    // determine propfile for this app
    // first look for env vars else look at classpath
    public static String findPropFile(App app, Class<?> klass) {

        var fileFromEnv = propFileFromEnv(app);
        if (fileFromEnv != null) return fileFromEnv.getAbsolutePath();

        // no file in env , so look in classpath
        // this is useful in dev
        if (klass != null && app.getPropFileShortname() != null) {
            return getFilePathFromClasspath(klass, app.getPropFileShortname());
        }
        return null;
    }

    // get then name of a prop file if it has been set using the env
    // else we just use the filename stored in app.
    // we can use -Dappcode.props to set the filename
    public static File propFileFromEnv(App app) {
        var env = env2Props(app.getAppcode(),false);
        String fname = env.getProperty(MYAppConstants.EnvVars.PropFile);
        if (fname == null) {
            // no env var so look for standard name
            fname = app.getPropFileShortname();
        }
        if (fname != null) {

            String home = env.getProperty(MYAppConstants.EnvVars.Home);
            File f = OSHelper.getFile(fname, home);
            return f;
        }
        return null;
    }

    // look for all env vars starting with 'appcode' and covert to properties
    // converts env var appcodepropname to a property appcode.propname
    public static Properties env2Props(String appCode,boolean removePrefix) {
        String prefix = appCode.trim().toLowerCase();
        Properties p = new Properties();

        // get system env and java props
        Map<String, String> sysenv = OSHelper.getAllEnv();
        Map<String, String> sysprop = OSHelper.getAllProperties();
        //sysprop.forEach((k,v)->System.out.println(" SYSPROP prop:  "+k+ " = "+v));
        //sysenv.forEach((k,v)->System.out.println(" SYSENV prop:  "+k+ " = "+v));

        // now combined the two , with env override props
        HashMap<String, String> combinedEnv = new HashMap<>(sysprop);
        combinedEnv.putAll(sysenv);
        // filter map to get all entries starting with env
        int len = prefix.length();
        Map<String, String> appenv = combinedEnv.entrySet().stream().filter(m -> m.getKey().toLowerCase().startsWith(prefix))

                .collect(Collectors.toMap(m -> parsePropName(m.getKey(), prefix), m -> m.getValue()));
        p.putAll(appenv);
        if(removePrefix)
            p= filterPrefix(appCode,p,true);
        return p;
    }

    // convert a key to a a.b notation. so Myapp_home or myapplogpath will convert to myapp.home and myapp.logpath
    // response is always lowercase
    public static String parsePropName(String propname, String appcode) {
        propname = propname.substring(appcode.length());
        // remove _ is it exists
        if (propname.charAt(0) == '_') propname = propname.substring(1);
        if (propname.charAt(0) == '.') propname = propname.substring(1);
        return appcode + "." + formatPropName(propname);
    }

    // looks for a prop file in the classpath of the given class's loader
    public static Properties getFromClasspathFile(Class<?> klass, String filename) {
        Properties prop = new Properties();
        InputStream inputStream;
        try {
            inputStream = klass.getClassLoader().getResourceAsStream(filename);
            if (inputStream != null) {
                prop.load(inputStream);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return prop;
    }

    // load props from file ,
    public static Properties loadFromFile(String filename) {
        Properties props = new Properties();
        File propFile = new File(filename);
        if (propFile.exists()) {
            try {
                if (useCommonsConfig) {
                    props = loadUsingCommonsConfig(filename);
                } else {
                    props.load(new FileInputStream(propFile));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (formatNames) {
                if (props != null) {
                    Properties formattedProps = new Properties();
                    // place formatted props in new properties
                    props.entrySet().stream().forEach(e -> formattedProps.put(formatPropName(e.getKey().toString()), e.getValue()));
                    props = formattedProps;
                }
            }
        }
        return props;
    }

    // loads properties from a file using commons config
    // and convert to properties
    public static Properties loadUsingCommonsConfig(String filename) {
        Properties props = new Properties();
        Configuration config = MyProperties.getConfig(filename);
        config.getKeys().forEachRemaining(c -> props.put(c, config.getString(c)));
        return props;
    }

    // loads a property file using commons config
    private static Configuration getConfig(String filename) {
        Configuration config = null;
        Configurations configs = new Configurations();
        try {
            config = configs.properties(new File(filename));
        } catch (ConfigurationException e) {
            e.printStackTrace();
        }
        return config;
    }

    public static String getFilePathFromClasspath(Class<?> klass, String filename) {
        String absPath = "";
        try {
            var url = klass.getClassLoader().getResource(filename);
            if (url != null) {
                File f = new File(url.toURI());
                absPath = f.getAbsolutePath();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return absPath;
    }

    // formats the propname to be lowercase and without certain chars
    public static String formatPropName(String propname) {
        if (propname == null || propname.isEmpty()) return null;
        String p = propname.toLowerCase().trim();
// remove '-' since we cannot have that in a fieldname

        p = p.replace("-", "");
        return p;
    }


    // convert props to map
    public static Map<String, String> toMap(Properties properties) {
        Map<String, String> map = new HashMap<>();
        for (final String name : properties.stringPropertyNames())
            map.put(name, properties.getProperty(name));
        return map;
    }

    // returns a subset of properties starting with a prefix( and a .) . optionally strip the prefix from the name
    public static Properties filterPrefix(String p, Properties props, boolean strip) {
        Properties newprops = new Properties();
        if (p == null | p.isEmpty()) return newprops;
        p = p.toLowerCase().trim();
        if (!p.endsWith(".")) p += ".";
        int len = p.length();
        final String prefix = p;
        for (Map.Entry<Object, Object> entry : props.entrySet()) {
            if (entry.getKey().toString().toLowerCase().startsWith(prefix.toLowerCase())) {
                var key = strip ? entry.getKey().toString().substring(len) : entry.getKey().toString();
                newprops.put(key, entry.getValue());
            }
        }
        return newprops;
    }


    // populates a static class with values from a hierarchical set of properties.
    // property names are expected to be lowercase dotted notation
    public static void populateStatic(Class<?> klass, Properties props, String prefix) throws IllegalAccessException {
        Field[] flist = klass.getFields();
        for (Field f : klass.getFields()) {
            setField(props, f, prefix);
        }
        for (Class<?> nextKlass : klass.getClasses()) {
            String cname = nextKlass.getSimpleName();
            String newprefix = (prefix == null || prefix.isEmpty()) ? cname : prefix + "." + cname;
            populateStatic(nextKlass, props, newprefix);
        }
    }

    // set a field to the right type of value , eat exceptions
    private static Field setField(Properties props, Field f, String prefix) {
        String propname = f.getName();
        if (prefix != null && !prefix.isEmpty()) propname = prefix + "." + propname;
        propname = formatPropName(propname);
        Object val = props.get(propname);
        Class<?> _t = f.getType();

        try {
            if (val != null) {

                if (_t == val.getClass()) {
                    f.set(null, val);
                    return f;
                }
                if (f.getType() == String.class) {
                    f.set(null, val.toString());
                }
                if (isIntegerType(_t)) {
                    if (isNumeric(val.toString())) {
                        f.set(null, Integer.parseInt(val.toString()));
                    }
                    return f;
                }
                if (isNummericType(_t)) {
                    if (isNumeric(val.toString())) {
                        f.set(null, Double.parseDouble(val.toString()));
                    }
                }
                if (isBooleanType(_t)) {
                    if (isBooleanType(val.getClass()))
                        f.set(null, val);
                    if (isBooleanString(val.toString())) {
                        f.set(null, toBoolean(val.toString()));
                    }
                }

            }
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        //f.set(null, val);
        return f;
    }

    //return true of string is true/false/y/n
    private static boolean isBooleanString(String bool) {
        if (bool == null) return false;
        String s = bool.trim().toLowerCase();
        return "true".equals(s) || "false".equals(s) || "y".equals(s) || "yes".equals(s) || "no".equals(s) || "n".equals(s);

    }

    // return true of string matchs true or Y
    private static boolean toBoolean(String toString) {
        if (toString == null) return false;
        String s = toString.trim().toLowerCase();
        return "true".equals(s) || "y".equals(s) || "yes".equals(s);
    }

    public static boolean isNummericType(Class<?> klass) {
        return klass == int.class || klass == long.class || klass == double.class || klass.isAssignableFrom(Number.class);
    }

    public static boolean isIntegerType(Class<?> klass) {
        return klass == int.class || klass == long.class || klass.isAssignableFrom(Integer.class);
    }

    public static boolean isBooleanType(Class<?> _t) {
        return _t == Boolean.class || _t == boolean.class;
    }


    public static boolean isNumeric(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }


    // get props from multiple files , files are relative to config dir
    // returns a map of filename and properties

    public static Map<String, Properties> getProperties(String configDir, boolean ignoreCase, Logger log, String... propFilesShortNames) {
        String[] fullFileNames = new String[propFilesShortNames.length];
        for (int i = 0; i < propFilesShortNames.length; i++) {
            fullFileNames[i] = OSHelper.join(configDir, propFilesShortNames[i]);
        }
        return getProperties(ignoreCase, log, fullFileNames);
    }

    // read properties from multiple property files
    // returns a map of filename and properties
    // files are absolute paths
    // using a lambda to log messages to avoind repeated null checks
    public static Map<String, Properties> getProperties(boolean ignoreCase, Logger log, String... propFiles) {
        Map<String, Properties> result = new HashMap<>();
        Consumer<String> _debug = msg -> {
            if (log != null) log.debug("getProperties: " + msg);
        };
        if (propFiles == null) {
            _debug.accept("No property file names provided , returning empty properties");
            return result;
        }
        for (String f : propFiles) {
            Properties _props = new Properties();
            if (f == null) break;
            Properties p = getProperties(f);
            _debug.accept("Processing file :" + p + " count: " + p.size());
            if (p.size() > 0) {
                p.forEach((k, v) -> {
                    String key = ignoreCase ? k.toString().toLowerCase() : k.toString();
                    // warn if duplicate so that we know to clean up the prop file
                    if (_props.contains(key)) _debug.accept("Replacing property named :" + key);
                    _props.put(key, v);
                    _debug.accept("Adding property key :" + key + " value: " + v);
                });
            }
            result.put(f, _props);
        }
        return result;
    }

    // get property from file
    // filename must be absolute or relative to current dir
    public static Properties getProperties(String propFileName) {
        FileInputStream inputStream = null;
        Properties props = new Properties();
        if (propFileName == null || propFileName.isEmpty())
            return props;
        try {
            inputStream = new FileInputStream(propFileName);
            if (inputStream != null) {
                props.load(inputStream);
                inputStream.close();
            }
        } catch (FileNotFoundException e) {
            // we are using sysout since we dont know if logger is initialized
            System.out.println("PropertyHelper:Property File not found: " + propFileName);
        } catch (IOException e) {
            System.out.println("PropertyHelper:Exception reading Property File not found: " + propFileName);
        }
        if (props == null) props = new Properties();
//        if (ignoreCase) {
//            Properties formattedProps = new Properties();
//            // place formatted props in new properties
//            props.entrySet().stream().forEach(e -> formattedProps.put(formatPropName(e.getKey().toString()), e.getValue()));
//            props = formattedProps;
//        }
        return props;
    }


    // combine property collections
    public static Properties combineProps(Collection<Properties> propsList) {
        Properties result = new Properties();

        for (Properties p : propsList) {
            result.putAll(p);
        }
        return result;
    }

}
