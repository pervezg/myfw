package my.fw.base.date;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class DateHelper {
	// format for mysql datetime
	public static java.text.SimpleDateFormat dbDateTimeFormat = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	public DateHelper() {
	}

// return difference between 2 string dates. 
	// we return an Integer since it is nullable
	// null indicates some error
	public static Integer differenceInDays(String fromDate, String toDate) {
		Integer days = null;
		if (null == fromDate || fromDate.isEmpty() || null == toDate || toDate.isEmpty()) {
			return days;
		}
		try {
			SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date date1 = myFormat.parse(fromDate);
			Date date2 = myFormat.parse(toDate);
			long diff = date2.getTime() - date1.getTime();
			days = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
		}

		catch (ParseException e) {
		}
		return days;
	}

	public static String DBnow() {
		String currentTime = dbDateTimeFormat.format(now());
		return currentTime;
	}

	// this is a wrapper on date . its is only useful if we add offset ( hours,mins,secs) so that we have the ability to move of app clock ahead and back
	public static Date now() {
		return new Date();
	}
}
