package my.fw.base.utils;

import java.util.Map;
import java.util.TreeMap;

// home for utility method that dont fall into other helpers 
public class ObjectHelper {

	// sort map by keys
	public static Map<String, String> sort(Map<String, String> m) {

		Map<String, String> sortedMap = new TreeMap<String, String>(m);
		return sortedMap;

	}

}
