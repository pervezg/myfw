package my.fw.base.date;

import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Date;


// opdate is useful to be able to move the applications datetime back and forward by adding day and minute offsets
public class MYDate {


    private static String jsonDTformat = "yyyy-MM-dd'T'HH:mm:ss";
    public static String mysqlDTformat = "yyyy-MM-dd HH:mm:ss";

    // for use with localdatetime
    public static final DateTimeFormatter dbFormatter = DateTimeFormatter.ofPattern(mysqlDTformat);
    public static final DateTimeFormatter jsonFormatter = DateTimeFormatter.ofPattern(jsonDTformat);

    public static final SimpleDateFormat mysqlSimpleFormatter = new SimpleDateFormat(mysqlDTformat);

    private static int OffsetDays = 0;
    private static int OffsetMins = 0;

    public static LocalDateTime NULL = LocalDateTime.of(2000, 1, 1, 0, 0, 0);
    public static LocalDateTime MAX = LocalDateTime.of(2037, 1, 1, 0, 0, 0);
    private static ZoneId currentZoneid = ZoneId.systemDefault();


    public static LocalDateTime _now() {
        LocalDateTime _now = LocalDateTime.now().plusDays(OffsetDays).plusMinutes(OffsetMins);
        return _now;
    }

    // return adjusted datetime
    public static Date now() {
        return toDate(_now());
    }

    // conversion from localdatetime to date
    private static Date toDate(LocalDateTime n) {
        return java.util.Date.from(n.atZone(currentZoneid).toInstant());
    }

    // return adjusted date with hms = 0.0.0
    public static Date today() {
        return toDate( _now().with(LocalTime.MIDNIGHT));
    }

    public static int AddMins(int min) {
        OffsetMins += min;
        return OffsetMins;
    }

    public static int AddDays(int day) {
        OffsetDays += day;
        return OffsetDays;
    }

    public static int ResetDays() {
        OffsetDays = 0;

        return OffsetDays;
    }

    public static int ResetMins() {
        OffsetMins = 0;

        return OffsetMins;
    }

    // parse json datetime.
    // "yyyy-MM-dd'T'HH:mm:ss.SSSX"
    public static Date fromJsonDate(String jsonDate) {
        if (!jsonDate.isEmpty() && jsonDate.length() > 19) {
            LocalDateTime dt = LocalDateTime.parse(jsonDate.substring(0, 19),
                    DateTimeFormatter.ofPattern(jsonDTformat));
            return toDate(dt);
        }
        return toDate(NULL);
    }

    // convert to mysql date format string
    public static String toDbDate(Date dt) {
        if (dt == null)
            return null;
        return mysqlSimpleFormatter.format(dt);
    }


    public static String getCurrentDate() {

        java.util.Date dt = new java.util.Date();
        java.text.SimpleDateFormat sdf =
                new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        String currentTime = sdf.format(dt);
        return currentTime;

    }


    // convert to mysql date format string
    public static String toDbDate(LocalDateTime dt) {
        return dt.format(dbFormatter);
    }

    public static void Reset() {
        ResetDays();
        ResetMins();
    }

    public static Date MAXDate(){
        return toDate(MAX);
    }
}
