package my.fw.base.app;

public class MYAppConstants {

	public static class Application{
		public static final String propFile ="application.properties";
	}
	public static class EnvVars {
		public static final String Home ="home";
		public static final String PropFile ="props";
	}
	public static String[] propFileSuffixes = {"app.properties", "conn.properties", "secrets.properties"};

	public static class AppFolders{

		public static final String Config ="config";
		public static final String Logs ="logs";
	}

}
