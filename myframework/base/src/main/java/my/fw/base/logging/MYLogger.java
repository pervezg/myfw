package my.fw.base.logging;

// simple extension on sl4j to provide structured logging and  lose coupling via OPLoggerImpl 

public interface MYLogger extends org.slf4j.Logger {

    public void TRACE(String objName, String _method,String format, Object... arguments);
	
}
