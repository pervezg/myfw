package my.fw.base.app;

import javax.sql.DataSource;
import java.util.*;

// represnets a application
public class App {
    // name ( short ) of the app
    public String name;

    // descriptive title
    String title;

    // a very short code that is used to prefix all environment vars
    String appcode;

    // relative file name of prop file
    String propFileShortname;

    String propFileAbsPath;

    String version;

    // parent of this app - smaller apps like CLIs etc can inherit from  parent app
    App parent;

    // resolved properties of this app - this will be a comvination of env, prop file and database settings
    public Properties props;

    public String homeDir;
    public String configDir;
    public String logDir;

    public Properties propsFromEnv;
    public Properties propsFromFile;
    public Properties propsFromDb;

    // filename-property map
    public Map<String, Properties> propsMap;

     String[] propFileSuffixes = MYAppConstants.propFileSuffixes;

    private DataSource defaultds;


    public App(String name, String appcode) {
        this(name, appcode, appcode + ".properties");
    }

    public App(String name, String appcode, String propFileName) {
        this.name = name;
        this.appcode = appcode;
        this.propFileShortname = propFileName;
        props = new Properties();
        propsFromFile = new Properties();
        propsFromDb = new Properties();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAppcode() {
        return appcode;
    }

    public void setAppcode(String appcode) {
        this.appcode = appcode;
    }

    public App getParent() {
        return parent;
    }

    public void setParent(App parent) {
        this.parent = parent;
    }

    public Properties getProps() {
        return props;
    }

    public void setProps(Properties props) {
        this.props = props;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getPropFileShortname() {
        return propFileShortname;
    }

    public void setPropFileShortname(String propFileShortname) {
        this.propFileShortname = propFileShortname;
    }

    public String getPropFileAbsPath() {
        return propFileAbsPath;
    }

    public void setPropFileAbsPath(String propFileAbsPath) {
        this.propFileAbsPath = propFileAbsPath;
    }

    // return a list of files names that will contain properties
    public Collection<String> getPropFileShortNames() {
        if (propFileSuffixes == null || propFileSuffixes.length == 0) propFileSuffixes = new String[]{"app.properties"};
        String prefix = appcode.toLowerCase();
        List result = new ArrayList<String>();
        Arrays.stream(propFileSuffixes).forEach(s -> result.add(prefix + "." + s));
        return result;
    }
}
