package my.fw.base.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector;
import com.fasterxml.jackson.databind.module.SimpleModule;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Helper for JSON serialization and deserialization
 */
public class JSONHelper {

	/**
	 * Jackson Object Mapper used to serialization/deserialization
	 */
	protected static ObjectMapper objectMapper;
	static {
		initialize();
	}

	protected static void initialize() {
		objectMapper = new ObjectMapper();
		SimpleModule module = new SimpleModule();
		objectMapper.registerModule(module);
		AnnotationIntrospector introspector = new JacksonAnnotationIntrospector();
		objectMapper.setAnnotationIntrospector(introspector);
		objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
		objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		objectMapper.configure(MapperFeature.DEFAULT_VIEW_INCLUSION, false);
	}

	/**
	 * Return the ObjectMapper. It can be used to customize
	 * serialization/deserialization configuration.
	 *
	 * @return the current object mapper instance
	 */
	public ObjectMapper getObjectMapper() {
		if (objectMapper == null)
			initialize();
		return objectMapper;
	}

	/**
	 * @throws Exception Serialize and object to a JSON String representation
	 * 
	 * @param o The object to serialize @return The JSON String
	 *          representation @throws
	 */
	public static String serialize(Object o) throws Exception {
		if (objectMapper == null)
			initialize();
		OutputStream baOutputStream = new ByteArrayOutputStream();
		try {
			objectMapper.writeValue(baOutputStream, o);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;

		}
		return baOutputStream.toString();
	}

	/**
	 * Serialize and object to a JSON String representation with a Jackson view
	 * 
	 * @param o    The object to serialize
	 * @param view The Jackson view to use
	 * @return The JSON String representation
	 * @throws Exception
	 */
	public static String serialize(Object o, Class<?> view) throws Exception {
		if (objectMapper == null)
			initialize();
		OutputStream baOutputStream = new ByteArrayOutputStream();
		try {
			ObjectWriter writter = objectMapper.writerWithView(view);
			writter.writeValue(baOutputStream, o);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;

		}
		return baOutputStream.toString();
	}

	/**
	 * Deserialize a JSON string
	 * 
	 * @param content The JSON String object representation
	 * @param type    The type of the deserialized object instance
	 * @return The deserialized object instance
	 * @throws Exception
	 */
	public static <T> T deserialize(String content, Class<T> type) throws Exception {
		if (objectMapper == null)
			initialize();
		try {
			return objectMapper.readValue(content, type);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public static <T> T fromJSONString(String content, Class<T> type) throws Exception {
		return deserialize(content, type);
	}

	public static Map<String, String> fromJSONStringToMap(String content) throws Exception {
		if (objectMapper == null)
			initialize();
		TypeReference<HashMap<String, String>> typeRef = new TypeReference<HashMap<String, String>>() {
		};
		Map<String, String> map = objectMapper.readValue(content, typeRef);
		return map;
	}

	public static String getField(String json, String fieldName) {
		JsonNode parent;
		if (objectMapper == null)
			initialize();
		try {
			parent = objectMapper.readTree(json);
			String content = parent.path(fieldName).asText();
			return content;

		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String toJSONString(Object o) {
		if (objectMapper == null)
			initialize();
		String result = null;
		if (o != null) {
			try {
				result = objectMapper.writeValueAsString(o);
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}
		}
		return result;

	}
}
