package my.fw.base.utils;

import java.util.HashMap;
import java.util.Map;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NameClassPair;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;

import org.slf4j.Logger;


public class JNDIHelper {

	public static Map logContext(Logger log, Context ctx) throws NamingException {
		String namespace = ctx instanceof InitialContext ? ctx.getNameInNamespace() : "";
		HashMap<String, Object> map = new HashMap<String, Object>();
		log.info("> Listing namespace: " + namespace);
		NamingEnumeration<NameClassPair> list = ctx.list(namespace);
		while (list.hasMoreElements()) {
			NameClassPair next = list.next();
			String name = next.getName();
			String jndiPath = namespace + name;
			Object lookup;
			try {
				log.info("> Looking up name: " + jndiPath);
				Object tmp = ctx.lookup(jndiPath);
				if (tmp instanceof Context) {
					lookup = logContext(log, (Context) tmp);
				} else {
					lookup = tmp.toString();
					log.info("> : " + lookup);
					System.err.println("JNDI: " + jndiPath + " : " + lookup);
				}
			} catch (Throwable t) {
				lookup = t.getMessage();
			}
			map.put(name, lookup);

		}
		return map;
	}
}
