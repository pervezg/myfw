package my.fw.base.utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import my.fw.base.logging.MYLogFactory;
import org.slf4j.Logger;

public class PropertyReader {
	static Logger log = MYLogFactory.getLogger(PropertyReader.class.getName());

	public static String getConfigString(String url) {
		String finalURL = null;
		try {
			Properties prop = new Properties();
			InputStream inputStream = PropertyReader.class.getClassLoader().getResourceAsStream("/config.properties");
			prop.load(inputStream);
			finalURL = prop.getProperty(url);
			log.info("getConfigString value : " + finalURL);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return finalURL;
	}

}
