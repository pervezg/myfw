package my.fw.base;

import my.fw.base.app.App;
import my.fw.base.app.MYAppConstants;
import my.fw.base.constants.MYConstants;
import my.fw.base.system.MyProperties;
import my.fw.base.system.OSHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Map;
import java.util.Properties;
@Deprecated
// see my.fw.app.AppFactory
public class MyAppFactory {

    static Logger log = LoggerFactory.getLogger(MyAppFactory.class);

    // initialize app environment based on app information
    public static App init(App app) {

        app.homeDir = getHomeDir(app);
        app.configDir = getConfigDir(app);
        initProperties(app);
        return app;
    }

    public static App initProperties( App app) {

        String[] fnames = app.getPropFileShortNames().toArray(new String[0]);
        Map<String, Properties> propmap = MyProperties.getProperties(app.configDir, true, log, fnames);
        app.propsMap = propmap;
        app.props = MyProperties.combineProps(propmap.values());
        return app;
    }

    // get home directory from environment var
    public static String getHomeDir(App app) {
        String homevarName = app.getAppcode() + MYAppConstants.EnvVars.Home;
        String homevarValue = OSHelper.getEnvVar(homevarName);
        if (homevarValue != null && homevarName != "")
            return OSHelper.getFullPath(homevarValue);
        return null;
    }

    // get config dir relative to home dir
    private static String getConfigDir(App app) {
        return OSHelper.join(app.homeDir, MYAppConstants.AppFolders.Config);
    }

}
