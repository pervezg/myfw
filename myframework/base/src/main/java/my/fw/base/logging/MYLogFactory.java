package my.fw.base.logging;

import java.io.File;
import java.util.Collection;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.core.joran.spi.JoranException;
import ch.qos.logback.core.util.StatusPrinter;
import my.fw.base.system.OSHelper;

//import org.slf4j.impl.StaticLoggerBinder;
public class MYLogFactory {
	// final static StaticLoggerBinder binder = StaticLoggerBinder.getSingleton();

	private static org.slf4j.Logger rootLogger;
	private static String logDir = "./";
	private static String appName = "myfw";
//https://reflectoring.io/logging-format-logback/
	// https://blog.overops.com/how-to-instantly-improve-your-java-logging-with-7-logback-tweaks/
//	private static	String pattern = "_log_%d{yyyy_MM_dd_HH}.txt";
//	private static String fileNamePattern = "_log_%d{yyyy_MM_dd}.log";
	private static String messagePattern = "%date %level [%thread] %logger{10} [%file:%line] %msg%n";

	public static String archiveDir = "archive";
	public static String logLevel = "INFO";
	public static boolean enableConsole = true;
	private static String fileNamePattern = ".log";
	private static String archivefileNameSuffixPattern = "_log_%d{yyyy_MM_dd}.log";


	// gets a logger , assume that root logger is already configured by init
	public static Logger getLogger(String lname) {
		return getSlf4jLogger(lname);
	}

	// we need to create a root logger with the correct config so that other logger
	// can inherit it.
	public static void init(String dir, String appname, String level) {
		if (dir != null && !dir.isEmpty())
			logDir = dir;
		logDir = OSHelper.getFullPath(logDir);
		if (appname != null && !appname.isEmpty())
			appName = appname;
		initRootLog(logDir, appName, level, enableConsole);
		showLoggerInfo(rootLogger);
	}


	// int logger using properties in log file
	// we provide log dir in case it is missing from props
	public static void init(String appname, Properties props, String _logdir) {
		applyProps(props, _logdir);
		init(logDir, appname, logLevel);
	}

	// updates setting based on props
	// we take logdir as an additional params since this is a critical param
	public static void applyProps(Properties props, String _logdir) {
		if (StringUtils.isNotBlank(_logdir))
			logDir = _logdir;
		logDir = props.getProperty(PropNames.dir, logDir);
		logLevel = props.getProperty(PropNames.level, logLevel);
		archiveDir = props.getProperty(PropNames.archiveDir, archiveDir);
		if (StringUtils.isNotBlank(props.getProperty(PropNames.console)))
			enableConsole = Boolean.valueOf(props.getProperty(PropNames.console));
	}

	// create a root logger , appname is inserted into the file template so that we
	// can have a per app log
	private static void initRootLog(String dir, String appnamePrefix, String level, boolean enableConsole) {
		String pattern = appnamePrefix + fileNamePattern;
		String arcpattern = archiveDir + '/' + appnamePrefix + archivefileNameSuffixPattern;
		// 2021_sept - new patter for dir based archive model
		arcpattern = archiveDir + getDateviseArchiveDir(appnamePrefix);
		rootLogger = LogbackHelper.newLogger(org.slf4j.Logger.ROOT_LOGGER_NAME, dir, pattern, arcpattern, level,
				appnamePrefix, enableConsole);
		rootLogger.info("Log directory : {}", dir);
	}

	// reutrn slf4jLogger
	public static Logger getSlf4jLogger(String lname) {
		Logger _logger = LoggerFactory.getLogger(lname);
		//	showLoggerInfo(_logger);
		return _logger;
	}

	public static void setLoggingLevel(ch.qos.logback.classic.Level level) {
		ch.qos.logback.classic.Logger root = (ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory
				.getLogger(ch.qos.logback.classic.Logger.ROOT_LOGGER_NAME);
		root.setLevel(level);
	}

	public static void setLoggingLevelDebug(ch.qos.logback.classic.Logger l) {
		l.setLevel(ch.qos.logback.classic.Level.DEBUG);
	}

	// find the real class of the slf4j logger and dump info
	public static void showLoggerInfo(org.slf4j.Logger l) {
		String _c = l.getClass().getName();
		// String lvl = null;
		if (_c == "ch.qos.logback.classic.Logger") {
			ch.qos.logback.classic.Logger _logback = (ch.qos.logback.classic.Logger) l;
			LogbackHelper.showLogbackInfo(_logback);
		}
		l.info("Logger : " + _c);
	}

	public static void initConfig(String configFileName) {
		if (configFileName == null || configFileName.isEmpty())
			return;
		File f = OSHelper.getFile(configFileName, null);
		if (f == null)
			return;
		LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();

		try {
			JoranConfigurator configurator = new JoranConfigurator();
			configurator.setContext(context);
			// Call context.reset() to clear any previous configuration, e.g. default
			// configuration. For multi-step configuration, omit calling context.reset().
			context.reset();
			configurator.doConfigure(f);
		} catch (JoranException je) {
			// StatusPrinter will handle this
			je.printStackTrace();
			//log.error("Getting exception : init Configuration ", je.getMessage());
		}
		StatusPrinter.printInCaseOfErrorsOrWarnings(context);
	}

	public static Logger getLogger(Class<?> klass) {
		return getLogger(klass.getSimpleName());
	}

	// creates a new logger with a separate rolling file
	public static ch.qos.logback.classic.Logger getFileLogger(String logName, String level) {

		return getFileLogger(logName, logDir, level);
	}

	// creates a new logger with a separate rolling file
	public static ch.qos.logback.classic.Logger getFileLogger(String logName, String ldir, String level) {
		String pattern = logName + fileNamePattern;
		String arcpattern = archiveDir + '/' + logName + archivefileNameSuffixPattern;
		// 2021_sept - new patter for dir based archive model
		arcpattern = archiveDir + getDateviseArchiveDir(logName);

		ch.qos.logback.classic.Logger newlogger = LogbackHelper.newFileLogger(logName, ldir, pattern, arcpattern, level,
				logName);
		newlogger.info("Log directory : {}", ldir);
		return newlogger;
	}

	// creates a new logger from root context and then ADD an appender
	public static ch.qos.logback.classic.Logger addRollingFile(Logger _log, String ldir, String level) {

		String logName = _log.getName();
		String pattern = logName + fileNamePattern;
		String arcpattern = archiveDir + '/' + logName + archivefileNameSuffixPattern;
		ch.qos.logback.classic.Logger newlogger = LogbackHelper.addRollingFileLogger((ch.qos.logback.classic.Logger) _log, ldir, pattern, arcpattern, level,
				logName);
		newlogger.info("Log directory : {}", ldir);
		return newlogger;
	}

	// creates a new logger with a separate rolling file
	// will change log level if it is not null
	public static Logger getLogerWithOwnFile(Class<?> klass, String ldir, String level) {
		String logName = klass.getSimpleName();
		Logger _log = LoggerFactory.getLogger(klass);

		String pattern = logName + fileNamePattern;
		String arcpattern = archiveDir + '/' + logName + archivefileNameSuffixPattern;
		LogbackHelper.addRollingFileLogger((ch.qos.logback.classic.Logger) _log, ldir, pattern, arcpattern, level,
				logName);
		return _log;
	}

	// returns pattern for hierarchal archive structure
	static String getDateviseArchiveDir(String appname) {
		String archivedirNamePattern = "/%d{yyyy/MM/dd,aux}";
		String filepattern = archivedirNamePattern + "/" + appname + archivefileNameSuffixPattern;
		return filepattern;
	}

	// sets log leve to INFO if the log is present
	public static void disableLog(String logname) {
		LogbackHelper.setLogLevel(logname, "ERROR");
	}

	// set the log level for multiple logs
	// properties have key=logname and value=level
	public static void setLevel(Properties p) {
		if (p == null) return;
		p.forEach((k, v) -> {
			String logname = k.toString();
			String level = p.getProperty(logname);
			setLevel(logname, level);
		});
	}

	public static void setLevel(String logname, String level) {
		LogbackHelper.setLogLevel(logname, level);
	}

	public static void disableLog(Collection<String> lognames) {
		if (lognames == null) return;
		for (String l : lognames) {
			disableLog(l);
		}
	}

	public static class PropNames {
		public static final String prefix = "log";
		public static final String level = "level";
		public static final String dir = "dir";
		public static final String archiveDir = "archivedir";
		public static final String console = "console";
		public static final String configfile = "config";
		public static final String consolePattern = "consolepattern";
		public static final String logPattern = "pattern";
		public static final String loglevelprefix = "loglevel";
	}
}
