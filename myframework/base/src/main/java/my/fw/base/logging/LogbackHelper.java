package my.fw.base.logging;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.filter.ThresholdFilter;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.Appender;
import ch.qos.logback.core.ConsoleAppender;
import ch.qos.logback.core.FileAppender;
import ch.qos.logback.core.rolling.RollingFileAppender;
import ch.qos.logback.core.rolling.TimeBasedRollingPolicy;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.LoggerFactory;

import java.util.*;

public class LogbackHelper {

	public static final String defaultPattern = "%date %level [%thread] %logger{10} [%file:%line] %msg%n";
	public static final String archiveDir = "archive";
	// create a file based appender
	// https://stackoverflow.com/questions/16910955/programmatically-configure-logback-appender
	// https://stackoverflow.com/questions/38208617/practical-use-of-logback-context-selectors
//

	public static FileAppender<ILoggingEvent> createFileAppender(LoggerContext logCtx, String dir,
																 String fileNamePattern, String layoutPattern, String filterLevel) {
		fileNamePattern = dir + '/' + fileNamePattern;

		PatternLayoutEncoder logEncoder = createPLE(logCtx, layoutPattern);

		FileAppender<ILoggingEvent> _appender = new FileAppender<ILoggingEvent>();
		_appender.setContext(logCtx);
		_appender.setName("FILE");
		_appender.setFile(fileNamePattern);
		_appender.setEncoder(logEncoder);
		_appender.setAppend(true);
		if (filterLevel != null) {
			ThresholdFilter errorFilter = new ThresholdFilter();
			errorFilter.setLevel(filterLevel);
			errorFilter.start();
			if (errorFilter != null)
				_appender.addFilter(errorFilter);
		}
		_appender.start();
		return _appender;
	}

	public static RollingFileAppender<ILoggingEvent> rollingFileLogger(LoggerContext logCtx, String dir,
																	   String fileNamePattern, String arcFile, String layoutPattern) {
		String archiveFilePattern = dir + '/' + arcFile;
		fileNamePattern = dir + '/' + fileNamePattern;
		PatternLayoutEncoder logEncoder = createPLE(logCtx, layoutPattern);
		RollingFileAppender<ILoggingEvent> _appender = new RollingFileAppender<ILoggingEvent>();
		_appender.setContext(logCtx);
		_appender.setName("ROLLINGFILE");
		_appender.setEncoder(logEncoder);
		_appender.setAppend(true);
		_appender.setFile(fileNamePattern);

		TimeBasedRollingPolicy logFilePolicy = new TimeBasedRollingPolicy();
		logFilePolicy.setContext(logCtx);
		logFilePolicy.setParent(_appender);
		logFilePolicy.setFileNamePattern(archiveFilePattern);
		// logFilePolicy.setMaxHistory(7);
		logFilePolicy.start();

//		SizeAndTimeBasedRollingPolicy p = new SizeAndTimeBasedRollingPolicy();
//		p.setContext(logCtx);
//		p.setParent(logFileAppender);
//		p.setFileNamePattern("d:\\logs\\logfile-%d.%i.log");
//		p.start();

		_appender.setRollingPolicy(logFilePolicy);
//		logFileAppender.setRollingPolicy(p);
		_appender.start();

		return _appender;
	}

	public static ConsoleAppender<ILoggingEvent> createConsoleAppender(LoggerContext logCtx, String layoutPattern) {
		PatternLayoutEncoder ple = createPLE(logCtx, layoutPattern);
		ConsoleAppender<ILoggingEvent> _appender = new ConsoleAppender<ILoggingEvent>();
		_appender.setContext(logCtx);
		_appender.setName("CONSOLE");
		_appender.setEncoder(ple);
		_appender.start();
		return _appender;
	}

	// create and start and encoder
	public static PatternLayoutEncoder createPLE(LoggerContext lc, String pattern) {
		pattern = (pattern != null && !pattern.isEmpty()) ? pattern : defaultPattern;
		PatternLayoutEncoder ple = new PatternLayoutEncoder();
		ple.setPattern(pattern);
		ple.setContext(lc);
		ple.start();
		return ple;
	}

	// creates a new logger with console + file logger
	public static Logger newLogger(String logname, String dir, String fileNamePattern, String archiveFileNamePattern, String level,
								   String appnamePrefix, boolean enableConsole) {
		Level lvl = Level.INFO;
		lvl = Level.toLevel(level, lvl);
		LoggerContext logCtx = (LoggerContext) LoggerFactory.getILoggerFactory();
		List<Logger> ll = logCtx.getLoggerList();
		// next line will pick up any logback config in respurces files and process them
		String layoutPattern = (appnamePrefix == null || appnamePrefix.isEmpty()) ? defaultPattern
				: appnamePrefix + " " + defaultPattern;
		Logger log = logCtx.getLogger(logname);
		log.setAdditive(false);
		log.setLevel(lvl);
		log.addAppender(rollingFileLogger(logCtx, dir, fileNamePattern, archiveFileNamePattern, layoutPattern));
		log.addAppender(createFileAppender(logCtx, dir, appnamePrefix + "_ERROR.log", layoutPattern, "ERROR"));
		if (enableConsole && !hasConsole(log)) log.addAppender(createConsoleAppender(logCtx, layoutPattern));
		return log;

	}

	// creates a new logger with just a rolling file
	public static Logger newFileLogger(String logname, String dir, String fileNamePattern, String archiveFileNamePattern, String level,
									   String appnamePrefix) {
		Level lvl = Level.INFO;
		lvl = Level.toLevel(level, lvl);
		LoggerContext logCtx = (LoggerContext) LoggerFactory.getILoggerFactory();

		String layoutPattern = (appnamePrefix == null || appnamePrefix.isEmpty()) ? defaultPattern
				: appnamePrefix + " " + defaultPattern;
		Logger log = logCtx.getLogger(logname);
		log.setAdditive(false);
		log.setLevel(lvl);
		log.addAppender(rollingFileLogger(logCtx, dir, fileNamePattern, archiveFileNamePattern, layoutPattern));
		return log;

	}

	// add an extra rolling file to logger
	// will also change log level if level is non-null
	public static Logger addRollingFileLogger(Logger _log, String dir, String fileNamePattern, String archiveFileNamePattern, String level,
											  String appnamePrefix) {
		Level lvl = Level.INFO;
		lvl = Level.toLevel(level, lvl);
		LoggerContext logCtx = _log.getLoggerContext();
		String layoutPattern = (appnamePrefix == null || appnamePrefix.isEmpty()) ? defaultPattern
				: appnamePrefix + " " + defaultPattern;
		//_log.setAdditive(false);
		if (level != null) _log.setLevel(Level.toLevel(level));
		_log.addAppender(rollingFileLogger(logCtx, dir, fileNamePattern, archiveFileNamePattern, layoutPattern));
		return _log;

	}

	public static void showLogbackInfo(ch.qos.logback.classic.Logger _logback) {
		ch.qos.logback.classic.Level level = _logback.getEffectiveLevel();
		String lvl = level == null ? "NULL" : level.toString();
		LoggerContext logCtx = (LoggerContext) LoggerFactory.getILoggerFactory();
		List<Logger> ll = logCtx.getLoggerList();
		_logback.info("Logger Name :{} Level:{}", _logback.getName(), lvl);
		int i = 1;
		for (Iterator<Appender<ILoggingEvent>> index = _logback.iteratorForAppenders(); index.hasNext(); ) {
			Appender<ILoggingEvent> appender = index.next();
			String _method = "Appender : " + i + ":" + appender.getName() + " : ";
			if ("ch.qos.logback.core.rolling.RollingFileAppender" == appender.getClass().getName()) {
				RollingFileAppender _roller = (ch.qos.logback.core.rolling.RollingFileAppender) appender;

				_logback.info(_method + "Rolling File : File name {}", i, appender.getName(), _roller.getFile());
			} else if (("ch.qos.logback.core.FileAppender" == appender.getClass().getName())) {
				FileAppender _file = (ch.qos.logback.core.FileAppender) appender;
				_logback.info(_method + "File : File name {}", i, appender.getName(), _file.getFile());
			} else {
				_logback.info(_method + " Class: {}", i, appender.getName(), appender.getClass().getName());
			}
			i++;
		}
	}

	// collect info for context and all logs given any 1 logger.
	// retruns a sorted map
	public static Map<String, String> getAllLogbackInfo(Logger _logback) {
		Map<String, String> sortedMap = new TreeMap<>();
		Map<String, String> mainMap = new LinkedHashMap<>();

		LoggerContext logCtx = (LoggerContext) LoggerFactory.getILoggerFactory();
		List<Logger> ll = logCtx.getLoggerList();
		mainMap.put("totalLoggers", String.valueOf(ll.size()));
		Logger root = logCtx.getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME);
		mainMap.putAll(getLogbackInfo(root));
		for (int i = 0; i < ll.size(); i++) {
			sortedMap.putAll(getLogbackInfo(ll.get(i)));
		}
		mainMap.putAll(sortedMap);
		return mainMap;
	}

	// collect info and appenders for a single log.
	public static Map<String, String> getLogbackInfo(Logger _logback) {
		Map<String, String> _map = new HashMap<>();
		String _name = _logback.getName();
		ch.qos.logback.classic.Level level = _logback.getEffectiveLevel();
		String lvl = level == null ? "NULL" : level.toString();
		_map.put(_name + ".level", lvl);
		int i = 1;
		for (Iterator<Appender<ILoggingEvent>> index = _logback.iteratorForAppenders(); index.hasNext(); ) {
			Appender<ILoggingEvent> appender = index.next();
			String prefix = _name + ".Appender." + i + "." + appender.getName();
			_map.put(prefix + ".class", appender.getClass().toString());
			if ("ch.qos.logback.core.rolling.RollingFileAppender" == appender.getClass().getName()) {
				RollingFileAppender _roller = (ch.qos.logback.core.rolling.RollingFileAppender) appender;
				_map.put(prefix + ".file", _roller.getFile());
			} else if (("ch.qos.logback.core.FileAppender" == appender.getClass().getName())) {
				FileAppender _file = (ch.qos.logback.core.FileAppender) appender;
				_map.put(prefix + ".file", _file.getFile());
			}
			i++;
		}
		return _map;
	}


	// checks is log already has CONSOLE appender
	private static boolean hasConsole(ch.qos.logback.classic.Logger log) {
		for (Iterator<Appender<ILoggingEvent>> index = log.iteratorForAppenders(); index.hasNext(); ) {
			Appender<ILoggingEvent> appender = index.next();
			if (appender.getClass().getSimpleName().equals("ConsoleAppender")) return true;
		}
		return false;
	}

	// sets log leve to INFO if the log is present
	public static void setLogLevel(String logname, String level) {
		if (StringUtils.isBlank(logname)) return;
		LoggerContext logCtx = (LoggerContext) LoggerFactory.getILoggerFactory();
		ch.qos.logback.classic.Logger logger = logCtx.getLogger(logname);
		logger.setLevel(Level.valueOf(level));
	}

}
