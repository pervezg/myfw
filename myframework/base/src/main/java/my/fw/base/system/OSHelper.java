package my.fw.base.system;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.TreeMap;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import my.fw.base.logging.MYLogHelper;
import org.slf4j.Logger;

import my.fw.base.utils.ObjectHelper;

public class OSHelper {

	public OSHelper() {
	}

	// log env var
	public static void logEnv(Logger log) {

		log.info("Envionment Variables >>>>>>");
		Map<String, String> l = System.getenv();
		l = ObjectHelper.sort(l);
		MYLogHelper.logMap(log, "Env", l);
//		for (String e : l.keySet()) {
//			log.info("ENV:\t" + e + "\t\t:" + l.get(e));
//		}
//		log.info("Envionment Variables <<<<<<<<<<");
		log.info("Current Directory : " + CurrentDir());

	}

	// return env vars in sorted map
	public static Map<String, String> getAllEnv() {
		TreeMap<String,String> t= new TreeMap<>(System.getenv());
		return t;
	}
	public static Map<String, String> getAllProperties() {
		Map<String,String> t= new TreeMap(System.getProperties());
		return t;
	}

	// detect if running in container
	public static Boolean inJEEContainer() {
		try {
			new InitialContext().lookup("java:comp/env");
			return true;
		} catch (NamingException ex) {
			return false;
		}
	}

	// get env var - name is case insensitive
	public static String getEnvVar(String name) {
		if (name == null || name.isEmpty())
			return null;
		return System.getenv(name);
	}

	// get value of a java -D prop and if missing try for the same named envvar
	public static String getSysPropOrEnvVar(String name) {
		String val = null;
		val = getJavaProperty(name.toLowerCase());
		if (val == null || val.isEmpty())
			val = System.getenv(name);
		return val;
	}

	public static String getJavaProperty(String name) {
		String val = null;
		if (name == null || name.isEmpty())
			return null;
		val = System.getProperty(name);
		if (val == null) {
			// try lowercvase since java props are case sensitive
			val = System.getProperty(name.toLowerCase());

		}
		return val;
	}

	// read and return a boolean envvar flag
	public static Boolean getEnvBool(String name) {
		String s = System.getenv(name);
		if (s == null || s.isEmpty())
			return false;
		s = s.toLowerCase().trim();
		return (s.contentEquals("true") || s.equals("on") || s.equals("1"));
	}

	public static String CurrentDir() {
		return System.getProperty("user.dir");
	}

	// gets a file , look inside optional rootpath if the filename is relative
	public static File getFile(String filename, String rootPath) {
		// checkk if abs
		try {
			Path p = Paths.get(filename);

			if (!p.isAbsolute() && rootPath != null && !rootPath.isEmpty())
				filename =join( rootPath, filename);
			File f = new File(filename);
			if (f.exists() && !f.isDirectory()) {
				return f;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String getFullPath(String dir) {
		String result = null;
		File f = new File(dir);

		try {
			result = f.getCanonicalPath();
		} catch (IOException e) {
//				e.printStackTrace();
		}
		return result;
	}

	// joings dir and path and gets a full name
	public static String join(String dir, String file) {
		Path p = Paths.get(dir, file);
		Path f = p.getFileName();
		return getFullPath(p.toString());
	}

	// looks for file in classpath and converts to a full filename
	// returns null if not found
	public static String getFileFromClasspath(String propfilename) {
		   URL f = Thread.currentThread().getContextClassLoader().getResource(propfilename);
		   String p=null;
		if(f!=null)
			 p = f.getFile();
		return p;
	}

	// gets the path of file from the classpath of the class
	public static URI getFileFromClasspath(Class<?> klass, String filename) throws URISyntaxException {
		return klass.getClass().getResource(filename).toURI();
		// Files.readAllLines(				Paths.get(this.getClass().getResource("res.txt").toURI()), Charset.defaultCharset());
	}

	public static String createDirIfMissing(String dir) throws IOException {
		File f = new File(dir);
		if (f.exists()) {
			if (f.isDirectory()) return f.getAbsolutePath();
			else throw new IOException("the path " + dir + " exists but is not a directory");
		}
		f.mkdirs();
		return f.getAbsolutePath();
	}

	// saves a string to a file
	public static void saveToFile(String dir, String fileName, String s)
			throws IOException {
		dir = OSHelper.createDirIfMissing(dir);
		String f = OSHelper.join(dir, fileName);
		FileWriter fileWriter = new FileWriter(f);
		PrintWriter printWriter = new PrintWriter(fileWriter);
		printWriter.print(s);
		printWriter.close();
	}

}
