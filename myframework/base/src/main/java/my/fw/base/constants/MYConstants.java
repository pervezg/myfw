package my.fw.base.constants;

import org.springframework.beans.factory.annotation.Value;

public class MYConstants {

    public static class Status {
        public static final String Blocked = "BLOCKED";
        public static final String Active = "ACTIVE";
        public static final String Draft = "DRAFT";
        public static final String New = "NEW";
        public static final String Closed = "CLOSED";
        public static final String Disabled = "DISABLED";
        public static final String Pending = "PENDING";

    }

    public static class Result {
        public static final String Rejected = "REJECTED";
        public static final String Success = "SUCCESS";
        public static final String Failed = "FAILED";
        public static final String Retry = "RETRY";
    }
}
