package my.fw.repo;

import my.fw.model.IObjWithId;
import my.fw.model.Result;
import my.fw.service.ISimpleSvc;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;


public class MapRepo<T extends IObjWithId<String>> extends HashMap<String, T> implements ISimpleSvc<T, String> {
    public MapRepo() {
        super();
    }

    @Override
    public List<T> findAll() {
        return new ArrayList<T>(values());
    }

    @Override
    public T findOne(String key) {
        key = prepare(key);
        if (containsKey(key))
            return get(key);
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public boolean exists(String s) {
        return false;
    }

    public T save(T t) {
        return save(t, "UNKNOWN");
    }

    @Override
    public T save(T t, String createdBy) {
        if (t == null)
            return null;
        String id = t.getId();
        t.setId(id);
        put(id, t);
        return t;
    }

    @Override
    public int update(T t) {
        String id = t.getId();
        if (containsKey(id)) {
            put(id, t);
            return 1;
        }

        return 0;
    }

    @Override
    public int update(T t, boolean includeNulls) {
        return 0;
    }

    @Override
    public int update(T t, boolean includeNulls, String modifiedBy) {
        return 0;
    }

    public int update(T v, String modifiedBy) {
        return update(v);
    }

    @Override
    public int delete(String key) {
        key = prepare(key);
        if (containsKey(key)) {
            delete(key);
            return 1;
        }
        return 0;
    }

    @Override
    public String getID(T v) {
        return v.getId();
    }

    @Override
    public T setID(T v, String s) {
        v.setId(s);
        return v;
    }

    @Override
    public String toID(Object o) {
        return o.toString();
    }

    @Override
    public String prepare(String s) {
        if (s != null && s != "") return s.toLowerCase().trim();
        return null;
    }

    @Override
    public T setDefaults(T v) {
        return v;
    }

    @Override
    public T format(T t) {
        return null;
    }

    @Override
    public T expand(T v) {
        return v;
    }

    @Override
    public Result validate(T v) {
        return Result.Success();
    }

    public String getKey(T t) {
        if (t == null)
            return null;
        return prepare(t.getId());
    }

    // loads data into report from a collection
    public int load(Collection<T> _data) {
        if (_data == null) return 0;
        _data.forEach(t -> save(t));
        return _data.size();
    }

}
