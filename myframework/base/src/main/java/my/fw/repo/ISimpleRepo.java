package my.fw.repo;

import java.util.List;

// basic repo interface , indended for any repo that is not SQL based
// T = the entity manged by this repo
// ID - type of key - usually String or Integer
public interface ISimpleRepo<T, ID> {

    boolean exists(ID id);

    long count();

    ID getId(T t);

    T findOne(ID _id);

    Iterable<T> findAll();

    int delete(ID _id);

    T save(T t);

    int update(T t);

}
