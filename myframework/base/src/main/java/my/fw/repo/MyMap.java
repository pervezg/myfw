package my.fw.repo;

import java.util.HashMap;

import my.fw.model.IObjWithId;

public class MyMap<V extends IObjWithId<String>> extends HashMap<String, V> {

	public V findOne(String key) {
		if (containsKey(key))
			return get(key);
		return null;
	}

	public V save(V t) {
		if (t == null)
			return null;
		put(getKey(t), t);
		return t;
	}

	public String getKey(V t) {
		if (t == null)
			return null;
		return t.getId().toLowerCase().trim();
	}

}
