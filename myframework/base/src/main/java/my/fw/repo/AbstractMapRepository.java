package my.fw.repo;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

// the is base class implementing repo interface on a hashmap
// the getid method need to be implemented in the concrete subclass
public abstract class AbstractMapRepository<T, ID> implements ISimpleRepo<T, ID> {
    private Map<ID, T> _map = new HashMap<ID, T>();

    @Override
    public T findOne(ID id) {
        if (exists(id)) return _map.get(id);
        return null;
    }

    @Override
    public boolean exists(ID id) {
        return _map.containsKey(id);
    }

    @Override
    public Iterable<T> findAll() {
        return new ArrayList<>(_map.values());
    }

    @Override
    public long count() {
        return _map.size();
    }

    @Override
    public int delete(ID id) {
        if (exists(id)) {
            _map.remove(id);
            return 1;
        }
        return 0;
    }

    @Override
    public T save(T t) {
        return null;
    }

    @Override
    public int update(T t) {
        ID id = getId(t);
        if(_map.containsKey(id)) {
            _map.put(id,t);
            return 1;
        }
        return 0;
    }

    public void deleteAll() {
        _map.clear();
    }
}
