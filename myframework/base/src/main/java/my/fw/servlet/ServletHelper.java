package my.fw.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.TreeMap;
import java.util.stream.Collectors;

import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import org.slf4j.Logger;

import my.fw.api.model.BaseWebResponse;
import my.fw.base.utils.JSONHelper;
import my.fw.base.logging.MYLogFactory;
import my.fw.base.logging.MYLogHelper;
import my.fw.model.OPSession;

public class ServletHelper {

	public ServletHelper() {
	}

	// get a header from request without throwing exceptions
	public static String getHeader(HttpServletRequest request, String headerName) {

		String value = null;
		if (headerName != null && !headerName.isEmpty()) {
			value = request.getHeader(headerName);
		}
		return value;
	}

	// todo - use jackson
	// extracts body from request as a string buffer.
	// NOTE - this is a destructive call , the body will disapper from the request.
	public static StringBuffer getBody(HttpServletRequest request) throws IOException {
		String line = null;
		StringBuffer sbuff = new StringBuffer();
	//	String requestData = request.getReader().lines().collect(Collectors.joining());

		BufferedReader reader = request.getReader();
		while ((line = reader.readLine()) != null) {
			sbuff.append(line);
		}
		reader.close();
		return sbuff;

	}



	// extract the request body as single line string
	public static String getRequestAsString(HttpServletRequest request) {
		StringBuffer sbuffer = new StringBuffer();
		String line = null;
		try {
			BufferedReader reader = request.getReader();
			while ((line = reader.readLine()) != null) {
				sbuffer.append(line);
			}
			reader.close();
			return sbuffer.toString();
		} catch (Exception e) {
			// ERROR(e, "getRequestAsString", "Exception parsing request " +
			// e.getMessage());
		}

		return null;
	}



	// send json response using a serlaized string
	public static void sendJSONResponse(String resp, HttpServletResponse response) throws IOException {
		response.setContentType("application/json");
		sendResponse(resp, response);
	}

	// create a basicresponse and place in response
	public static void sendErrorResponse(String message, HttpServletResponse response) {
		BaseWebResponse err = new BaseWebResponse(false, message);
		try {
			sendJSONResponse(JSONHelper.serialize(err), response);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void sendResponse(String resp, HttpServletResponse response) throws IOException {
		PrintWriter out = response.getWriter();
		out.write(resp);
		out.flush();
		out.close();
	}



	// standard logging for an http request. pass the request and the name of the
	// caller
	public static void logRequest(Logger log, HttpServletRequest request, String caller) {
		if (log == null)
			log = MYLogFactory.getLogger("");

		log.info(caller + "\t:" + "URI\t:" + request.getRequestURI());
		Enumeration<String> headerNames = request.getHeaderNames();
		String qs = request.getQueryString();
		// Iterate over all the Headers
		while (headerNames.hasMoreElements()) {
			String h = headerNames.nextElement();
			log.info(caller + "\t:" + "Header: " + h + "\t\t: " + request.getHeader(h));

		}
		if (qs != null && !qs.isEmpty())
			log.info(caller + "\t:" + "QueryString: " + qs);
		// note - we dont want to log the body since request.getReader can only be used
		// once.

	}

	public static void logRequest(HttpServletRequest request, String caller) {
		{
			logRequest(MYLogFactory.getLogger(""), request, caller);
		}

	}

	public static void logServletContext(Logger log, ServletContext sc) {

		log.info("context path:\t" + sc.getContextPath());
		log.info("context name:\t" + sc.getServletContextName());
		log.info("server info:\t" + sc.getServerInfo());
	}

	// print all servlet mappings for debug use
	// eat exceptions since we dont want logging to cause problems
	// dump information related to the activated war and tomcat into log.
	public static void logAppInfo(Logger log, ServletContext sc) {
		String cp = sc.getContextPath();
		log.info("Context " + cp + " Starting up >>>>>>>>>>");
		try {
			ServletHelper.logServletContext(log, sc);
			MYLogHelper.logMap(log, "Mappings", getServletMappings(sc));
			MYLogHelper.logMap(log, "Parameters", getParams(sc));
			MYLogHelper.logMap(log, "Attr", getAttrs(sc));
			MYLogHelper.logMap(log, "Prop", getJavaProperties());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.info("Context " + cp + " Startup log complete <<<<<<<<<<");
	}

	public static Map<String, String> getServletMappings(ServletContext sc) {
		Map<String, ? extends ServletRegistration> _reg = sc.getServletRegistrations();
		Map<String, String> tm = new TreeMap<String, String>();

		for (Entry<String, ? extends ServletRegistration> r : _reg.entrySet()) {
			ServletRegistration sr = r.getValue();
			String cls = sr.getClassName();
			Collection<String> mappings = sr.getMappings();
			String url = mappings.isEmpty() ? "" : mappings.iterator().next();
			tm.put(url, cls);
		}
		return tm;
	}

	// get tomcat params
	public static Map<String, String> getParams(ServletContext sc) {
		Enumeration<String> m = sc.getInitParameterNames();
		TreeMap<String, String> tm = new TreeMap<String, String>();
		while (m.hasMoreElements()) {
			String s = (String) m.nextElement();
			String v = sc.getInitParameter(s);
			tm.put(s, v);
		}
		return tm;
	}

	// get tomcat params
	public static Map<String, String> getAttrs(ServletContext sc) {
		Enumeration<String> m = sc.getAttributeNames();
		TreeMap<String, String> tm = new TreeMap<String, String>();
		while (m.hasMoreElements()) {
			String s = (String) m.nextElement();
			String v = sc.getInitParameter(s);
			tm.put(s, v);
		}
		return tm;
	}

	public static Map<String, String> getJavaProperties() {
		TreeMap<String, String> tm = new TreeMap<String, String>();
		Properties p = System.getProperties();
		Enumeration keys = p.keys();
		while (keys.hasMoreElements()) {
			String key = (String) keys.nextElement();
			String value = (String) p.get(key);
			tm.put(key, value);
		}
		return tm;
	}

	private static void logMap(Logger log, String prefix, Map<String, String> sortedMap) {
		int i = 1;
		log.info(prefix + "  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		for (Entry<String, String> entry : sortedMap.entrySet()) {
			log.info(i + " " + prefix + "\t-:" + entry.getKey() + "   \t\t:" + entry.getValue());
			i++;
		}
		log.info(prefix + "  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
		String s;
		try {
			s = JSONHelper.serialize(sortedMap);
			log.info(s);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static <T> T getJSONBody(HttpServletRequest request, Class<T> classtype) throws Exception {
		String body = request.getReader().lines().collect(Collectors.joining());
		T t = JSONHelper.deserialize(body, classtype);
		return t;
	}

	// retreives a saved OPsession from httpsession object
	public static OPSession retreiveSession(HttpServletRequest request) {
		HttpSession s = request.getSession();
		if (s != null) {
			OPSession os = (OPSession) s.getAttribute("opsession");
			return os;
		}
		return null;
	}
	// retreives a saved OPsession from httpsession object
		public static OPSession retreiveSession(HttpSession s) {
			if (s != null) {
				OPSession os = (OPSession) s.getAttribute("opsession");
				return os;
			}
			return null;
		}
}
