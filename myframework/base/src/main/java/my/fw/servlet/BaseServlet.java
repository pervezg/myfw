package my.fw.servlet;

import java.util.NoSuchElementException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;


import my.fw.base.logging.MYLogFactory;
import my.fw.model.OPSession;

@SuppressWarnings("serial")
public class BaseServlet extends HttpServlet {

	protected String ObjName;
	protected OPSession opSession;
	protected org.slf4j.Logger log;

	public BaseServlet() {
		this.ObjName = this.getClass().getSimpleName();
		this.log = MYLogFactory.getLogger(ObjName);
	}

	// called only once when tomcat initializes this servlet class
	public void init() {
		TRACE("init", "Servlet Initialized...");
	}

	protected OPSession retreiveSession(HttpServletRequest request) {
		opSession = ServletHelper.retreiveSession(request);
		if (opSession == null) {
			TRACE("retreiveSession", "No OPSession found");
		} else {
			TRACE("retreiveSession", "Retreived OPSession for user :{} at {}", opSession.getUserId(),
					opSession.getLoginTS());
		}
		return opSession;
	}



	protected void TRACE(String message, Object... propertyValues) {
		TRACE(ObjName, "", message, propertyValues);
	}

	protected void TRACE(String method, String message, Object... propertyValues) {
		TRACE(this.ObjName, method, message, propertyValues);
	}

	protected void ERROR(Exception ex, String method, String message) {
		ERROR(ex, ObjName, method, message);
	}

	protected void ERROR(String message, Object... propertyValues) {
		ERROR(ObjName, "", message, propertyValues);
	}

	protected void ERROR(String method, String message, Object... propertyValues) {
		ERROR(this.ObjName, method, message, propertyValues);
	}

	protected void INFO(String message, Object... propertyValues) {
		INFO(ObjName, "", message, propertyValues);
	}

	protected void INFO(String method, String message, Object... propertyValues) {
		INFO(this.ObjName, method, message, propertyValues);
	}

	private void ERROR(String _obj, String method, String message, Object... propertyValues) {
		if (log != null) {
			String messageTemplate = _obj + " : " + method + " : " + message;
			log.error(messageTemplate, propertyValues);
		}

	}

	public void ERROR(Exception ex, String ObjName, String method, String message, Object... propertyValues) {
		if (log != null) {
			String messageTemplate = ObjName + " : " + method + " : " + message;
			log.error(messageTemplate, ex);
		}
	}

	private void TRACE(String _obj, String method, String message, Object... propertyValues) {
		if (log != null) {
			String messageTemplate = _obj + " : " + method + " : " + message;
			log.debug(messageTemplate, propertyValues);
		}
	}

	private void INFO(String _obj, String method, String message, Object... propertyValues) {
		if (log != null) {
			String messageTemplate = _obj + " : " + method + " : " + message;
			log.info(messageTemplate, propertyValues);
		}
	}

}
