package my.fw.api.model;

public interface IWebResponse {
	public String getMessage();

	public void setMessage(String message);

	public boolean getStatus();

	public void setStatus(boolean status);

	public boolean isSuccessful();

}
