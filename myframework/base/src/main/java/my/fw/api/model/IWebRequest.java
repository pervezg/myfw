package my.fw.api.model;

// basic web request
public interface IWebRequest {
	public String getSessionId();

	public void setSessionId(String sessionId);

	public String getBodyAsString();

	public void setBodyAsString(String bodyAsString);
public String getTraceid();
}
