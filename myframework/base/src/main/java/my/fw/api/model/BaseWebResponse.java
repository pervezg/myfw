package my.fw.api.model;

import my.fw.model.Result;

// most basic json response
public class BaseWebResponse implements IWebResponse {
    public boolean status;
    public String errorCode;
    public String message;
    public String humanMessage;
    //used to connect request/response
    public String traceid;
    // data to be returned by api
    public Object data;

    public BaseWebResponse() {
    }

    public BaseWebResponse(Boolean status, String message) {
        this.message = message;
        this.status = status;
    }

    public BaseWebResponse(Boolean status, String msg, String humanmsg) {
        this.status = status;
        this.message = msg;
        this.humanMessage = humanmsg;

    }

    public static BaseWebResponse Error(Exception e,String msg) {
        return new BaseWebResponse(false,msg+":"+e.getMessage(), "System Error");
    }

    public BaseWebResponse(Result r) {
        if (r != null) {
            setStatus(r.getStatus());
            setErrorCode(r.getErrorCode());
            setMessage(r.getMessage());
            setHumanMessage(r.getHumanMessage());
            this.data = r.getData();
        }
    }

    @Override
    public boolean isSuccessful() {
        return status;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getHumanMessage() {
        return humanMessage;
    }

    public void setHumanMessage(String humanMessage) {
        this.humanMessage = humanMessage;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String msg) {
        this.message = msg;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean s) {
        this.status = s;
    }

}
