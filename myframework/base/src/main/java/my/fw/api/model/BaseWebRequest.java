package my.fw.api.model;

// base class for request body
public class BaseWebRequest implements IWebRequest {

	// request body as a string 
	// we need to do this since servlet body can be parsed only once and we need to have this string for duplicate req detection
	public String SessionId;
	public String traceid;
	public String getSessionId() {
		return SessionId;
	}
	public void setSessionId(String sessionId) {
		SessionId = sessionId;
	}
	public String getBodyAsString() {
		return bodyAsString;
	}
	public void setBodyAsString(String bodyAsString) {
		this.bodyAsString = bodyAsString;
	}

	@Override
	public String getTraceid() {
		return this.traceid;
	}

	public String bodyAsString;
	//public JSONObject bodyAsJSON;
	public BaseWebRequest() {
	}

}
