package my.fw.api.model;

import java.util.HashMap;
import java.util.Map;

public class ReportRequest {

	public ReportRequest() {
		filters = new HashMap<String, String>();
		sorting = new HashMap<String, String>();

	}

	public String name;
	public Map<String, String> filters;
	public Map<String, String> sorting;
	public String format;
	
}
