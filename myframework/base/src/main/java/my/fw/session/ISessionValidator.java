package my.fw.session;

public interface ISessionValidator {
	// validate session based on token
	boolean isValidSession(String id,String token);

}
