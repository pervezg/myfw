package my.fw.service;

import my.fw.model.Result;

// is is intended to be a copy of isimplesvc but returns results
public interface ISimpleSvc<T, ID> {


    // get all records
    Iterable<T> findAll();

    // find a single record by id
    T findOne(ID id);

    long count();

    boolean exists(ID id);


    // insert a single record
    T save(T t);
    T save(T t, String createdBy);

    // update a single record based on ID in the object
    int update(T t);

    int update(T t,boolean includeNulls);

    int update(T t,boolean includeNulls, String modifiedBy);

    int delete(ID id);

    // get the ID of this object
    ID getID(T t);

    // set the id of this object
    T setID(T t, ID id);

    // convert an object to id
    ID toID(Object o);

    // do preprocessing on an id - e,g tolowercase etc.
    ID prepare(ID id);

    // set missing values
    T setDefaults(T t);

    // change the format / content etc of fields
    T format(T t);

    // validate and return all validation errors
    Result validate(T t);

    // populate non db fields for the object
    T expand(T t);

}
