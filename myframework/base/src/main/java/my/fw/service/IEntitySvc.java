package my.fw.service;

import java.util.List;

import my.fw.model.Result;

public interface IEntitySvc<T, ID> {
	
	
	// get all records
	List<T> findAll();

	// find a single record by id
	T findOne(ID id);

	// insert a single record
	T save(T t);

	// update a single record based on ID in the object
	int update(T t);
	int update(T t,String modifiedBy);
	int delete(ID id);

	// get the ID of this object
	ID getID(T t);

	// set the id of this object
	T setID(T t, ID id);

	// convert an object to id
	ID toID(Object o);
	
	// do preprocessing on an id - e,g tolowercase etc.
	ID prepare(ID id);
	
	// populate non db fields for the object
	T expand(T t);
	Result validate(T t);

}
