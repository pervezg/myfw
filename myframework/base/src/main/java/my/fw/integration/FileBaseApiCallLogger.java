package my.fw.integration;

import my.fw.model.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FileBaseApiCallLogger implements IApiCallLogger {
    private static Logger log = LoggerFactory.getLogger("apilogger");

    @Override
    public Result logCall(ApiCallInfo apiCall) {
        log.info("{}", apiCall);
        return Result.SuccessWithData(apiCall);
    }

    @Override
    public Result logCall(String refno, String apiName, String callType, String payload, String result) {
        ApiCallInfo c = new ApiCallInfo(refno, apiName, callType, payload);
        return logCall(c);
    }
}
