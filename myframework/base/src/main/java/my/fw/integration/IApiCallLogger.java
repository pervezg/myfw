package my.fw.integration;


import my.fw.model.Result;

public interface IApiCallLogger {
    // logs an api call to repository
    Result logCall(ApiCallInfo apiCall);

    Result logCall(String refno, String apiName, String callType, String payload, String result);

}
