package my.fw.integration;

// info used in an api call - used for logging external api calls
public class ApiCallInfo {
    public String reqRefNo;
    public String apiName;
    public String callType;
    public String payload;

    public ApiCallInfo(String reqRefNo, String apiName, String callType, String payload) {
        this.reqRefNo = reqRefNo;
        this.apiName = apiName;
        this.callType = callType;
        this.payload = payload;
    }

    public ApiCallInfo() {
    }


}
