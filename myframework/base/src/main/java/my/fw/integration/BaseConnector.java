package my.fw.integration;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

// base class that can be extended by all connectors
public abstract class BaseConnector implements IConnector {
    protected String name;
    protected String version;
    protected String description;
    protected Map<String, String> settings;
    private Properties props;

    public BaseConnector(String name) {
        this(name, new HashMap<>());
    }

    public BaseConnector(String name, Map<String, String> props) {
        if (name != null) name = name.toLowerCase().trim();
        this.name = name;
        applySettings(props);
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getVersion() {
        return this.version;
    }

    @Override
    public String getDescription() {
        return this.description;
    }

    @Override
    public Map<String, String> getSettings() {
        if (settings == null) settings = new HashMap<>();
        return settings;
    }

    @Override
    public void applySettings(Map<String, String> props) {
        if (props != null) {
            getSettings().putAll(props);
        }
    }

    @Override
    public void clearSettings() {
        this.settings = new HashMap<>();
    }

    @Override
    public void setCredentials(String... credentials) {

    }

    @Override
    public void setSetting(String name, String value) {
        if (name == null) return;
        name = name.toLowerCase().trim();
        getSettings().put(name, value);
    }

    @Override
    public String getSetting(String name) {
        if (name == null) return null;
        name = name.toLowerCase().trim();
        return getSettings().get(name);
    }

    @Override
    public void applySettings(Properties props) {
        this.props=props;
    }
}
