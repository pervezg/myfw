package my.fw.integration;

import java.util.Map;
import java.util.Properties;

// a connector to external system
// this is the base interface for all connectors
public interface IConnector {
    String getName();

    String getVersion();

    String getDescription();

    Class<?> getType();

    Map<String, String> getSettings();

    // add settings to the connector . appends/ replaces
    void applySettings(Map<String, String> props);

    void applySettings(Properties props);

    void clearSettings();

    // set the credentials for the connector
    // this is separate from other settings
    // since we dont know the number and nature of credentials , we use a string array
    void setCredentials(String... credentials);

    void setSetting(String name, String value);

    String getSetting(String name);

    // initialize the connector. this is used for connectors that need some setting to be applied before they
    // can be initted
    void init();
    void reset();
    void close();

}
