package my.fw.model;

public interface IProperty{
    void setName(String name);
    String getName();
    String getValue();
    void setValue(String value);
}
