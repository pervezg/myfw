package my.fw.model;

import javax.persistence.Column;

// common base for all child tables container parent props
// contians everythig for a prop object except parent
public abstract class AbsEntityProperty<ID> extends AbsEntityWithId<ID> implements IProperty {
    @Column
    String name;

    @Column
    String value;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String n) {
        if (n != null) {
            this.name = n.toLowerCase().trim();
        }
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public void setValue(String value) {
        this.value = value;
    }


}
