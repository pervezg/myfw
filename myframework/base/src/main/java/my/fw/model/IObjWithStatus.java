package my.fw.model;

public interface IObjWithStatus {
	String getStatus();
	void setStatus(String s);

}
