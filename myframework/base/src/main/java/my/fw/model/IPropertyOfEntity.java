package my.fw.model;

public interface IPropertyOfEntity<ID,PID> extends IProperty,IHasParent<PID>,IObjWithId<ID>,IObjWithUpdateTS {
}
