package my.fw.model;

public interface IHasParent<PID> {
    public PID getParentId();
    public void setParentId(PID i);
}
