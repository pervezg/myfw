package my.fw.model;

import my.fw.model.IObjWithId;

// this is a special interface to be used in cases where the object has in Integer id
// it is used in case of tables that have an autoupdated id
// this is a substitue for IobjWithId<Integer>. It is  complicated to check is an object implements a generic inteface
// so we use this as a substitute .
// the sql repo check for this interface to autogen an id
public interface IEntityWithId extends IObjWithId<Integer> {
	public Integer getId();
	public void setId(Integer i);
}
