package my.fw.model;

// Object representing result of an operation
public class Result {
	public boolean status;
	public String errorCode;
	public String message;
	public String humanMessage;
	public Exception ex;
	// data to be returned by api
	public Object data;

	public Exception getException() {
		return ex;
	}

	public void setException(Exception e) {
		this.ex = e;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public Result() {

	}

	public Result(boolean succ) {
		setSuccess(succ);
	}

	public Result(String errcode, String msg) {
		this(false, errcode, msg);
	}

	public Result(boolean succ, String errcode, String msg) {
		this.errorCode = errcode;
		this.message = msg;
		setSuccess(succ);
	}

	public static Result Error(String errcode, String msg) {
		return new Result(false, errcode, msg);
	}

	public static Result Success() {
		return new Result(true, "SUCCESS", null);
	}

	public static Result SuccessWithData(Object d) {
		Result r = new Result(true, "SUCCESS", null);
		r.setData(d);
		return r;
	}

	public static Result Success(String msg) {
		return new Result(true, "SUCCESS", msg);
	}

	public Result(Boolean status, String message) {
		this.message = message;
		this.status = status;
	}

	public void setSuccess(boolean b) {
		this.status = b;
	}

	public boolean isSuccess() {
		return status;
	}

	public Boolean isSuccessful() {
		return status;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getHumanMessage() {
		return humanMessage;
	}

	public void setHumanMessage(String humanMessage) {
		this.humanMessage = humanMessage;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String msg) {
		this.message = msg;
	}

	public boolean getStatus() {
		return status;
	}

	public void setStatus(boolean s) {
		this.status = s;
	}

	public static Result Error(Exception e, String msg) {
		Result r = new Result();
		r.setMessage(msg);
		r.setException(e);
		return r;
	}

}
