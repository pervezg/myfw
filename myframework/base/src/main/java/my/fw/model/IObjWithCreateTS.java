package my.fw.model;

import java.util.Date;

public interface IObjWithCreateTS {
	Date getCreatedOn();

	void setCreatedOn(Date createTs);

	//void setCreateTS();

	//String getCreatedBy();

//	void setCreatedBy(String createdBy);
}
