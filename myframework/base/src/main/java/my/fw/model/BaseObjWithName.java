package my.fw.model;

public abstract class BaseObjWithName implements IObjWithName, IObjWithId<String>  {

	protected String name;

	public BaseObjWithName() {
	}

	public BaseObjWithName(String n) {
		setName(n);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		if (name != null & !name.isEmpty()) {
			this.name = name.trim().toLowerCase();
		}
	}

	@Override
	public String getId() {
		return getName();
	}

	@Override
	public void setId(String _id) {
		setName(_id);
	}

}
