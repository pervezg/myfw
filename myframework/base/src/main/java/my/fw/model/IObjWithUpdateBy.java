package my.fw.model;

// records the id that updated a record
// used be generic service and usually mapped to a db column named updateby
public interface IObjWithUpdateBy {
	String getUpdateBy();
	void setUpdateBy(String userid);
}
