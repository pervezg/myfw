package my.fw.model;

import java.util.Date;

public interface IObjWithUpdateTS {
	Date getModifiedOn();

	void setModifiedOn(Date ts);

	//void setUpdateTS();

	//String getModifiedBy();

	//void setModifiedBy(String modifiedBy);
}
