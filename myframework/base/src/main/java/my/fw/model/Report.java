package my.fw.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Report extends BaseObjWithName implements IObjWithName, IObjWithId<String> {
	public String title;
	public String query;
	public List<String> params;
	public Map<String, String> filters;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public List<String> getParams() {
		return params;
	}

	public void setParams(List<String> params) {
		this.params = params;
	}

	public Map<String, String> getFilters() {
		return filters;
	}

	public void setFilters(Map<String, String> filters) {
		this.filters = filters;
	}

	public Report() {
		params = new ArrayList<String>();
		filters = new HashMap<String, String>();
	}

	@Override
	public String getId() {
		return name;
	}

	@Override
	public void setId(String _id) {
		setName(_id);
	}

}
