package my.fw.model;

public interface IObjWithCreatedBy {
    String getCreatedBy();
    void setCreatedBy(String created_by);
}
