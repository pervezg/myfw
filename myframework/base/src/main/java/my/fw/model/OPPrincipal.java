package my.fw.model;

import java.util.List;

// principal object represents a user of the system along with permissions
// this object is passed to various classes to that the userinfo does not to be got from DB 
public class OPPrincipal {
	String userId;
	String primaryRole;
	List<String> roles;
	String firstName;
	String lastName;
	String emailId;
	

}
