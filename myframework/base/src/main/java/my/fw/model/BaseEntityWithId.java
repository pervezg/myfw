package my.fw.model;

import my.fw.model.IEntityWithId;

public class BaseEntityWithId implements IEntityWithId {

	protected Integer id;

	public BaseEntityWithId() {
	}

	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public void setId(Integer i) {
		this.id = i;
	}

}
