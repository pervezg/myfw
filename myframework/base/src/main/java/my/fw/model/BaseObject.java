package my.fw.model;

import my.fw.base.logging.MYLogFactory;
import org.slf4j.Logger;

public abstract class BaseObject {
	protected String ObjName = "";
	// development mode , used for better logging or diag features.
	protected boolean devMode=false;

	public boolean isDevMode() {
		return devMode;
	}

	public void setDevMode(boolean devMode) {
		this.devMode = devMode;
	}

	// new slf4j logger with logback - need to switch to this
	protected Logger log;

	public BaseObject() {
		this.ObjName = this.getClass().getSimpleName();
		log = MYLogFactory.getLogger(ObjName);

	}

	// pass a custom logger
	public BaseObject(Logger logl) {
		ObjName = this.getClass().getSimpleName();
		//this._log = logl == null ? Logger.getLogger(ObjName) : logl;
		this.log = log == null ? MYLogFactory.getLogger(ObjName) : log;

	}

	protected void TRACE(String message, Object... propertyValues) {
		TRACE(ObjName, "", message, propertyValues);
	}

	protected void TRACE(String method, String message, Object... propertyValues) {
		TRACE(this.ObjName, method, message, propertyValues);
	}

	protected void ERROR(Exception ex, String method, String message) {
		ERROR(ex, ObjName, method, message);
	}

	protected void ERROR(String message, Object... propertyValues) {
		ERROR(ObjName, "", message, propertyValues);
	}

	protected void ERROR(String method, String message, Object... propertyValues) {
		ERROR(this.ObjName, method, message, propertyValues);
	}

	protected void INFO(String message, Object... propertyValues) {
		INFO(ObjName, "", message, propertyValues);
	}

	protected void INFO(String method, String message, Object... propertyValues) {
		INFO(this.ObjName, method, message, propertyValues);
	}

	private void ERROR(String _obj, String method, String message, Object... propertyValues) {
		if (log != null) {
			String messageTemplate = _obj + " : " + method + " : " + message;
			log.error(messageTemplate, propertyValues);
		}
	}

	public void ERROR(Exception ex, String ObjName, String method, String message, Object... propertyValues) {
		if (log != null) {
			String messageTemplate = ObjName + " : " + method + " : " + message;
			log.error(messageTemplate, ex);
		}
	}

	private void TRACE(String _obj, String method, String message, Object... propertyValues) {
		if (log != null) {
			String messageTemplate = _obj + " : " + method + " : " + message;
			log.debug(messageTemplate, propertyValues);
		}
	} 

	private void INFO(String _obj, String method, String message, Object... propertyValues) {
		if (log != null) {
			String messageTemplate = _obj + " : " + method + " : " + message;
			log.info(messageTemplate, propertyValues);
		}
	}

}
