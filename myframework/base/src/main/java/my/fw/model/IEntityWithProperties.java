package my.fw.model;

public interface IEntityWithProperties<ID,PID> extends IObjWithId<ID> {

    IPropertyOfEntity<ID,PID> getProp(String key);
    void putProp(IPropertyOfEntity<ID,PID> prop);
    void removeProp(String key);
}
