package my.fw.model;

public interface IObjWithUserid {
	String getUserId();

	void setUserId(String id);

}
