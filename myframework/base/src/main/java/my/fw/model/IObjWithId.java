package my.fw.model;

// object with with id field 
// the ID can only be  Integer or String
public interface IObjWithId<ID> {
	ID getId();
	void setId(ID _id);
}
