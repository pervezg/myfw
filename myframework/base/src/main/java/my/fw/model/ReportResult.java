package my.fw.model;

import java.util.List;

public class ReportResult extends Result {

	private Report report;
	private List<String> headings;
	// count and sql are only for debugging 
	public long count;
	public String sql;

	public ReportResult() {
	}

	public ReportResult(boolean success, String errcode, String msg) {
		setSuccess(success);
		setErrorCode(errcode);
		setMessage(msg);
	}

	public Report getReport() {
		return report;
	}

	public void setReport(Report report) {
		this.report = report;
	}

	public List<String> getColumns() {
		return headings;
	}

	public void setColumns(List<String> columns) {
		this.headings = columns;
	}

	public static ReportResult Error(String errcode, String msg) {

		return new ReportResult(false, errcode, msg);
	}
}
