package my.fw.model;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

public abstract class AbsEntityWithId<ID> implements IObjWithId<ID>, IObjWithUpdateTS {
    @Id
    @Column
    @GeneratedValue

     ID id;

    @Column
    Date updatets;

    @Override
    public ID getId() {
        return this.id;
    }

    @Override
    public void setId(ID i) {
        this.id = i;
    }

    @Override
    public Date getModifiedOn() {
        return updatets;
    }

    @Override
    public void setModifiedOn(Date ts) {
        this.updatets = ts;
    }
}
