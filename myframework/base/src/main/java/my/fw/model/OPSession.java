package my.fw.model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// contain information regarding the current session/user which can be propagated to other classes for authorization
public class OPSession implements IObjWithUserid {
	String userId;
	String sessionID;
	String primaryRole;
	List<String> roles;
	LocalDateTime loginTS;
	Map<String, String> attributes;

	public OPSession() {
		super();
		this.loginTS = LocalDateTime.now();
		this.roles = new ArrayList<String>();
		this.attributes = new HashMap<String, String>();
	}

	public OPSession(String uid, String role, boolean b, String sid) {
		this();
		this.userId = uid;
		this.primaryRole = role;
		this.sessionID=sid;
		setAttribute("isfirstlogin", Boolean.toString(b));
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public void setAttribute(String key, String value) {
		if (key == null || key.isEmpty())
			return;
		key = key.toLowerCase().trim();
		if (attributes == null)
			attributes = new HashMap<String, String>();
		attributes.put(key, value);
	}

	public String getAttribute(String key) {
		if (key == null || key.isEmpty() || attributes == null)
			return null;
		key = key.toLowerCase().trim();
		return attributes.get(key);
	}

	public String getSessionID() {
		return sessionID;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public String getPrimaryRole() {
		return primaryRole;
	}

	public void setPrimaryRole(String primaryRole) {
		this.primaryRole = primaryRole;
	}

	public LocalDateTime getLoginTS() {
		return loginTS;
	}

	public void setLoginTS(LocalDateTime loginTS) {
		this.loginTS = loginTS;
	}

}
