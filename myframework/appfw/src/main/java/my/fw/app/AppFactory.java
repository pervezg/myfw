package my.fw.app;

import my.fw.base.app.App;
import my.fw.base.logging.MYLogFactory;
import my.fw.base.system.MyProperties;
import my.fw.base.system.OSHelper;
import my.fw.sqlrepo.db.DBConnHelper;
import my.fw.sqlrepo.db.DSFactory;
import my.fw.sqlrepo.db.DatabaseConnectionManager;
import org.apache.commons.lang3.StringUtils;

import javax.sql.DataSource;
import java.util.Map;
import java.util.Properties;
// factory class initialize application settings, logs and db
// assumes that there is a single DS

public class AppFactory {
    static DataSource ds;
    static boolean initDone = false;

    // global init  , is run only once
    public static App initApp(Class<?> klass, App app) {
        return initApp(klass, app, true, true);
    }

    public static App initApp(Class<?> klass, App app, boolean initLogs, boolean initDB) {
        if (initDone) return app;
        app = loadAppProperties(app);
        if (initLogs) initLogs(app);
        if (initDB) initDs(app);
        initDone = true;
        return app;
    }

    // populates the properties ( and may also init the DB)  for the app.
    public static App initAppProperties(Class<?> klass, App app) {

        app = MyProperties.initAppProperties(app, klass);
        return app;
    }

    // Initialize logs from props in App
    public static void initLogs(App app) {
//        String logdir = app.props.getProperty(MYLogFactory.PropNames.dir);
//        String loglevel = app.props.getProperty(MYLogFactory.PropNames.level);
//        MYLogFactory.init(logdir, app.getName(), loglevel);
        Properties logprops = MyProperties.filterPrefix("log", app.getProps(), true);
        MYLogFactory.init(app.name, logprops, null);
    }

    //
    public static DataSource initDs(App app) {
        if (ds != null) return ds;
        ds = DBConnHelper.getDSFromProps("datasource", app.getProps());
        DSFactory.setDefaultDS(ds);
        DatabaseConnectionManager.logDataSource(ds);
        DatabaseConnectionManager.setDefaultDS(ds);
        return null;
    }


    // gets a property from the app properties
// looks for appname.key and then for key
    public static String getAppProperty(App app, String key) {
        if (app == null || app.props == null || key == null) return null;
        String val;
        val = app.props.getProperty(app.name + "." + key);
        if (val != null) return val;
        return app.props.getProperty(key);
    }


    public static App initPropertiesFromConfigFolder(App app) {
        String[] fnames = app.getPropFileShortNames().toArray(new String[0]);
        Map<String, Properties> propmap = MyProperties.getProperties(app.configDir, true, null, fnames);
        app.propsMap = propmap;
        app.props = MyProperties.combineProps(propmap.values());
        return app;
    }

    public static App loadAppProperties(App app) {
        // first create a list of propfilenames
        String appcode = app.getAppcode();

        // 1 - load all env vars that start with appcode e.g. APPCODEHOME
        // this will capture any env vars starting with appcode, including home etc
        app.propsFromEnv = MyProperties.env2Props(app.getAppcode(),true);


        String home = app.propsFromEnv.getProperty("home");
        if (StringUtils.isBlank(home)) {
            System.out.println("cannot determine home for app " + appcode);
            throw new RuntimeException();
        }
        app.homeDir = home;
        app.configDir = OSHelper.join(home, "config");
        initPropertiesFromConfigFolder(app);
        setDefaultProperties(app, home);


//        // 2. now check if env contains a specific prop file name
//        String propFileAbsPath = findPropFile(app, klass);
//
//        if (!propFileAbsPath.isEmpty()) {
//            app.setPropFileAbsPath(propFileAbsPath);
//            Properties fileprops = loadFromFile(propFileAbsPath);
//            Properties appprops = filterPrefix(app.getAppcode(), fileprops, true);
//            app.propsFromFile = appprops;
//        }


        // TODO  now we need to get db informatmation , init db and read settings


        // finally combine all props in order of precedence
        app.props.putAll(app.propsFromDb);
        app.props.putAll(app.propsFromFile);
        app.props.putAll(app.propsFromEnv);

        return app;
    }

    // initializes app from a set of properties
    public static App setAppProperties(App app, Properties appProps){

        String home =appProps.getProperty("home");
        if (StringUtils.isBlank(home)) {
            System.out.println("cannot determine home for app " + app.getAppcode());
            throw new RuntimeException();
        }
        app.setProps(appProps);
        app.homeDir = home;
        app.configDir = OSHelper.join(home, "config");
        setDefaultProperties(app, home);
       return app;
    }
    // sets certain default props for an app
    static App setDefaultProperties(App app, String homeDir) {
        if (StringUtils.isBlank(app.getProps().getProperty("log.dir"))) {
            String logdir = OSHelper.join(homeDir, "logs");
            app.getProps().setProperty("log.dir", logdir);
        }
        if (StringUtils.isBlank(app.getProps().getProperty("log.level"))) {
            app.getProps().setProperty("log.level", "DEBUG");
        }
        return app;
    }

    static void setDefault(Properties props, String propname, String propdefault) {
        String s = props.getProperty(propname);
        if (StringUtils.isBlank(s)) {
            props.setProperty(propname, propdefault);
        }
    }

    // sets log level for all loglevel.* props
    public static void disableExtraLogs(App app) {
        Properties logprops = MyProperties.filterPrefix(MYLogFactory.PropNames.loglevelprefix, app.getProps(), true);
        MYLogFactory.setLevel(logprops);
    }

}
