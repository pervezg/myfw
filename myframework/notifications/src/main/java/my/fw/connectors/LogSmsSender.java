package my.fw.connectors;


import my.fw.base.logging.MYLogFactory;
import my.fw.integration.AbstractBaseConnector;
import my.fw.model.Result;
import my.fw.notifications.ISMSSender;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

import java.util.Properties;

// sends sms messages to log file
//
public class LogSmsSender extends AbstractBaseConnector implements ISMSSender {
    public static class PropNames {
        public static String logDir = "logdir";
        public static String logName = "logname";

    }

    static Logger log;
    String logName = "email";
    String logDir = ".";


    public LogSmsSender() {
        super("LogSmsSender");
        this.description = "Mock SMS sender that logs sendsms calls ";
        this.version = "01";
        init();
    }

    @Override
    public void init() {
        super.init();
        log = MYLogFactory.getFileLogger(logDir, logName, "INFO");
    }

    @Override
    public void applySettings(Properties props) {
        super.applySettings(props);
        String s = settings.get(LogMailSender.PropNames.logDir);
        if (StringUtils.isNotBlank(s)) this.logDir = s;
        String n = settings.get(LogMailSender.PropNames.logName);
        if (StringUtils.isNotBlank(n)) this.logName = n;
    }

    @Override
    public Result sendMsg(String to, String body, String from) {
        log.info(" To: {} |Form : {} | body: {}", to, from, body);
        return Result.Success();
    }

    @Override
    public Class<?> getType() {
        return ISMSSender.class;
    }
}
