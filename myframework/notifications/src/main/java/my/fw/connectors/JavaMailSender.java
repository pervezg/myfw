package my.fw.connectors;

import my.fw.model.Result;
import my.fw.notifications.IEmailSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.util.Properties;

// mailsender based on javax.mail
public class JavaMailSender implements IEmailSender {

    static Logger log = LoggerFactory.getLogger(JavaMailSender.class);
    String user;
    String pwd;
    String host;
    private Properties javaMailProps;

    public JavaMailSender() {
        javaMailProps = setdefaultProperties();
    }

    public JavaMailSender(String host, String user, String pass) {
        this();
        this.user = user;
        this.pwd = pass;
        this.host = host;
        javaMailProps.put("mail.smtp.host", host);
        log.debug("JavaMailSender: Initialized host: {} user: {}",host,user);

    }

    // props follow  in javamail naming and not netc
    public JavaMailSender(Properties props) {
        this();
        javaMailProps.putAll(props);
    }

    public JavaMailSender(String host, String user, String pass, Properties props) {
        this(host, user, pass);
        System.out.println("JavaMailSender:"+host+" "+user+" "+pass+" "+props);
        javaMailProps.putAll(props);
    }

    public void setProperties(Properties props) {
        this.javaMailProps = toJavaMailProps(props);
    }

    @Override
    public Result sendMail(String to, String subject, String mailBody) {
        return _sendMail(to, subject, mailBody, null, null);
    }

    @Override
    public Result sendMail(String to, String subject, String mailBody, String from) {
        return _sendMail(to, subject, from, mailBody, null);
    }

    @Override
    public Result sendMail(String to, String subject, String mailBody, String from, String cc) {
        return _sendMail(to, subject,  mailBody,from, cc);
    }

    // send simple mail
    public Result _sendMail(String to, String subject, String mailBody, String from, String cc) {
        try {
            log.debug("JavaMailSender: sending via {}:{} to: {} subject:{} ",host,user,to,subject);
            MimeMessage message = newMimeMessage(from, to, cc, subject);
            message.setContent(mailBody, "text/html");
            _send(message);
            return Result.Success();
        } catch (Exception mex) {
            log.error("JavaMailSender:sendMail ", mex);
            return Result.Error(mex,"JavaMailSender:sendMail");
        }
    }

    private static void _send(Message message) throws MessagingException {
        long startTime = System.currentTimeMillis();
        int s = message.getSize();
        Transport.send(message);

        long endTime = startTime - System.currentTimeMillis();
        log.debug("sendMail : email size: {}  Time taken: {} ms", s, endTime);
    }

    private MimeMessage setAddresses(MimeMessage message, String from, String to, String cc) throws MessagingException {
        String[] recipientList = to.split(",");
        if (from != null && !from.isEmpty()) {
            message.setFrom(new InternetAddress(from));
        }
        for (int i = 0; i < recipientList.length; i++) {
            //log.debug("SendMailService.java ::: sendMail() :: Mail Recipients TO are : " + recipientList[i]);
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(recipientList[i]));
        }
        if (cc != null && !cc.equalsIgnoreCase("")) {
            String[] ccList = cc.split(",");
            for (int i = 0; i < ccList.length; i++) {
               // log.debug("SendMailService.java ::: sendMail() :: Mail Recipients CC are : " + ccList[i]);
                message.addRecipient(Message.RecipientType.CC, new InternetAddress(ccList[i]));
            }
        }
        return message;

    }


    public Result SendMailWithAttachment(String to,String subject,String mailBody, String from, String cc,  String path,  String pdfFileName) {
        try {
            MimeMessage message = newMimeMessage(from, to, cc, subject);

            // Create the message part
            BodyPart messageBodyPart = new MimeBodyPart();

            // Now set the actual message
            messageBodyPart.setText(mailBody);

            // Create a multipar message
            Multipart multipart = new MimeMultipart();

            // Set text message part
            multipart.addBodyPart(messageBodyPart);

            // Part two is attachment
            messageBodyPart = new MimeBodyPart();

            DataSource source = new FileDataSource(path);
            messageBodyPart.setDataHandler(new DataHandler(source));
            messageBodyPart.setFileName(pdfFileName);
            multipart.addBodyPart(messageBodyPart);

            // Send the complete message parts
            message.setContent(multipart);

            // Send message
            _send(message);

            //log.debug("Sent message successfully....");
            return Result.Success();
        } catch (MessagingException e) {
            log.error("sendattachement() ", e);
            return Result.Error(e,"sendattachement");
        }
    }

    private MimeMessage newMimeMessage(String from, String to, String cc, String subject) throws MessagingException {
        Session session = getSmtpSession();
        MimeMessage message = new MimeMessage(session);
        message = setAddresses(message, from, to, cc);
        message.setSubject(subject);
        return message;
    }

    private Session getSmtpSession() {
        Session session = Session.getDefaultInstance(javaMailProps,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(
                                user, pwd);
                    }
                });
        return session
                ;
    }


    // sets properties for using  javamail
    private Properties toJavaMailProps(Properties props) {
        Properties properties = System.getProperties();
        properties.put("mail.smtp.host", props.get("host"));
        properties.put("mail.smtp.user", props.get("user"));
        properties.put("mail.smtp.password", props.get("pwd"));
        properties.put("mail.smtp.auth", props.get("auth"));
        properties.put("mail.smtp.starttls.enable", props.get("starttls"));
        properties.put("mail.smtp.ssl.enable", props.get("enablessl"));
        properties.put("mail.smtp.port", props.get("port"));
        return properties;
    }

    // sets default properties for using  javamail so that we need only set host/user/pwd
    private Properties setdefaultProperties() {
        Properties properties = new Properties();
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "false");
        properties.put("mail.smtp.ssl.enable", "true");
        properties.put("mail.smtp.port", "465");
        return properties;
    }


    //@Override
    public Result sendMail( String from, String[] to, String subject, String mailBody,String[] cc) {
        log.info(" To: {} |Form : {} | subject: {}", to, from, subject);
        if (mailBody != null)
            log.info("Body: {}", mailBody);
        return Result.Success();
    }

}
