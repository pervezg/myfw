package my.fw.connectors;


import my.fw.base.logging.MYLogFactory;
import my.fw.integration.AbstractBaseConnector;
import my.fw.model.Result;
import my.fw.notifications.IEmailSender;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

import java.util.Map;
import java.util.Properties;

// Mail sender that will send all email to log file
public class LogMailSender extends AbstractBaseConnector implements IEmailSender {

    public static class PropNames {
        public static String logDir = "logdir";
        public static String logName = "logname";

    }

    static Logger log;
    String logName = "email";
    String logDir = ".";

    public LogMailSender() {
        super(LogMailSender.class.getSimpleName());
        init();
    }

    @Override
    public Class<?> getType() {
        return IEmailSender.class;
    }


    @Override
    public void init() {
        super.init();
        log = MYLogFactory.getFileLogger(logDir, logName, "INFO");
    }

    @Override
    public void applySettings(Properties props) {
        super.applySettings(props);
        String s = settings.get(PropNames.logDir);
        if (StringUtils.isNotBlank(s)) this.logDir = s;
        String n = settings.get(PropNames.logName);
        if (StringUtils.isNotBlank(n)) this.logName = n;
    }

    @Override
    public Result sendMail(String to, String subject, String mailBody) {
        return sendMail(to, subject, mailBody, null, null);
    }

    @Override
    public Result sendMail(String to, String subject, String mailBody, String from) {
        return sendMail(to, subject, mailBody, from, null);
    }

    @Override
    public Result sendMail(String to, String subject, String mailBody, String from, String cc) {
        log.info(" To: {} |Form : {} | subject: {}", to, from, subject);
        if (mailBody != null)
            log.info("Body: {}", mailBody);
        return Result.Success();
    }


    @Override
    public Result SendMailWithAttachment(String to, String mailFrom, String mailCC, String mailbody, String path, String subject, String pdfFileName) {
        log.info(" To: {} |Form : {} | subject: {}");
        if (mailbody != null)
            log.info(mailbody);
        return Result.Success();
    }


    public Result sendMail(String from, String[] to, String subject, String mailBody, String[] cc) {
        log.info(" To: {} |Form : {} | subject: {}", to, from, subject);
        if (mailBody != null)
            log.info("Body: {}", mailBody);
        return Result.Success();
    }

}
