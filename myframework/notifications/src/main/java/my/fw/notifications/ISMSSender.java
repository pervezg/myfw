package my.fw.notifications;


import my.fw.model.Result;

public interface ISMSSender {

 Result sendMsg(String mobileNum, String msgBody, String senderId);
}
