package my.fw.notifications;


import my.fw.model.Result;

public interface IEmailSender {
   // Result sendMail( String from, String[] to, String subject, String mailBody,String[] cc);

    Result sendMail(String to, String subject, String mailBody);

    Result sendMail(String to, String subject, String mailBody, String from);

    Result sendMail(String to, String subject, String mailBody, String from, String cc);

    Result SendMailWithAttachment(String to, String subject, String mailBody, String from, String cc, String path, String pdfFileName);
}
