package my.fw.tests.notofication;

import my.fw.connectors.LogMailSender;
import my.fw.connectors.LogSmsSender;
import my.fw.notifications.IEmailSender;
import my.fw.notifications.ISMSSender;
import org.junit.Test;

import java.util.Map;
import java.util.Properties;

public class EmailTest {

    @Test
    public void SendMailTest() {
        IEmailSender i = getemailSender();
        i.sendMail("pervez@1pay.in", "hello", "hello pervez");
        i.sendMail("pervez@1pay.in", "hello", "hello pervez 1");
        i.sendMail("pervez@1pay.in", "hello", "hello pervez 2");
    }

    @Test
    public void SendSmsTest() {
        ISMSSender i = getSmsSender();
        i.sendMsg("990012345", "hello", "MYBANK");
        i.sendMsg("990012345", "hello 1", "MYBANK");
        i.sendMsg("990012345", "hello 2", "MYBANK");
        i.sendMsg("990012345", "hello 3", "MYBANK");
    }

    LogMailSender getemailSender() {
        Properties p = new Properties();
        p.put(LogMailSender.PropNames.logName, "mailtest");
        p.put(LogMailSender.PropNames.logDir, "d:\\home");
        LogMailSender s = new LogMailSender();
        s.applySettings(p);
        s.init();
        return s;
    }

    LogSmsSender getSmsSender() {
        Properties p = new Properties();
        p.put(LogMailSender.PropNames.logName, "smstest");
        p.put(LogMailSender.PropNames.logDir, "d:\\home");
        LogSmsSender s = new LogSmsSender();
        s.applySettings(p);
        s.init();
        return s;
    }

}
