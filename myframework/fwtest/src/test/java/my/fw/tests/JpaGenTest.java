package my.fw.tests;

import my.fw.sqlrepo.db.DBConnHelper;
import my.fw.sqlrepo.util.JpaEntityGenerator;
import org.junit.Test;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

public class JpaGenTest {

    //@Test

    public void jpatest() throws SQLException, IOException {

        genJpa("acqdb", "jdbc:mysql://localhost:3306/acqdb?profileSQL=true", "root", "Netc_1234", "d:\\acqjpa");
    }

    public void genJpa(String schema, String jdbcUrl, String u, String p, String dir) throws SQLException, IOException {

        DataSource ds = DBConnHelper.createMysqlDS(jdbcUrl, schema, u, p);
        JpaEntityGenerator gen = new JpaEntityGenerator(ds);
        String s = gen.genAllEntity(schema);
        gen.genAllEntityList(schema, "d:\\acqjpa", false);
    }

}
